﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.Resources
{
    public class FactorSecction
    {
        public FactorSecction()
        {
            this.listFactorResult = new List<double>();
        }

        public void addElement(int idClass, double factor, double frequence, double element)
        {
            this.idClass = idClass;
            this.factor = factor;
            this.frequence = frequence;
            this.listFactorResult.Add(element);
        }

        public List<double> getListFactorResult()
        {
            return this.listFactorResult;
        }

        public double getFrequence()
        {
            return this.frequence;
        }

        public int getIdClass()
        {
            return this.idClass;
        }

        public double getFactor()
        {
            return this.factor;
        }

        private int idClass;                        //id de entrada da classe
        private double frequence;                   //frequencia absoluta
        private double factor;                      //fator ponderador
        private List<double> listFactorResult;      //Resultado da multiplicação do fator pela quantidade de secções
    }
}
