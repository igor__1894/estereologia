﻿/*
    IPEN (Instituto de Pesquisas Energéticas e Nucleares)
    
    Grupo GVCM
    Aluno: Igor Bernardes Urias
    Projeto desenvolvido em  2016-2017
    -------------------------------------------------------------------------------------------------------------------

    Aplicação desenvolvida para análise quantitativa de microestruturas a partir dos métodos estereológicos;
    Como foco do projeto, o método empregado para esta análise é o de Saltykov;
    
    -------------------------------------------------------------------------------------------------------------------

    Trabalho desenvolvido em um projeto de iniciação científica do Instituto de Pesquisas Energéticas e Nucleares (IPEN)
    CCTM
        
*/

using System;
using System.Drawing;
using System.Windows.Forms;
using ShadowEngine;
using ShadowEngine.OpenGL;
using Tao.OpenGl;
using Stereology.PanelStyles;
using Stereology.MathAlgo;
using Estereology.PanelStyles;

namespace Stereology
{
    //Enumeração dos tipos de vizualiações do sistema
    enum StyleView
    {
        ModelView, ProjectionView, OrthoView

    }

    public partial class MainForm : Form
    {           

        public static Vector2 formPosition
        {
            get {
                return MainForm.vectorFormPosition;
            }

            set {
                MainForm.vectorFormPosition = value;
            }
        }

        public MainForm()
        {
            //Método para a inicialização dos componentes da interface
            initializeComponents();

            hdcDisplay = (uint)panelStereologyGL.Handle;
            string error = "";

            OpenGLControl.OpenGLInit(ref hdcDisplay, panelStereologyGL.Width, panelStereologyGL.Height, ref error);

            if (error != "")
            {
                MessageBox.Show(error);
            }

            startDefaultScene();
            createComponentsInfo();
        }

        public void startDefaultScene()
        {
            startLight();

            Position camera;
            Position center;

            camera.x = 1.5f;
            camera.y = 1.0f;
            camera.z = 1.5f;

            center.x = 0.0f;
            center.y = 0.0f;
            center.z = 0.0f;

            systemStereology.Camara.initCameraOrtho(camera, center);

            //ContentManager.SetTextureList("texturas\\");
            //ContentManager.LoadTextures();

            createTopMenu();                        //Criação do menu principal
            createOptionsMenu();                    //Criação do menu de opções de configurações
            systemStereology.CreateScene();         //Renderizar o sistema de esferas

            //Camera.CenterMouse();
        }

        private void startProcessEstereology(object sender, EventArgs e)
        {
            switch (optionView)
            {
                case 0:
                    createOrthoViewScene();

                    break;

                case 1:
                    createModelViewScene();

                    break;

                case 2:
                    createOrthoViewPlane();

                    break;

                case 3:
                    createViewCropPlane();

                    break;

                default:

                    break;
            }
        }

        public static void createBitmap()
        {
            Bitmap bitmap = new Bitmap(500/*this.ClientSize.Width*/, 500/*this.ClientSize.Height*/);

            //System.Drawing.Imaging.BitmapData data = bitmap.LockBits(/*this.ClientRectangle*/new Rectangle(0, 0, 500, 500), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            System.Drawing.Imaging.BitmapData data = bitmap.LockBits(/*this.ClientRectangle*/new Rectangle(0, 0, 500, 500), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);


            Gl.glReadBuffer(Gl.GL_RGB);
            Gl.glReadPixels(0, 0, /* this.ClientSize.Width*/500, 500/*this.ClientSize.Height*/, Gl.GL_RGB, Gl.GL_UNSIGNED_BYTE, data.Scan0);
            Gl.glFinish();

            bitmap.UnlockBits(data);
            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);


            bitmap.Save("slices/sliceEster" + indexBitmap + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            indexBitmap++;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            vectorFormPosition = new Vector2(this.Left, this.Top);
        }

        private void pnlViewPort_MouseDown(object sender, MouseEventArgs e)
        {
            switch (optionView)
            {
                //Código de controle para a visualização no sistema ortogonal
                case 0:                                  

                    if (e.Button == MouseButtons.Left)
                    {
                        rotateInScene = 1;
                    }
                    else
                    {
                        rotateInScene = -1;
                    }
                    break;
                
                //Código de controle para a visualização no sistema livre
                case 1:                                   

                    if (e.Button == MouseButtons.Left)
                    {
                        this.moveInScene = 1;
                    }
                    else
                    {
                        this.moveInScene = -1;
                    }
                    break;
                
                //Código de controle para a visualização no sistema de planos
                case 2:                                 

                    if (e.Button == MouseButtons.Left)
                    {
                        rotateInScene = 1;
                    }
                    else
                    {
                        rotateInScene = -1;
                    }
                    break;

                case 3:

                    if (e.Button == MouseButtons.Left)
                    {
                        MainForm.fixRotate++;
                    }
                    else
                    {
                        MainForm.fixRotate--;
                    }

                    break;
            }
        }

        private void pnlViewPort_MouseUp(object sender, MouseEventArgs e)
        {
            this.moveInScene = 0;

            switch (optionView)
            {
                case 3:
                    if (e.Button == MouseButtons.Left)
                    {
                        MainForm.fixRotate++;
                    }
                    else
                    {
                        MainForm.fixRotate--;
                    }

                    break;
            }
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void pnlViewPort_Paint(object sender, PaintEventArgs e)
        {

        }

        public void createTopMenu()
        {
            mainMenu = new MainMenu();
            this.Menu = mainMenu;

            MenuItem menuArquivo = new MenuItem("&File");
            MenuItem menuNew = new MenuItem("&New");
            menuArquivo.MenuItems.Add(menuNew);
            MenuItem menuOpenImage = new MenuItem("&Open Image");
            menuArquivo.MenuItems.Add(menuOpenImage);
            MenuItem menuSaveBitmap = new MenuItem("&Save Image");
            menuArquivo.MenuItems.Add(menuSaveBitmap);
            mainMenu.MenuItems.Add(menuArquivo);

            MenuItem menuStyleView = new MenuItem("&Visualization");
            MenuItem subMenuOrthoView = new MenuItem("&Orthogonal");
            MenuItem subMenuFree = new MenuItem("&Free");
            MenuItem subMenuOrthoPlane = new MenuItem("&Planes");
            MenuItem subMenuCropView = new MenuItem("&Crop");
            menuStyleView.MenuItems.Add(subMenuOrthoView);
            menuStyleView.MenuItems.Add(subMenuFree);
            menuStyleView.MenuItems.Add(subMenuOrthoPlane);
            menuStyleView.MenuItems.Add(subMenuCropView);
            mainMenu.MenuItems.Add(menuStyleView);

            MenuItem menuConfig = new MenuItem("&Configuration");
            MenuItem subMenuFrustrum = new MenuItem("&Volume");
            menuConfig.MenuItems.Add(subMenuFrustrum);
            MenuItem subMenuVolume = new MenuItem("&Lines and grid");
            menuConfig.MenuItems.Add(subMenuVolume);
            mainMenu.MenuItems.Add(menuConfig);

            menuNew.Click += new System.EventHandler(this.onClickNewSpheres);
            menuSaveBitmap.Click += new System.EventHandler(this.onClickSaveImge);
            menuOpenImage.Click += new System.EventHandler(this.onMenuClickOpenFile);
            subMenuOrthoView.Click += new System.EventHandler(this.onMenuClickOrtho);
            subMenuFree.Click += new System.EventHandler(this.onMenuClickFree);
            subMenuOrthoPlane.Click += new System.EventHandler(this.onMenuClickOrthoPlane);
            subMenuCropView.Click += new System.EventHandler(this.onMenuClickCropPlane);
            subMenuFrustrum.Click += new System.EventHandler(this.onMenuClickVolumeFrustum);
            subMenuVolume.Click += new System.EventHandler(this.onClickVolumeLines);

            menuPanel = new MenuPanel(this.Menu, mainMenu);
        }

        public void createOptionsMenu()
        {
            MenuOptions menuOptions = new MenuOptions(this);
        }

        public void createComponentsInfo()
        {
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(this.onClickKey);

            this.panelMainConponents = new System.Windows.Forms.Panel();
            this.panelMainConponents.SetBounds(535, 75, 445, 500);
            this.panelMainConponents.BackColor = System.Drawing.Color.FromArgb(150, 150, 150);

            //Definição da label para o nome principal da aplicação
            this.labelTitleApplication = new System.Windows.Forms.Label();
            this.labelTitleApplication.Text = "Quantitative Stereology IgorKov";
            this.labelTitleApplication.Font = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
            this.labelTitleApplication.ForeColor = System.Drawing.Color.Black;
            this.labelTitleApplication.SetBounds(10, 10, 400, 25);
            this.panelMainConponents.Controls.Add(labelTitleApplication);

            panelOrtho = new PanelOrtho(10, 50, 425, 440);
            panelOrtho.createPanel(panelMainConponents);
            panelOrtho.setVisible(true);

            panelPlanes = new PanelPlanes(10, 50, 425, 440, false);
            panelPlanes.createPanelPlane(panelMainConponents);

            panelCrop = new PanelCrop(10, 50, 425, 440, panelMainConponents);

            this.Controls.Add(panelMainConponents);
        }

        void onClickKey(object sender, KeyPressEventArgs e)
        {
            Position camera;
            Position center;

            switch (e.KeyChar)
            {
                case 'e':
                    enableFree = false;         //desabilita o mouse para controle na interface de visualização

                    optionView = 0;

                    camera.x = 1.5f;
                    camera.y = 1.0f;
                    camera.z = 1.5f;

                    center.x = 0.0f;
                    center.y = 0.0f;
                    center.z = 0.0f;

                    IntersectSolid.enableOnlySpheres = false;
                    systemStereology.Camara.initCameraOrtho(camera, center);

                    break;

                case 'E':
                    enableFree = false;         //desabilita o mouse para controle na interface de visualização

                    optionView = 0;

                    camera.x = 1.5f;
                    camera.y = 1.0f;
                    camera.z = 1.5f;

                    center.x = 0.0f;
                    center.y = 0.0f;
                    center.z = 0.0f;

                    IntersectSolid.enableOnlySpheres = false;
                    systemStereology.Camara.initCameraOrtho(camera, center);

                    break;

                case 'o':   //visuaização ortogonal

                    optionView = 0;

                    camera.x = 1.5f;
                    camera.y = 1.0f;
                    camera.z = 1.5f;

                    center.x = 0.0f;
                    center.y = 0.0f;
                    center.z = 0.0f;

                    IntersectSolid.enableOnlySpheres = false;
                    systemStereology.Camara.initCameraOrtho(camera, center);

                    break;

                case 'O': //visuaização ortogonal

                    optionView = 0;

                    camera.x = 1.5f;
                    camera.y = 1.0f;
                    camera.z = 1.5f;

                    center.x = 0.0f;
                    center.y = 0.0f;
                    center.z = 0.0f;

                    IntersectSolid.enableOnlySpheres = false;
                    systemStereology.Camara.initCameraOrtho(camera, center);

                    break;

                case 'p':
                    Camera.rotate = 0;
                    MainForm.rotateInScene = 0;     //pausa a rotação da projeção ortogonal

                    break;

                case 'P':
                    Camera.rotate = 0;
                    MainForm.rotateInScene = 0;     //pausa a rotação da projeção ortogonal

                    break;
            }

        }

        /* Este método compreende a visualização do sistema de partículas esféricas no modo ModelView, com
           isso, o usuário poderá navegar pela interface analisando cada partícula com certo grau de liberdade
           nos eixos cartesianos*/
        public void createModelViewScene()
        {
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glPopMatrix();

            Glut.glutInitDisplayMode(Glut.GLUT_SINGLE | Glut.GLUT_RGB);
            Gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

            Gl.glPushMatrix();

            startLight();

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glFlush();
            Gl.glPopMatrix();

            if (enableFree)
            {
                systemStereology.Camara.updateModelView(this.moveInScene);
            }

            systemStereology.drawSceneStereology();

            Winapi.SwapBuffers(hdcDisplay);
        }

        /* Este método compreende a visualização do sistema de partículas esféricas no modo Ortogonal de projeção,
           com isso, o usuário poderá navegar pela interface observando por meio de rotações  o sistema de partículas
           no sistema cartesiano*/
        public void createOrthoViewScene()
        {
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glPopMatrix();

            Glut.glutInitDisplayMode(Glut.GLUT_SINGLE | Glut.GLUT_RGB);
            Gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

            Gl.glPushMatrix();

            startLight();

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Glut.glutInitDisplayMode(Glut.GLUT_SINGLE | Glut.GLUT_RGB);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glEnable(Gl.GL_DEPTH_TEST);

            systemStereology.Camara.updateOrthoView();
            systemStereology.drawSceneStereology();

            Gl.glFlush();
            Gl.glPopMatrix();

            Winapi.SwapBuffers(hdcDisplay);
        }

        /* Este método compreende a visualização do sistema de partículas esféricas no modo ortogonal com o
           aparecimento dos planos criados no sistema 3D*/
        public void createOrthoViewPlane()
        {
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glPopMatrix();

            Glut.glutInitDisplayMode(Glut.GLUT_SINGLE | Glut.GLUT_RGB);
            Gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

            Gl.glPushMatrix();

            startLight();

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Glut.glutInitDisplayMode(Glut.GLUT_SINGLE | Glut.GLUT_RGB);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glEnable(Gl.GL_DEPTH_TEST);

            systemStereology.Camara.updatePlaneView();
            systemStereology.drawSceneStereology();

            Gl.glFlush();
            Gl.glPopMatrix();

            Winapi.SwapBuffers(hdcDisplay);
        }

        /* Este método compreende a visualização do sistema de partículas esféricas por planos cortantes ortogonais*/
        public void createViewCropPlane()
        {

            Glut.glutInitDisplayMode(Glut.GLUT_SINGLE | Glut.GLUT_RGB);
            Gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

            Gl.glPushMatrix();

            startLight();

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Glut.glutInitDisplayMode(Glut.GLUT_SINGLE | Glut.GLUT_RGB);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glEnable(Gl.GL_DEPTH_TEST);

            systemStereology.Camara.updateCropPlaneView();
            systemStereology.drawSceneStereology();

            Gl.glFlush();
            Gl.glPopMatrix();

            Winapi.SwapBuffers(hdcDisplay);
        }

        public void startLight()
        {
            Gl.glEnable(Gl.GL_LIGHT_MODEL_AMBIENT);

            float[] materialAmbient = { Atributes.materialAmbientR, Atributes.materialAmbientG, Atributes.materialAmbientB, Atributes.materialAmbientAlpha };
            float[] materialDiffuse = { Atributes.materialDifuseR, Atributes.materialDifuseG, Atributes.materialDifuseB, Atributes.materialDifuseAlpha};
            float[] materialSpecular = { Atributes.materialSpecularR, Atributes.materialSpecularG, Atributes.materialSpecularB, Atributes.materialSpecularAlpha};
            float[] materialShininess = { Atributes.materialShiness };
            float[] ambientLightPosition = { Atributes.ambientLigthPosX, Atributes.ambientLightPosY, Atributes.ambientLightPosZ};
            float[] lightAmbient = { Atributes.ambientLightX, Atributes.ambientLightY, Atributes.ambientLightZ, 0.5f };

            Gl.glLightModelfv(Gl.GL_LIGHT_MODEL_AMBIENT, materialAmbient);

            Lighting.MaterialAmbient = materialAmbient;
            Lighting.MaterialDiffuse = materialDiffuse;
            Lighting.MaterialSpecular = materialSpecular;
            Lighting.MaterialShininess = materialShininess;
            Lighting.AmbientLightPosition = ambientLightPosition;
            Lighting.LightAmbient = lightAmbient;

            Lighting.SetupLighting();

        }

        //Método para disparar o evento de visualização ortogonal do sistema de esferas
        private void onMenuClickOrtho(object sender, System.EventArgs e)
        {
            this.panelOrtho.setVisible(true);
            this.panelCrop.setVisible(false);
            panelPlanes.setVisible(false);

            enableFree = false;

            Position camera;
            Position center;

            camera.x = 1.5f;            //deifnição da posição de controle da posição ortogonal no eixo x
            camera.y = 1.0f;            //definição da posição de controle da posição ortogonal no eixo y
            camera.z = 1.5f;            //dedinição da posição de controle da posição ortogonal no eixo z

            center.x = 0.0f;            //definição da posição origem do sistema no eixo x
            center.y = 0.0f;            //definição da posição origem do sistema no eixo y
            center.z = 0.0f;            //definição da posição origem do sistema no eixo z

            startLight();               //inicialição do controle de iluminação do sistema

            optionView = 0;             //código de indexação para a projeção ortogonal
            systemStereology.Camara.initCameraOrtho(camera, center);
        }

        //Inicialização do modelo de visualização livre
        private void onMenuClickFree(object sender, System.EventArgs e)
        {
            this.panelOrtho.setVisible(true);
            panelPlanes.setVisible(false);
            this.panelCrop.setVisible(false);

            enableFree = true;

            MainForm.rotateInScene = 0;                             //Desabilita o sistema de visualização livre
            optionView = 1;                                         //código de indexação para a projeção ortogonal
            systemStereology.Camara.initCameraModelView();
        }

        private void onMenuClickCropPlane(object sender, System.EventArgs e)
        {
            this.panelOrtho.setVisible(false);
            panelPlanes.setVisible(false);
            this.panelCrop.setVisible(true);

            enableFree = false;
            enableVolume = true;

            startLight();

            Position camera;
            Position center;

            camera.x = 1.5f;                                                    //deifnição da posição de controle da posição ortogonal no eixo x
            camera.y = 1.0f;                                                    //deifnição da posição de controle da posição ortogonal no eixo Y
            camera.z = 1.5f;                                                    //deifnição da posição de controle da posição ortogonal no eixo Z

            center.x = 0.0f;                                                    //definição da posição origem do sistema no eixo x
            center.y = 0.0f;                                                    //definição da posição origem do sistema no eixo y
            center.z = 0.0f;                                                    //definição da posição origem do sistema no eixo z

            optionView = 3;                                                     //código de indexação para a projeção ortogonal
            systemStereology.Camara.initCameraCropPlane(camera, center);
        }

        //Inicialização do modelo de projeção ortogonal planar
        private void onMenuClickOrthoPlane(object sender, System.EventArgs e)
        {
            panelOrtho.setVisible(false);
            panelPlanes.setVisible(true);
            panelCrop.setVisible(false);

            enableFree = false;
            MainForm.enableVolume = true;

            Position camera;
            Position center;

            optionView = 2;

            camera.x = 1.5f;
            camera.y = 1.0f;
            camera.z = 1.5f;

            center.x = 0.0f;
            center.y = 0.0f;
            center.z = 0.0f;

            startLight();
            systemStereology.Camara.initCameraOrthoPlane(camera, center);
        }

        //........................................................................................

        private void onClickSaveImge(object sender, System.EventArgs e)
        {
            createBitmap();
        }

        private void onMenuClickVolumeFrustum(object sender, System.EventArgs e)
        {
            formVolumeFrustrum = new System.Windows.Forms.Form();
            formVolumeFrustrum.Text = "Volume de visualização";
            formVolumeFrustrum.SetBounds(200, 200, 300, 500);
            formVolumeFrustrum.MaximizeBox = false;

            System.Windows.Forms.Label labelTitle = new System.Windows.Forms.Label();
            labelTitle.Text = "Opção de configuração do volume do display";
            labelTitle.SetBounds(10, 10, 250, 30);
            formVolumeFrustrum.Controls.Add(labelTitle);

            //..................................................................................................

            System.Windows.Forms.Label labelLeft = new System.Windows.Forms.Label();
            labelLeft.Text = "Ajuste Esquerda: ";
            labelLeft.SetBounds(10, 40, 200, 30);
            formVolumeFrustrum.Controls.Add(labelLeft);

            this.scrollFrustrumLeft = new System.Windows.Forms.HScrollBar();
            this.scrollFrustrumLeft.SetBounds(10, 70, 150, 15);
            this.scrollFrustrumLeft.Minimum = -100;
            this.scrollFrustrumLeft.Maximum = 109;
            this.scrollFrustrumLeft.Value = (int)(Atributes.leftFrustrumOrtho * 100.0f); ;
            this.scrollFrustrumLeft.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLeftFrustrum);
            formVolumeFrustrum.Controls.Add(this.scrollFrustrumLeft);

            this.textBoxFrustrumLeft = new System.Windows.Forms.TextBox();
            this.textBoxFrustrumLeft.SetBounds(170, 70, 30, 30);
            this.textBoxFrustrumLeft.Enabled = false;
            this.textBoxFrustrumLeft.Text = (Atributes.leftFrustrumOrtho * 100.0f).ToString();
            this.formVolumeFrustrum.Controls.Add(this.textBoxFrustrumLeft);

            //..................................................................................................

            System.Windows.Forms.Label labelRight = new System.Windows.Forms.Label();
            labelRight.Text = "Ajuste Direita: ";
            labelRight.SetBounds(10, 100, 200, 30);
            formVolumeFrustrum.Controls.Add(labelRight);

            this.scrollFrustrumRigth = new System.Windows.Forms.HScrollBar();
            this.scrollFrustrumRigth.SetBounds(10, 130, 150, 15);
            this.scrollFrustrumRigth.Minimum = -100;
            this.scrollFrustrumRigth.Maximum = 109;
            this.scrollFrustrumRigth.Value = (int)(Atributes.rightFrustrumOrtho * 100.0f); ;
            this.scrollFrustrumRigth.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollRightFrustrum);
            formVolumeFrustrum.Controls.Add(this.scrollFrustrumRigth);

            this.textBoxFrustrumRigth = new System.Windows.Forms.TextBox();
            this.textBoxFrustrumRigth.SetBounds(170, 130, 30, 30);
            this.textBoxFrustrumRigth.Enabled = false;
            this.textBoxFrustrumRigth.Text = (Atributes.rightFrustrumOrtho * 100.0f).ToString();
            formVolumeFrustrum.Controls.Add(this.textBoxFrustrumRigth);

            //..................................................................................................

            System.Windows.Forms.Label labelBottom = new System.Windows.Forms.Label();
            labelBottom.Text = "Ajuste Inferior: ";
            labelBottom.SetBounds(10, 160, 200, 30);
            formVolumeFrustrum.Controls.Add(labelBottom);

            this.scrollFrustrumBottom = new System.Windows.Forms.HScrollBar();
            this.scrollFrustrumBottom.SetBounds(10, 190, 150, 15);
            this.scrollFrustrumBottom.Minimum = -100;
            this.scrollFrustrumBottom.Maximum = 109;
            this.scrollFrustrumBottom.Value = (int)(Atributes.bottomFrustrumOrtho * 100.0f); ;
            this.scrollFrustrumBottom.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollBottomFrustrum);
            formVolumeFrustrum.Controls.Add(this.scrollFrustrumBottom);

            this.textBoxFrustrumBottom = new System.Windows.Forms.TextBox();
            this.textBoxFrustrumBottom.SetBounds(170, 190, 30, 30);
            this.textBoxFrustrumBottom.Enabled = false;
            this.textBoxFrustrumBottom.Text = (Atributes.bottomFrustrumOrtho * 100.0f).ToString();
            formVolumeFrustrum.Controls.Add(this.textBoxFrustrumBottom);

            //..................................................................................................

            System.Windows.Forms.Label labelTop = new System.Windows.Forms.Label();
            labelTop.Text = "Ajuste superior: ";
            labelTop.SetBounds(10, 210, 200, 30);
            formVolumeFrustrum.Controls.Add(labelTop);

            this.scrollFrustrumTop = new System.Windows.Forms.HScrollBar();
            this.scrollFrustrumTop.SetBounds(10, 240, 150, 15);
            this.scrollFrustrumTop.Minimum = -100;
            this.scrollFrustrumTop.Maximum = 109;
            this.scrollFrustrumTop.Value = (int)(Atributes.topFrustrumOrtho * 100.0f); ;
            this.scrollFrustrumTop.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollTopFrustrum);
            formVolumeFrustrum.Controls.Add(this.scrollFrustrumTop);

            textBoxFrustrumTop = new System.Windows.Forms.TextBox();
            textBoxFrustrumTop.SetBounds(170, 240, 30, 30);
            textBoxFrustrumTop.Enabled = false;
            textBoxFrustrumTop.Text = (Atributes.topFrustrumOrtho * 100.0f).ToString();
            formVolumeFrustrum.Controls.Add(textBoxFrustrumTop);

            //..................................................................................................

            System.Windows.Forms.Label labelNear = new System.Windows.Forms.Label();
            labelNear.Text = "Z Near: ";
            labelNear.SetBounds(10, 270, 200, 30);
            formVolumeFrustrum.Controls.Add(labelNear);

            this.scrollFrustrumNear = new System.Windows.Forms.HScrollBar();
            this.scrollFrustrumNear.SetBounds(10, 300, 150, 15);
            this.scrollFrustrumNear.Minimum = 0;
            this.scrollFrustrumNear.Maximum = 109;
            this.scrollFrustrumNear.Value = (int)(Atributes.nearFrustrumOrtho * 100.0f);
            this.scrollFrustrumNear.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollNearFrustrum);
            formVolumeFrustrum.Controls.Add(this.scrollFrustrumNear);

            this.textBoxFrustrumNear = new System.Windows.Forms.TextBox();
            this.textBoxFrustrumNear.SetBounds(170, 300, 30, 30);
            this.textBoxFrustrumNear.Enabled = false;
            this.textBoxFrustrumNear.Text = (Atributes.nearFrustrumOrtho * 100.0f).ToString();
            formVolumeFrustrum.Controls.Add(this.textBoxFrustrumNear);

            //..................................................................................................

            System.Windows.Forms.Label labelFar = new System.Windows.Forms.Label();
            labelFar.Text = "Z Far: ";
            labelFar.SetBounds(10, 330, 200, 30);
            formVolumeFrustrum.Controls.Add(labelFar);

            this.scrollFrustrumFar = new System.Windows.Forms.HScrollBar();
            this.scrollFrustrumFar.SetBounds(10, 360, 150, 15);
            this.scrollFrustrumFar.Minimum = 0;
            this.scrollFrustrumFar.Maximum = 109;
            this.scrollFrustrumFar.Value = (int)(Atributes.farFrustrumOrtho * 100.0f);
            this.scrollFrustrumFar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollFarFrustrum);
            formVolumeFrustrum.Controls.Add(this.scrollFrustrumFar);


            this.textBoxFrustrumFar = new System.Windows.Forms.TextBox();
            this.textBoxFrustrumFar.SetBounds(170, 360, 30, 30);
            this.textBoxFrustrumFar.Enabled = false;
            this.textBoxFrustrumFar.Text = (Atributes.farFrustrumOrtho * 100.0f).ToString();
            this.formVolumeFrustrum.Controls.Add(this.textBoxFrustrumFar);

            formVolumeFrustrum.Show();
        }

        //Método para abrir uma imagem
        private void onMenuClickOpenFile(object sender, System.EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    Bitmap bitmap = new Bitmap(openFileDialog.FileName);

                    //imageUtils = new ImageUtils(bitmap, openFileDialog.FileName);
                    //imageUtils.createInterfaceImageProjection();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("File not found: " + exception);
            }
        }

        public void onScrollLeftFrustrum(object sender, System.EventArgs e)
        {
            textBoxFrustrumLeft.Text = scrollFrustrumLeft.Value.ToString();
            Atributes.leftFrustrumOrtho = (float)scrollFrustrumLeft.Value / 100;
        }

        public void onScrollRightFrustrum(object sender, System.EventArgs e)
        {
            textBoxFrustrumRigth.Text = scrollFrustrumRigth.Value.ToString();
            Atributes.rightFrustrumOrtho = (float)scrollFrustrumRigth.Value / 100;
        }

        public void onScrollBottomFrustrum(object sender, System.EventArgs e)
        {
            textBoxFrustrumBottom.Text = scrollFrustrumBottom.Value.ToString();
            Atributes.bottomFrustrumOrtho = (float)scrollFrustrumBottom.Value / 100;
        }

        public void onScrollTopFrustrum(object sender, System.EventArgs e)
        {
            textBoxFrustrumTop.Text = scrollFrustrumTop.Value.ToString();
            Atributes.topFrustrumOrtho = (float)scrollFrustrumTop.Value / 100;
        }

        public void onScrollNearFrustrum(object sender, System.EventArgs e)
        {
            textBoxFrustrumNear.Text = scrollFrustrumNear.Value.ToString();
            Atributes.nearFrustrumOrtho = (float)scrollFrustrumNear.Value / 100;
        }

        public void onScrollFarFrustrum(object sender, System.EventArgs e)
        {
            textBoxFrustrumFar.Text = scrollFrustrumFar.Value.ToString();
            Atributes.farFrustrumOrtho = (float)scrollFrustrumFar.Value / 100;
        }

        public void onClickVolumeLines(object sender, System.EventArgs e)
        {
            formVolumeLine = new System.Windows.Forms.Form();
            formVolumeLine.SetBounds(100, 100, 300, 150);
            formVolumeLine.Text = "Volume Lines";

            checkVolume = new System.Windows.Forms.CheckBox();
            checkVolume.SetBounds(10, 10, 200, 20);
            checkVolume.Text = "View volume lines";
            checkVolume.Checked = enableVolume;
            checkVolume.Click += new System.EventHandler(this.onCheckVolume);
            formVolumeLine.Controls.Add(checkVolume);

            checkGrid = new System.Windows.Forms.CheckBox();
            checkGrid.SetBounds(10, 30, 300, 20);
            checkGrid.Text = "View Gridlines";
            checkGrid.Checked = enableGrid;
            checkGrid.Click += new System.EventHandler(this.onCheckGrid);
            formVolumeLine.Controls.Add(checkGrid);

            formVolumeLine.ShowDialog();
        }

        public void onCheckVolume(object sender, System.EventArgs e)
        {

            if (checkVolume.Checked == true)
            {
                enableVolume = true;                //ativo as linhas de volume ortogonal

            }
            else
            {
                enableVolume = false;               //desetivo as linhas de volume ortogonal
            }
        }

        public void onCheckGrid(object sender, System.EventArgs e)
        {
            if (checkGrid.Checked == true)
            {
                enableGrid = true;                  //ativo as grades de volume ortogonal

            }
            else
            {
                enableGrid = false;                 //desativo as grades de volume ortogonal
            }

        }

        public void onClickNewSpheres(object sender, System.EventArgs e)
        {
            formCreateNewSystem = new System.Windows.Forms.Form();
            formCreateNewSystem.SetBounds(100, 100, 400, 220);
            formCreateNewSystem.Text = "New Spheres";
            formCreateNewSystem.MaximizeBox = false;


            System.Windows.Forms.Label labelCreate = new System.Windows.Forms.Label();
            labelCreate.Text = "Enter the number of spheres: ";
            labelCreate.SetBounds(10, 10, 350, 20);
            labelCreate.Font = new Font("Arial", 8, FontStyle.Bold);
            formCreateNewSystem.Controls.Add(labelCreate);

            checkTypeSpheres = new System.Windows.Forms.CheckBox();
            checkTypeSpheres.SetBounds(10, 30, 250, 20);
            checkTypeSpheres.Text = "Spheres with different diameters:";
            checkTypeSpheres.Checked = enableChangeDiameter;
            checkTypeSpheres.Click += new System.EventHandler(this.onCheckRadius);
            formCreateNewSystem.Controls.Add(checkTypeSpheres);

            this.scrollQuantSpheres = new System.Windows.Forms.HScrollBar();
            this.scrollQuantSpheres.SetBounds(10, 60, 200, 15);
            this.scrollQuantSpheres.Maximum = 2009;
            this.scrollQuantSpheres.Minimum = 10;
            this.scrollQuantSpheres.Value = 200;
            this.scrollQuantSpheres.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollQuantSpheres);
            formCreateNewSystem.Controls.Add(this.scrollQuantSpheres);

            this.textQuantSpheres = new System.Windows.Forms.TextBox();
            this.textQuantSpheres.SetBounds(220, 60, 35, 20);
            this.textQuantSpheres.Enabled = false;
            this.textQuantSpheres.Text = "200";
            formCreateNewSystem.Controls.Add(this.textQuantSpheres);

            buttonQuantSpheres = new System.Windows.Forms.Button();
            buttonQuantSpheres.Text = "Create Spheres";
            buttonQuantSpheres.SetBounds(10, 100, 150, 50);
            buttonQuantSpheres.Click += new System.EventHandler(this.onClickCreateSpheres);
            formCreateNewSystem.Controls.Add(buttonQuantSpheres);

            formCreateNewSystem.ShowDialog();

            //sistema.CreateScene();
        }

        public void onScrollQuantSpheres(object sender, EventArgs e)
        {
            textQuantSpheres.Text = scrollQuantSpheres.Value.ToString();
        }

        public void onClickCreateSpheres(object sender, EventArgs e)
        {
            Atributes.quantSpheres = Int32.Parse(textQuantSpheres.Text);

            formCreateNewSystem.Dispose();

            systemStereology.CreateScene();

            //Atualização das informações no panel ortogonal
            panelOrtho.getLabelQuantSpheres().Text = "Number of spheres: " + Atributes.quantSpheres.ToString();
            panelOrtho.getLabelVolumnTotal().Text = "Total volume of spheres: " + calculateTotalVolumn() + " (u.v).";
            panelOrtho.getLabelVolumMatrix().Text = "Unit volume: " + calculateTotalMatrix() + " (u.v).";
            panelOrtho.getLabelTotalVolume().Text = "Total volume: " + (calculateTotalVolumn() + calculateTotalMatrix()) + " (u.v)";
        }

        public double calculateTotalVolumn()
        {
            double volume = 0;

            foreach (Spheres esfera in Stereology.listEsferas)
            {
                volume = volume + ((4 / 3) * Math.PI * (Math.Pow(esfera.getRadius(), 3)));
            }

            return volume;
        }

        public double calculateTotalMatrix()
        {
            double volumnMatrix = 0;
            double volumeTotal = 1.0;
            double volumeEsferas;

            volumeEsferas = calculateTotalVolumn();

            volumnMatrix = volumeTotal - volumeEsferas;

            return volumnMatrix;
        }

        public void onCheckRadius(object sender, EventArgs e)
        {
            if (checkTypeSpheres.Checked == true)
            {
                enableChangeDiameter = true;
            }
            else
            {

                if (checkTypeSpheres.Checked == false)
                {
                    enableChangeDiameter = false;
                }
            }
        }


        //...........................ATRIBUTOS DA CLASSE.............................................................

        public int moveInScene;                                     //controle de visualização para o sistema livre
        private uint hdcDisplay;                                    //display para a visualização gráfica OpenGL
        public static Vector2 vectorFormPosition;                   //vetor de controle de posição visual no sistema OpenGL
        private MainMenu mainMenu;                                  //definição do atributo do componente principal da interface
        private Stereology systemStereology = new Stereology();     //definição do ambiente de visulização das esferas

        //Atributos de componentes de interface

        private System.Windows.Forms.Form formCreateNewSystem;      //Interface para a definição e criação de um novo sistema de esferas
        private System.Windows.Forms.Form formVolumeFrustrum;       //Interface para controle de perspectivas de tela
        private System.Windows.Forms.Form formVolumeLine;           //Interface para atribuir linhas de grade na visualização

        private System.Windows.Forms.Label labelTitleApplication;   //Label com a descrição das informações para cada panel do sistema
        private System.Windows.Forms.TextBox textQuantSpheres;      //exibição na caixa de texto a quantidade de esferas a serem criadas
        private System.Windows.Forms.HScrollBar scrollQuantSpheres; //definição da quantidade de esferas no sistema

        private PanelOrtho panelOrtho;                              //Definição da visualização ortogonal
        private PanelCrop panelCrop;                                //Definição da visualização em corte
        private MenuPanel menuPanel;                                //Definição do painel de menu com opções

        //Atributos estáticos do sistema

        public static int optionView = 0;                               //Indexador para opção de visualização do sistema de esferas
        public static int rotateInScene = 0;                            //Controle de rotação nos sistemas de visualização
        public static float fixRotate = 0.0f;                           //Controle de rotação dos sistemas de visualização
        public static bool enableFree = false;                          //Controle lógico para habilitar a visualização livre
        public static bool enableVolume = false;                        //Controle lógico para ativar ou desativar linhas de volume
        public static bool enableGrid = false;                          //Controle lógico para ativar ou desativar grades    
        public static bool enableChangeDiameter = false;                //Controle lógico para ativar ou desativar diferentes esferas

        public static PanelPlanes panelPlanes;
       
        //Atributos para controle de visualização de câmera
        private System.Windows.Forms.TextBox textBoxFrustrumLeft;                   //textBox glFrustrum ajuste esquerda
        private System.Windows.Forms.TextBox textBoxFrustrumRigth;                  //textBox glFrustrum ajuste direite
        private System.Windows.Forms.TextBox textBoxFrustrumBottom;                 //textBox glFrustrum ajuste inferior
        private System.Windows.Forms.TextBox textBoxFrustrumTop;                    //textBox glFrustrum ajuste superior
        private System.Windows.Forms.TextBox textBoxFrustrumNear;                   //textBox glFrustrum ajuste próximo
        private System.Windows.Forms.TextBox textBoxFrustrumFar;                    //textBox glFrustrum ajuste distante

        private System.Windows.Forms.HScrollBar scrollFrustrumLeft;                 //scroll glFrustrum ajuste esquerda
        private System.Windows.Forms.HScrollBar scrollFrustrumRigth;                //scroll glFrustrum ajuste direita
        private System.Windows.Forms.HScrollBar scrollFrustrumBottom;               //scroll glFrustrum ajuste inferior
        private System.Windows.Forms.HScrollBar scrollFrustrumTop;                  //scroll glFrustrum ajuste superior
        private System.Windows.Forms.HScrollBar scrollFrustrumNear;                 //scroll glFrustrum ajuste próximo
        private System.Windows.Forms.HScrollBar scrollFrustrumFar;                  //scroll glFrustrum ajuste distente

        public static System.Windows.Forms.CheckBox checkVolume;                    //ativar ou desativar as linhas de volume
        public static System.Windows.Forms.CheckBox checkGrid;                      //ativar ou desativar as linhas de grade de volume
        public static System.Windows.Forms.CheckBox checkTypeSpheres;               //ativar ou desativar a opção de esferas de diferentes diâmetros
        public static System.Windows.Forms.Button buttonQuantSpheres;               //botão para confirmar a quantidade de esferas a serem criadas

        public static int indexBitmap = 0;
    }
}
