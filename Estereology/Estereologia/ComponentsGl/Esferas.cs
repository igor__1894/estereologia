﻿

using System;
using Tao.OpenGl;
using Stereology.PanelStyles;


namespace Stereology
{
    public class Spheres
    {

        public Spheres(float positionX, float positionY, float positionZ, float radius)
        {
            this.positionX = positionX;
            this.positionY = positionY;
            this.positionZ = positionZ;
            this.radius = radius;
        }

        public Spheres(Position position)
        {
            this.position = position;
        }

        public void createSpheres()
        {
            //Criação das esferas usando a biblioteca OpenGl

            try
            {
                quadratic = Glu.gluNewQuadric();
                Glu.gluQuadricNormals(quadratic, Glu.GLU_SMOOTH);

                list = Gl.glGenLists(1);
                Gl.glNewList(list, Gl.GL_COMPILE);
                Gl.glPushMatrix();

                Gl.glPopMatrix();
                Gl.glEndList();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception openGl: " + exception.ToString());
            }
        }

        public  void paintSpheres()
        {

            Gl.glPushMatrix();

            //Conversão das cores do tipo int para float
            float colorConvertR = (float)PanelOrtho.scrollR.Value/256;
            float colorConvertG = (float)PanelOrtho.scrollG.Value/256;
            float colorConvertB = (float)PanelOrtho.scrollB.Value/256;
            Gl.glColor3f(colorConvertR, colorConvertG, colorConvertB);

            Gl.glTranslatef(positionX, positionY, positionZ);
            Glut.glutSolidSphere(radius, 30, 30);

            Gl.glCallList(list);
            Gl.glPopMatrix();
        }

        public float getPositionX()
        {
            return positionX;
        }

        public float getPositionY()
        {
            return positionY;
        }

        public float getPositionZ()
        {
            return positionZ;
        }

        public float getRadius()
        {
            return radius;
        }

        //........................Atributos da classe sphere.....................

        private int list;                       //lista de conteúdos para rendereização openGl
        private float positionX;                //posição da esfera no eixo x
        private float positionY;                //posição da esfera no eixo y
        private float positionZ;                //posição da esfera no eixo z
        private float radius;                   //raio da esfera

        private Position position;              //posicao absoluta da esfera
        private Glu.GLUquadric quadratic;       //aplicação para a renderização openGl
    }
}
