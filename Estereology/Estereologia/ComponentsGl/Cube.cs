﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.OpenGl;

namespace Stereology.ComponentsGl
{
    public class Cube
    {
        int list;

        public Cube()
        {

        }

        public Cube(int positionX, int positionY, int positionZ, int factor)
        {
            this.positionX = positionX;
            this.positionY = positionY;
            this.positionZ = positionZ;
            this.factor = factor;
        }

        public void create()
        {
            try
            {
                quadratic = Glu.gluNewQuadric();
                Glu.gluQuadricNormals(quadratic, Glu.GLU_SMOOTH);
                list = Gl.glIsList(1);
                Gl.glNewList(list, Gl.GL_COMPILE);
                Gl.glPushMatrix();

                Gl.glPopMatrix();
                Gl.glEnd();
                Gl.glEndList();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception Cube: " + exception.ToString());
            }
        }

        public void paint()
        {
            Gl.glPushMatrix();

            Gl.glColor3f(0.0f, 0.0f, 1.0f);
            Gl.glTranslatef(this.positionX, this.positionY, this.positionZ);
            Glut.glutSolidCube(this.factor);

            Gl.glCallList(list);
            Gl.glPopMatrix();
        }

        private int factor;
        private Glu.GLUquadric quadratic;

        private int positionX;
        private int positionY;
        private int positionZ;

    }
}
