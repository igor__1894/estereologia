﻿using Stereology.MathAlgo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*  Classe responsável pelo gerenciamento e controle dos planos que foram criados
 *  pelo usuário;
 
     */
namespace Stereology.ComponentsGl
{
    public class PlaneUtils
    {

        public PlaneUtils()
        {
            listPlaneEsquation = new List<PlanEquation>();
            staticListPlaneEquation = new List<PlanEquation>();
        }

        public void addPlaneInListEquation(PlanEquation planeEquation)
        {
            this.planeEquation = planeEquation;
            listPlaneEsquation.Add(this.planeEquation);
            staticListPlaneEquation.Add(this.planeEquation);
        }

        public PlanEquation getIndexAtList(int index)
        {
            return listPlaneEsquation[index];
        }

        public List<PlanEquation> getListPlaneEquation()
        {
            return this.listPlaneEsquation;
        }

        public int optionPlane;
        private PlanEquation planeEquation;
        public  List<PlanEquation> listPlaneEsquation;              //lista com todos os planos que foram criados pelo usuário
        public static List<PlanEquation> staticListPlaneEquation;   //lista estática com os planos criados para exibição em console
    }
}
