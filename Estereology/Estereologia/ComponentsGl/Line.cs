﻿using System;
using System.Collections.Generic;
using System.Text;
using Tao.OpenGl;


namespace Stereology
{
    public class Line
    {

        int list;

        public Line()
        {

        }

        public Line(float xi, float yi, float zi, float xf, float yf, float zf)
        {
            this.xi = xi;
            this.yi = yi;
            this.zi = zi;
            this.xf = xf;
            this.yf = yf;
            this.zf = zf; 
        }

        public Line(float xi, float yi, float zi, float xf, float yf, float zf, float r, float g, float b)
        {
            this.xi = xi;
            this.yi = yi;
            this.zi = zi;
            this.xf = xf;
            this.yf = yf;
            this.zf = zf;
            this.r = r;
            this.g = g;
            this.b = b;   
        }

        public void createLine()
        {
            Glu.GLUquadric quadratic = Glu.gluNewQuadric();
            Glu.gluQuadricNormals(quadratic, Glu.GLU_SMOOTH);
            list = Gl.glGenLists(1);
            Gl.glNewList(list, Gl.GL_COMPILE);
            Gl.glPushMatrix();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glColor3f(r, g, b);
            Gl.glVertex3f(xi, yi, zi);
            Gl.glVertex3f(xf, yf, zf);

            Gl.glPopMatrix();
            Gl.glEnd();
            Gl.glEndList();
        }

        public void drawLine()
        {
            Gl.glPushMatrix();
            Gl.glCallList(list);
            Gl.glPopMatrix();
        }

        private float xi;
        private float yi;
        private float zi;
        private float xf;
        private float yf;
        private float zf;
        private float r;
        private float g;
        private float b;
    }
}

