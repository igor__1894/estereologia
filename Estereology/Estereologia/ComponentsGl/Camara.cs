﻿using System;
using System.Collections.Generic;
using System.Text;
using Tao.OpenGl;
using System.Drawing;

namespace Stereology
{
    public class Camera
    {
        const int fixPositionX = 280;
        const int fixPositionY = 350;

        #region Camera constants

        const double div1 = Math.PI / 180;
        const double div2 = 180 / Math.PI;

        #endregion

        public static float cameraViewX, cameraViewY, cameraViewZ;
        public static float cameraOrthoX, cameraOrthoY, cameraOrthoZ;
        public static float cameraOrthoPlaneX, cameraOrthoPlaneY, cameraOrthoPlaneZ;
        public static float cameraCropX, cameraCropY, cameraCropZ;
        public static float centerViewX, centerViewY, centerViewZ;
        public static float centerOrthoX, centerOrthoY, centerOrthoZ;
        public static float centerCropX, centerCropY, centerCropZ;
        public static float centerOrthoPlaneX, centerOrthoPlaneY, centerOrthoPlaneZ;
        public static float rotate = 0;
        public static float forwardSpeed = 0.1f;
        public static float yaw, pitch;
        public static float rotationSpeed = 0.5f;
        public static double i, j, k;
        public static bool blockRotate = false;

        public static float Pitch
        {
            get { return Camera.pitch; }
            set { Camera.pitch = value; }
        }

        public static float Yaw
        {
            get { return Camera.yaw; }
            set { Camera.yaw = value; }
        }

        public void initCameraModelView()
        {
            cameraViewX = 0.0f;
            cameraViewY = 0.0f;
            cameraViewZ = 0.0f;
            centerViewX = 0.0f;
            centerViewY = 0.0f;
            centerViewZ = 0.0f;
            lookModelView();
        }

        public void initCameraOrtho(Position camera, Position center)
        {
            cameraOrthoX = camera.x;
            cameraOrthoY = camera.y;
            cameraOrthoZ = camera.z;
            centerOrthoX = center.x;
            centerOrthoY = center.y;
            centerOrthoZ = center.z;
            lookOrthoView();
        }

        public void initCameraOrthoPlane(Position camera, Position center)
        {
            cameraOrthoPlaneX = camera.x;
            cameraOrthoPlaneY = camera.y;
            cameraOrthoPlaneZ = camera.z;
            centerOrthoPlaneX = center.x;
            centerOrthoPlaneY = center.y;
            centerOrthoPlaneZ = center.z;
            lookOrthoPlaneView();
        }

        public void initCameraCropPlane(Position camera, Position center)
        {
            cameraCropX = camera.x;
            cameraCropY = camera.y;
            cameraCropZ = camera.z;
            centerCropX = center.x;
            centerCropY = center.y;
            centerCropZ = center.z;
            lookCropPlaneView();
        }

        public void lookModelView()
        {
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Glu.gluLookAt(cameraViewX, cameraViewY, cameraViewZ, centerViewX, centerViewY, centerViewZ, 0, 1, 0);
        }

        public void lookOrthoView()
        {
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Glu.gluLookAt(cameraOrthoX, cameraOrthoY, cameraOrthoZ, centerOrthoX, centerOrthoY, centerOrthoZ, 1, 1, 1);

            Gl.glFrustum(Atributes.leftFrustrumOrtho, Atributes.rightFrustrumOrtho, Atributes.bottomFrustrumOrtho, Atributes.topFrustrumOrtho,
                Atributes.nearFrustrumOrtho, Atributes.farFrustrumOrtho);

            Gl.glScalef(Atributes.scaleOrthoX, Atributes.scaleOrthoY, Atributes.scaleOrthoZ);


            if (MainForm.rotateInScene == -1)
            {
                rotate++;
                Gl.glRotatef(rotate, 0, 1, 0);
            }
            else
            {
                if (MainForm.rotateInScene == 1)
                {
                    rotate--;
                    Gl.glRotatef(rotate, 0, 1, 0);
                }
            }
        }

        public void lookOrthoPlaneView()
        {
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Glu.gluLookAt(cameraOrthoX, cameraOrthoY, cameraOrthoZ, centerOrthoX, centerOrthoY, centerOrthoZ, 1, 1, 1);

            Gl.glFrustum(Atributes.leftFrustrumOrtho, Atributes.rightFrustrumOrtho, Atributes.bottomFrustrumOrtho, Atributes.topFrustrumOrtho,
                Atributes.nearFrustrumOrtho, Atributes.farFrustrumOrtho);

            Gl.glScalef(Atributes.scaleOrthoX, Atributes.scaleOrthoY, Atributes.scaleOrthoZ);


            if (MainForm.rotateInScene == -1)
            {
                rotate++;
                Gl.glRotatef(rotate, 0, 1, 0);
            }
            else
            {
                if (MainForm.rotateInScene == 1)
                {
                    rotate--;
                    Gl.glRotatef(rotate, 0, 1, 0);
                }
            }
        }

        public void lookCropPlaneView()
        {
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Glu.gluLookAt(cameraOrthoX, cameraOrthoY, cameraOrthoZ, centerOrthoX, centerOrthoY, centerOrthoZ, 1, 1, 1);

            Gl.glFrustum(Atributes.leftFrustrumOrtho, Atributes.rightFrustrumOrtho, Atributes.bottomFrustrumOrtho, Atributes.topFrustrumOrtho,
                Atributes.nearFrustrumOrtho, Atributes.farFrustrumOrtho);

            Gl.glScalef(Atributes.scaleOrthoX, Atributes.scaleOrthoY, Atributes.scaleOrthoZ);


            Gl.glRotatef(MainForm.fixRotate, 0, 1, 0);
        }

        static public float AnguloARadian(double pAngle)
        {
            return (float)(pAngle * div1);
        }

        static public float RadianAAngulo(double pAngle)
        {
            return (float)(pAngle * div2);
        }

        public void UpdateDirVector()
        {
            k = Math.Cos(AnguloARadian((double)yaw));
            i = -Math.Sin(AnguloARadian((double)yaw));
            j = Math.Sin(AnguloARadian((double)pitch));

            centerViewZ = cameraViewZ - (float)k;
            centerViewX = cameraViewX - (float)i;
            centerViewY = cameraViewY - (float)j;


        }

        public static void CenterMouse()
        {
            Winapi.SetCursorPos(MainForm.formPosition.X + fixPositionX, MainForm.formPosition.Y + fixPositionY);
        }

        public void updateOrthoView()
        {
            lookOrthoView();
        }

        public void updatePlaneView()
        {
            lookOrthoPlaneView();
        }

        public void updateCropPlaneView()
        {
            lookCropPlaneView();
        }

        public void updateModelView(int pressedButton)
        {
            #region Aim camera

            Pointer position = new Pointer();
            Winapi.GetCursorPos(ref position);

            int difX = MainForm.formPosition.X + fixPositionX - position.x;
            int difY = MainForm.formPosition.Y + fixPositionY - position.y;

            if (position.y < fixPositionY)
            {
                pitch -= rotationSpeed * difY / 2;
            }
            else
                if (position.y > fixPositionY)
            {
                pitch += rotationSpeed * -difY / 2;
            }
            if (position.x < fixPositionX)
            {
                yaw += rotationSpeed * -difX / 2;
            }
            else
                if (position.x > fixPositionX)
            {
                yaw -= rotationSpeed * difX / 2;
            }
            UpdateDirVector();
            CenterMouse();


            if (pressedButton == 1)
            {
                cameraViewX -= (float)i * forwardSpeed / 2;
                cameraViewY -= (float)j * forwardSpeed / 2;
                cameraViewZ -= (float)k * forwardSpeed / 2;
            }
            else
                if (pressedButton == -1)
            {
                cameraViewX += (float)i * forwardSpeed / 2;
                cameraViewY += (float)j * forwardSpeed / 2;
                cameraViewZ += (float)k * forwardSpeed / 2;
            }

            /* Visualização das coordenadas no espaço tridimensional*/

            #endregion

            lookModelView();
        }

    }
}
