﻿using Stereology.MathAlgo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.ComponentsGl
{
    //Esta classe contém o esquema - para cada plano eu tenho uma lista com n esferas que interseccionam

    public class PlaneSpheres
    {

        public PlaneSpheres()
        {

        }

        public PlaneSpheres(List<Spheres> listSpheres, PlanEquation planEquation)
        {
            staticListSpheres = listSpheres;
            staticPlanEquation = planEquation;

            this.listSpheres = listSpheres;
            this.planEquation = planEquation;
        }

        public List<Spheres> getSpheres()
        {
            return this.listSpheres;
        }

        private List<Spheres> listSpheres;
        private PlanEquation planEquation;
        public static List<Spheres> staticListSpheres;
        public static PlanEquation staticPlanEquation;
    }
}
