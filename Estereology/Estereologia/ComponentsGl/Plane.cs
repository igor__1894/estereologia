﻿using System;
using System.Collections.Generic;
using System.Text;
using ShadowEngine.OpenGL;
using Tao.OpenGl;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Stereology.MathAlgo;
using Stereology.PanelStyles;

namespace Stereology
{
    class Plane
    {

        int list;

        public Plane()
        {

        }

        // Método para criar os planos
        public void createPlanes()
        {

            Glu.GLUquadric quadratic = Glu.gluNewQuadric();
            Glu.gluQuadricNormals(quadratic, Glu.GLU_FLAT);
            list = Gl.glGenLists(1);
            Gl.glNewList(list, Gl.GL_COMPILE);
            Gl.glPushMatrix();

            Gl.glPopMatrix();
            Gl.glEnd();
            Gl.glEndList();
        }

        public void drawFreePlane(float x0, float y0, float z0, float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3)
        {
            Gl.glPushMatrix();

            Gl.glBegin(Gl.GL_QUADS);
            Gl.glEnable(Gl.GL_DEPTH_TEST);

            Gl.glColor3f(0.0f, 0.0f, 1.0f);

            Gl.glVertex3f(x0, y0, z0);
            Gl.glVertex3f(x1, y1, z1);
            Gl.glVertex3f(x2, y2, z2);
            Gl.glVertex3f(x3, y3, z3);

            Gl.glCallList(list);
            Gl.glPopMatrix();
        }

        public void drawPlane()
        {
            Gl.glPushMatrix();

            Gl.glBegin(Gl.GL_QUADS);

            if (MenuPanel.listIntersect != null & MenuPanel.staticListPlaneEquation.Count != 0)
            {
                float red = (float)MenuPanel.scrollColorPlaneR.Value / 255;
                float green = (float)MenuPanel.scrollColorPlaneG.Value / 255;
                float blue = (float)MenuPanel.scrollColorPlaneB.Value / 255;

                Gl.glColor3f(red, green, blue);

                switch (PlanEquation.optionPlaneStatic)
                {
                    case 1:

                        Gl.glVertex3f(1.0f, -1.0f, (float)(PlanEquation.termoIndenpendente));
                        Gl.glVertex3f(1.0f, 1.0f, (float)(PlanEquation.termoIndenpendente));
                        Gl.glVertex3f(- 1.0f, 1.0f, (float)(PlanEquation.termoIndenpendente));
                        Gl.glVertex3f(- 1.0f,-1.0f, (float)(PlanEquation.termoIndenpendente));

                        break;

                    case 2:

                        Gl.glVertex3f(1.0f,(float)PlanEquation.termoIndenpendente, -1.0f);
                        Gl.glVertex3f(1.0f, (float)PlanEquation.termoIndenpendente, 1.0f);
                        Gl.glVertex3f(-1.0f, (float)PlanEquation.termoIndenpendente, 1.0f);
                        Gl.glVertex3f(-1.0f, (float)PlanEquation.termoIndenpendente, -1.0f);

                        break;

                    case 3:

                        Gl.glVertex3f((float)(-PlanEquation.termoIndenpendente), 1.0f, -1.0f);
                        Gl.glVertex3f((float)(-PlanEquation.termoIndenpendente), 1.0f, 1.0f);
                        Gl.glVertex3f((float)(-PlanEquation.termoIndenpendente), -1.0f, 1.0f);
                        Gl.glVertex3f((float)(-PlanEquation.termoIndenpendente), -1.0f, -1.0f);

                        break;
                }

                /*Gl.glVertex3f(1.0f,(float)PlanEquation.termoIndenpendente, -1.0f);
                Gl.glVertex3f(1.0f,(float)PlanEquation.termoIndenpendente, 1.0f);
                Gl.glVertex3f(-1.0f,(float)PlanEquation.termoIndenpendente, 1.0f);
                Gl.glVertex3f(-1.0f,(float)PlanEquation.termoIndenpendente, -1.0f);*/

                /* for (int i = 0; i < MenuPanel.listIntersect.Count; i++)
                 {
                     float x = (float)MenuPanel.listIntersect[i].getX();
                     float y = (float)MenuPanel.listIntersect[i].getY();
                     float z = (float)MenuPanel.listIntersect[i].getZ();

                     Gl.glVertex3f(x, y, z);

                 }*/

            }

            Gl.glCallList(list);
            Gl.glPopMatrix();
        }


        public static double[] equation1 = { 1.0, 0.0, 0.0, 1.001 };
        public static double[] equation2 = { 0.0, 1.0, 0.0, 1.001 };
        public static double[] equation3 = { 0.0, 0.0, 1.0, 1.001 };
        public static double[] equation4 = { -1.0, 0.0, 0.0, 1.001 };
        public static double[] equation5 = { 0.0, -1.0, 0.0, 1.001 };
        public static double[] equation6 = { 0.0, 0.0, -1.0, 1.0 - 1 };
    }
}
