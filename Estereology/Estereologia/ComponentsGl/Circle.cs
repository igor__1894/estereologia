﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.ComponentsGl
{
    public class Circle
    {
        public Circle()
        {

        }

        public Circle(int indexPlane, int indexSphere, double radio, double area)
        {
            this.indexPlane = indexPlane;
            this.indexSphere = indexSphere;
            this.area = area;
            this.radio = radio;
        }

        public Circle(int indexPlane, int indexSphere, double posX, double posY, double posZ, double radio, double area,  Spheres esfera)
        {
            this.indexPlane = indexPlane;
            this.indexSphere = indexSphere;
            this.esfera = esfera;
            this.area = area;
            this.radio = radio;
            this.posX = posX;
            this.posY = posY;
            this.posZ = posZ;
        }

        public Circle(int indexPlane, int indexSphere, double posX, double posY, double posZ, double radio, double area)
        {
            this.indexPlane = indexPlane;
            this.indexSphere = indexSphere;
            this.area = area;
            this.radio = radio;
            this.posX = posX;
            this.posY = posY;
            this.posZ = posZ;
        }

        public double getArea()
        {
            return this.area;
        }

        public double getRadio()
        {
            return this.radio;
        }

        public int getIndexPlane()
        {
            return this.indexPlane;
        }

        public int getIndexSpheres()
        {
            return this.indexSphere;
        }

        public Spheres getSphere()
        {
            return this.esfera;
        }

        private int indexPlane;
        private int indexSphere;
        private Spheres esfera;
        private double radio;
        private double area;
        private double posX;
        private double posY;
        private double posZ;
    }
}
