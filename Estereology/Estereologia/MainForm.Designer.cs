﻿namespace Stereology
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void initializeComponents()
        {
            this.components = new System.ComponentModel.Container();
            this.panelStereologyGL = new System.Windows.Forms.Panel();
            this.timerPaintComponents = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();

            // 
            // panelStereologyGL
            // 
            this.panelStereologyGL.Location = new System.Drawing.Point(20, 75);
            this.panelStereologyGL.Name = "Stereology";
            this.panelStereologyGL.Size = new System.Drawing.Size(500, 500);
            this.panelStereologyGL.TabIndex = 0;
            this.panelStereologyGL.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlViewPort_Paint);
            this.panelStereologyGL.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlViewPort_MouseDown);
            this.panelStereologyGL.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlViewPort_MouseUp);
            
            // 
            // timerPaintComponents
            // 
            this.timerPaintComponents.Enabled = true;
            this.timerPaintComponents.Interval = 25;
            this.timerPaintComponents.Tick += new System.EventHandler(this.startProcessEstereology);

            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.Controls.Add(this.panelStereologyGL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estereologia Quantitativa";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
        }

        #endregion

        //...........................ATRIBUTOS DA CLASSE.............................................................

        private System.Windows.Forms.Timer timerPaintComponents;    //Timer de controle da interação entre usuário e sistema
        private System.Windows.Forms.Panel panelStereologyGL;       //Panel principal para a visulização gráfica dos componentes em OpenGl     
        private System.Windows.Forms.Panel panelMainConponents;     //Panel principal para controle de componentes na interface do usuário
    }
}

