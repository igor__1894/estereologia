﻿using Stereology.ComponentsGl;
using Stereology.Resources;
using System;
using System.Collections.Generic;


namespace Stereology.MathAlgo
{
    public class SaltykovVolumn
    {
        //public int totalIntervalSaltykov = 12;

        public SaltykovVolumn()
        {
            this.saltykov = new Saltykov();

            feedListCircles();

            //createFreqSectionRadio();
            //createFreqSectionArea();
            //createFreqSectionAreaRadio();

            //calculateTotalNumberSections();
            //testCalcSaltykov();
        }

        public void createListFactor()
        {
            this.listFactor = new List<double>();

            double razao = (double)listSaltyIntervalSectionArea[0].getFrequence() / (double)listCircle.Count;
            double proportionalFactor = 1 / (razao);

            this.proportionalRoot = proportionalFactor;

            //Cálculo dos fatores de proporção para adicionar na lista de cálculo do Saltykov
            for (int i = 0; i < Saltykov.quantClass; i++)
            {

                double relativeFrequence = (double)listSaltyIntervalSectionArea[i].getFrequence() / (double)listCircle.Count;

                double factor = relativeFrequence * (proportionalFactor);

                listFactor.Add(factor);
            }


        }
        
        /*Método com a aplicação recursiva do método de Saltykov para a construção da proporção entre fatores e ponderadores*/
        public List<FactorSecction> recursiveSaltykov(int iterator, int quantIntervalSaltykov, int quantClass, List<FactorSecction> list, FactorSecction factor, double element)
        {
            //Interação onde é construída a lista com as frequências por classe
            if (iterator < quantIntervalSaltykov)
            {
                factor.addElement(quantClass, this.listFactor[iterator], element, (element*this.listFactor[iterator]));
                iterator++;
                return recursiveSaltykov(iterator, quantIntervalSaltykov, quantClass, list, factor, element);
                
            }else
            {
                list.Add(factor);

                if (quantClass < (Saltykov.quantClass-1))
                { 

                    FactorSecction factorInput = new FactorSecction();

                    element = (double) this.listSaltyIntervalSectionArea[quantClass + 1].getFrequence();

                    quantClass++;

                    return recursiveSaltykov(0, quantIntervalSaltykov, quantClass, list, factorInput, element);

                }else
                {
                    return list;
                }
            }
        }

        //Teste da aplicação do método de Saltykov Baseando-se no artigo de Xu - Neutralizando variáveis negativas.
        public void testCalcSaltykov(int quantIntervalSaltykov)
        {
            listSaltykovFactors = new List<FactorSecction>();
            FactorSecction factorSec = new FactorSecction();

            listSaltykovFactors =  recursiveSaltykov(0, quantIntervalSaltykov, 0, listSaltykovFactors, factorSec, (double)this.listSaltyIntervalSectionArea[0].getFrequence());

            int totalSub = 0;
            int diag = 0;
            double adjust = 0;
            double collectorRepair = 0;
            double sumDif = 0;

            this.listQuantSecByDiamSize = new List<double>();

            for (int i = 0; i < listSaltykovFactors.Count; i++)
            {
                double firstValue = listSaltykovFactors[i].getListFactorResult()[0];

                for(int j = totalSub; j > 0; j--)
                {
                    sumDif = sumDif + listSaltykovFactors[diag].getListFactorResult()[j];
                    diag++;
                }

                double result = firstValue - sumDif;

                adjust = result + collectorRepair;

                if (adjust >= 0)
                {
                    collectorRepair = 0;
                    double quantSpheres = (getProportionalRoot() * adjust / ((double)Atributes.quantPlanes)) / (double)getListCircle()[0].getSphere().getRadius();

                    this.listQuantSecByDiamSize.Add(quantSpheres);

                }
                else
                {
                    collectorRepair = adjust;
                    adjust = 0;

                    double quantSpheres = (getProportionalRoot() * adjust / ((double)Atributes.quantPlanes)) / (double)getListCircle()[0].getSphere().getRadius();

                    this.listQuantSecByDiamSize.Add(quantSpheres);
                }

                diag = 0;
                sumDif = 0;
                totalSub++;
            }
        }

        public void feedListCircles()
        {
            
                if (IntersectSolid.listCircle.Count != 0 && IntersectSolid.listPlaneSpheres.Count != 0)
                {
                    listCircle = new List<Circle>();
                    listCircleCollenction = new System.Collections.ArrayList();

                    for(int i = 0; i < IntersectSolid.listCircle.Count; i++)
                    {
                        try
                        {
                            this.listCircle.Add(IntersectSolid.listCircle[i]);
                            this.listCircleCollenction.Add(IntersectSolid.listCircle[i].getArea());
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine("Memória insuficiente");
                        }
                    }

                    listCircleCollenction.Sort();

                    this.maxArea = (double)listCircleCollenction[listCircle.Count - 1];
                    this.minArea = (double)listCircleCollenction[0];
                }
            
          
        }

        public void createFreqSectionAreaRadio()
        {
            System.Collections.ArrayList listRemoveArea = new System.Collections.ArrayList();

            listSaltyIntervalRadiusAreaSection = new List<Interval>();

            listSaltyIntervalRadiusAreaSection = saltykov.createIntervalDiameter();

            for(int i = 0; i < listCircle.Count; i++)
            {
                listRemoveArea.Add(listCircle[i].getArea());
            }

            listRemoveArea.Sort();

            double maxArea = (double)listRemoveArea[listCircle.Count -1];
            
            for(int i = 0; i < listSaltyIntervalRadiusAreaSection.Count; i++)
            {
                for (int j = 0; j < listRemoveArea.Count; j++)
                {
                    double area = (double)listRemoveArea[j] / maxArea;
                    double maxInterval = listSaltyIntervalRadiusAreaSection[i].getIntervalMax();
                    double minInterval = listSaltyIntervalRadiusAreaSection[i].getIntervalMin();

                    if ((area > minInterval) && (area <= maxInterval))
                    {
                        listSaltyIntervalRadiusAreaSection[i].incrementElements();
                    }
                }
            }
        }

        public void createFreqSectionRadio()
        {
            System.Collections.ArrayList listRemoveRadius = new System.Collections.ArrayList();

            listSaltyIntervalRadius = new List<Interval>();

            listSaltyIntervalRadius = saltykov.createIntervalDiameter();

            for (int i = 0; i < listCircle.Count; i++)
            {
                listRemoveRadius.Add(listCircle[i].getRadio());
            }

            listRemoveRadius.Sort();

            double maxRadio = saltykov.findMaxRadio();
            
            for (int i = 0; i < listSaltyIntervalRadius.Count; i++)
            {
                for (int j = 0; j < listRemoveRadius.Count; j++)
                {
                    double radius = (double)listRemoveRadius[j]/maxRadio;
                    double maxInterval = listSaltyIntervalRadius[i].getIntervalMax();
                    double minInterval = listSaltyIntervalRadius[i].getIntervalMin();

                    if ((radius > minInterval) && (radius <= maxInterval))
                    {
                        
                        listSaltyIntervalRadius[i].incrementElements();

                        listRemoveRadius.RemoveAt(j);
                        j = 0;

                    }
                }
            }
        }

        public void createFreqSectionArea()
        {

            listFrequenceSectionArea = new List<double>();
            System.Collections.ArrayList listRemoveArea = new System.Collections.ArrayList();

            listSaltyIntervalSectionArea = new List<Interval>();

            listSaltyIntervalSectionArea = saltykov.createIntervalArea();

            for (int i = 0; i < listCircle.Count; i++)
            {
                listRemoveArea.Add(listCircle[i].getArea());
            }

            listRemoveArea.Sort();

            double maxArea = saltykov.findMaxArea();

            Console.WriteLine("Total de circulos: " + this.listCircle.Count);

            int count = 0;

            for (int i = 0; i < listSaltyIntervalSectionArea.Count; i++)
            {
                double maxInterval = Math.Round(listSaltyIntervalSectionArea[i].getIntervalMax(),15);
                double minInterval = Math.Round(listSaltyIntervalSectionArea[i].getIntervalMin(),15);

                for (int j = 0; j < listRemoveArea.Count; j++)
                {
                    double area = Math.Round((double) listRemoveArea[j] / maxArea,15);

                    //Console.WriteLine(area + " " + maxInterval + " " + minInterval);

                    if ((area >= minInterval) && (area <= maxInterval))
                    {
                        count++;
                        listSaltyIntervalSectionArea[i].incrementElements();
                        
                        //listRemoveArea.RemoveAt(j);
                        //j = 0;
                    }

                }
            }

            Console.WriteLine(count);

            for(int i = 0; i < listSaltyIntervalSectionArea.Count; i++)
            {
                this.listFrequenceSectionArea.Add((double)listSaltyIntervalSectionArea[i].getFrequence()/(double)this.listCircle.Count);
            }
        }

        public void calculateTotalNumberSections()
        {
            int i = 0;//(listSaltyIntervalSectionArea.Count-1);

            double[] constants = {0.0002, 0.0002, 0.0003, 0.0010, 0.0018, 0.0038, 0.0079, 0.0173, 0.0415, 0.1162, 0.4561, 1.6461};
            double relativeFrequence = 0;
            double frequence;
            double soma = 0;
            double volumn = 0;
            double quantParticles = 0;
            double totalParciles = 0;

            List<double> listTest = new List<double>();
            listTest.Add(104);
            listTest.Add(161);
            listTest.Add(253);
            listTest.Add(230);
            listTest.Add(138);
            listTest.Add(69);

            List<double> listTestR = new List<double>();
            listTestR.Add(63.10);
            listTestR.Add(50.12);
            listTestR.Add(39.81);
            listTestR.Add(31.62);
            listTestR.Add(25.12);
            listTestR.Add(19.95);

            //printFrequenceRadiuSection();
            //printFrequenceAreaSection();

            do
            {
                relativeFrequence = listSaltyIntervalSectionArea[i].getFrequence() / (double)listCircle.Count;
                frequence = listSaltyIntervalSectionArea[i].getFrequence();

                volumn = listSaltyIntervalSectionArea[i].getFrequence() * listFactorProp[0];

                // volumn = listTest[i] * constants[11];

                for (int j = 1; j <= i; j++)
                {
                    relativeFrequence = listSaltyIntervalSectionArea[i-j].getFrequence() / (double)listCircle.Count;
                    frequence = listSaltyIntervalSectionArea[i-j].getFrequence();

                    soma = soma + (frequence * listFactorProp[j]);

                    //soma = soma + (frequence * constants[11-j]);
                    //soma = soma + (listTest[i-j] * constants[11-j]);
                }

                quantParticles = (1/1/*((double)listSaltyIntervalRadius[i].getIntervalMax()/10)*/) * (volumn - soma);

                totalParciles = totalParciles + quantParticles;

                soma = 0;

                i ++;

            } while (i < 12);
        }

        public List<Interval> getListSaltyIntervalArea()
        {
            return this.listSaltyIntervalSectionArea;
        }

        public List<Interval> getListSaltyIntervalAreaRadio()
        {
            return this.listSaltyIntervalRadiusAreaSection;
        }

        public List<Interval> getListSaltyIntervalRadius()
        {
            return this.listSaltyIntervalRadius;
        }

        public List<Circle> getListCircle()
        {
            return this.listCircle;
        }

        public List<double> getListQuantParticles()
        {
            return this.listQuantParticles;
        }

        public List<double> getListFracaoVolumn()
        {
            return this.listFracaoVolumn;
        }

        public List<double> getListQuantSecByDiamSize()
        {
            return listQuantSecByDiamSize;
        }

        public double getMaxArea()
        {
            return this.maxArea;
        }

        public List<double> getListFactor()
        {
            return this.listFactor;
        }

        public double getProportionalRoot()
        {
            return this.proportionalRoot;
        }

        public List<double> getListfrequenceSectionArea()
        {
            return this.listFrequenceSectionArea;
        }

        private double maxArea;
        private double minArea;
        private double proportionalRoot;
        private Saltykov saltykov;
        private System.Collections.ArrayList listCircleCollenction;
        private List<Circle> listCircle;
        private List<Interval> listSaltyIntervalRadius;
        private List<Interval> listSaltyIntervalSectionArea;
        private List<Interval> listSaltyIntervalRadiusAreaSection;

        private List<double> listFactor;
        private List<double> listFactorProp;
        private List<double> listQuantParticles;                    //listagem com a quantidade de partículas   (teste)
        private List<double> listFracaoVolumn;                      //listagem da fração volumétrica            (teste)
        private List<double> listQuantSecByDiamSize;                //Lista da quantidade de secções por diâmetro máximo das classes N(25,25), N(24,24) ...
        private List<double> listFrequenceSectionArea;              //Lista com a quantidade secções de esfera por intervalo de área

        public static List<FactorSecction> listSaltykovFactors;

        public static List<double> listSizeK0;
        public static List<double> listSizeK1;
        public static List<double> listSizeK2;
        public static List<double> listSizeK3;
        public static List<double> listSizeK4;
        public static List<double> listSizeK5;
        public static List<double> listSizeK6;
        public static List<double> listSizeK7;
        public static List<double> listSizeK8;
        public static List<double> listSizeK9;
        public static List<double> listSizeK10;
        public static List<double> listSizeK11;
    }
}
