﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//http://csharp.net-informations.com/gui/cs-listview.htm
//Classe utilizada para os cálculos que envolvem planos no espaço

namespace Stereology.MathAlgo
{
    public class PlanEquation
    {

        public PlanEquation()
        {
            listPoint = new List<PointLocation>();
        }

        public PlanEquation(int index)
        {
            this.index = index;
            listPoint = new List<PointLocation>();
        }

        public PlanEquation(int index, int optionPlane)
        {
            this.index = index;
            this.optionPlane = optionPlane;
            optionPlaneStatic = optionPlane;

            listPoint = new List<PointLocation>();
        }

        public void setIndexOptionPlane(int optionPlane)
        {
            this.optionPlane = optionPlane;
            optionPlaneStatic = optionPlane;
        }

        public int getIndexOptionPlane()
        {
            return this.optionPlane;
        }

        public void createPerpRandomPointSerieA(Random random1, Random random2, float minRange, float maxRange)
        {
            double maxRangeP = 0.99;
            double minRangeP = -0.99;
            double linearIndp;

            Random random = new Random();
            linearIndp = random1.NextDouble() * (maxRangeP - minRangeP) + minRangeP;

            pointLocationX = 0.5f;
            pointLocationY = 0.5f;
            pointLocationZ = linearIndp;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

            pointLocationX = -0.5f;
            pointLocationY = -0.5;
            pointLocationZ = linearIndp;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

            pointLocationX = -0.5f;
            pointLocationY = 0.5f;
            pointLocationZ = linearIndp;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

        }

        public void createPerpRandomPointSerieB(Random random1, Random random2, float minRange, float maxRange)
        {
            double maxRangeP = 0.99;
            double minRangeP = -0.99;
            double linearIndp;

            Random random = new Random();
            linearIndp = random1.NextDouble() * (maxRangeP - minRangeP) + minRangeP;

            pointLocationZ = 0.5f;
            pointLocationX = 0.5f;
            pointLocationY = linearIndp;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

            pointLocationZ = -0.5f;
            pointLocationX = -0.5f;
            pointLocationY = linearIndp;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

            pointLocationZ = -0.5f;
            pointLocationX = 0.5f;
            pointLocationY = linearIndp;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

        }

        public void createPerpRandomPointSerieC(Random random1, Random random2, float minRange, float maxRange)
        {
            double maxRangeP = 0.99;
            double minRangeP = -0.99;
            double linearIndp;

            Random random = new Random();
            linearIndp = random1.NextDouble() * (maxRangeP - minRangeP) + minRangeP;

            pointLocationZ = 0.5f;
            pointLocationX = linearIndp;
            pointLocationY = 0.5f;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

            pointLocationZ = -0.5f;
            pointLocationX = linearIndp;
            pointLocationY = -0.5f;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

            pointLocationZ = -0.5f;
            pointLocationX = linearIndp;
            pointLocationY = 0.5f;
            listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));

        }

        public void createRandomPointSerie(Random random1, Random random2, float minRange, float maxRange)
        {
            double range1;
            double range2;
            //Com três pontos eu determino dois vetores e consequentemente o plano
            for (int i = 0; i < 3; i++)
            {
                range1 = random1.NextDouble() * (maxRange - minRange) + minRange;
                range2 = random2.NextDouble() * (maxRange - minRange) + minRange;
                while (Math.Sqrt(-2 * (Math.Log(range2))) * Math.Cos(2 * Math.PI * range2) >= 1 ||
                       Math.Sqrt(-2 * (Math.Log(range1))) * Math.Cos(2 * Math.PI * range2) <= -1)
                {
                    range1 = minRange + (random1.NextDouble() * maxRange);
                    range2 = minRange + (random2.NextDouble() * maxRange);
                }
                pointLocationX = Math.Sqrt(-2 * (Math.Log(range1))) * Math.Cos(2 * Math.PI * range2);


                range1 = random1.NextDouble() * (maxRange - minRange) + minRange;
                range2 = random2.NextDouble() * (maxRange - minRange) + minRange;
                while (Math.Sqrt(-2 * (Math.Log(range2))) * Math.Cos(2 * Math.PI * range2) >= 1 ||
                       Math.Sqrt(-2 * (Math.Log(range1))) * Math.Cos(2 * Math.PI * range2) <= -1)
                {
                    range1 = minRange + (random1.NextDouble() * maxRange);
                    range2 = minRange + (random2.NextDouble() * maxRange);
                }
                pointLocationY = Math.Sqrt(-2 * (Math.Log(range1))) * Math.Cos(2 * Math.PI * range2);


                range1 = random1.NextDouble() * (maxRange - minRange) + minRange;
                range2 = random2.NextDouble() * (maxRange - minRange) + minRange;
                while (Math.Sqrt(-2 * (Math.Log(range2))) * Math.Cos(2 * Math.PI * range2) >= 1 ||
                       Math.Sqrt(-2 * (Math.Log(range1))) * Math.Cos(2 * Math.PI * range2) <= -1)
                {
                    range1 = minRange + (random1.NextDouble() * maxRange);
                    range2 = minRange + (random2.NextDouble() * maxRange);
                }
                pointLocationZ = Math.Sqrt(-2 * (Math.Log(range1))) * Math.Cos(2 * Math.PI * range2);

                listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));
            }

        }

        public void createRandomPoints(Random random, float minRange, float maxRange)
        { 
            //Com três pontos eu determino dois vetores e consequentemente o plano
            for (int i = 0; i < 3; i++)
            {
                pointLocationX = minRange + (random.NextDouble() * maxRange);
                pointLocationY = minRange + (random.NextDouble() * maxRange);
                pointLocationZ = minRange + (random.NextDouble() * maxRange);

                listPoint.Add(new PointLocation(pointLocationX, pointLocationY, pointLocationZ));
            }
        }

        public String getPlanEquation()
        {

            vectorAB = new VectorEquation(listPoint[0], listPoint[1]);       //construção do primeiro vetor LD ao plano
            vectorAC = new VectorEquation(listPoint[0], listPoint[2]);       //construção do segundo vetor LD ao plano

            double determinante1 = (vectorAB.getY() * vectorAC.getZ()) - (vectorAB.getZ() * vectorAC.getY());
            double determinante2 = (vectorAB.getX() * vectorAC.getZ()) - (vectorAB.getZ() * vectorAC.getX());
            double determinante3 = (vectorAB.getX() * vectorAC.getY()) - (vectorAB.getY() * vectorAC.getX());

            double linear1 = -1 * (determinante1 * listPoint[0].getX());
            double linear2 = (determinante2 * listPoint[0].getY());
            double linear3 = -1 * (determinante3 * listPoint[0].getZ());

            double varIdependente = (linear1) + (linear2) + (linear3);


            double coeficienteX = determinante1;
            double coeficienteY = -1 * determinante2;
            double coeficienteZ = determinante3;

            PlanEquation.coeficienteX = coeficienteX;
            PlanEquation.coeficienteY = coeficienteY;
            PlanEquation.coeficienteZ = coeficienteZ;
            PlanEquation.termoIndenpendente = varIdependente;

            this.coeficientePlaneX = coeficienteX;
            this.coeficientePlaneY = coeficienteY;
            this.coeficientePlaneZ = coeficienteZ;
            this.termoIndependentePlane = varIdependente;

            planEquation = this.coeficientePlaneX.ToString("0.0000") + ".x " + "+ " + this.coeficientePlaneY.ToString("0.000000") + ".y " + "+ " + this.coeficientePlaneZ.ToString("0.0000") + ".z " + "+ " + this.termoIndependentePlane.ToString("0.0000") + " = 0";

            return planEquation;
        }

        public VectorEquation getVectorAB()
        {
            return vectorAB;
        }

        public VectorEquation getVectorAC()
        {
            return vectorAC;
        }

        public double getCoeficienteX()
        {
            return this.coeficientePlaneX;
        }

        public double getCoeficienteY()
        {
            return this.coeficientePlaneY;
        }

        public double getCoeficienteZ()
        {
            return this.coeficientePlaneZ;
        }

        public double getTermoIndenpendente()
        {
            return this.termoIndependentePlane;
        }

        public int getIndex()
        {
            return this.index;
        }

        //Para a construção do plano é necessário usar 3 pontos para a construção de dois vetores
        private VectorEquation vectorAB;                //primeiro vetor para ser usado na equação do plano
        private VectorEquation vectorAC;                //segundo vetor para ser usado na equação do plano

        public static List<PointLocation> listPoint;    //lista de pontos no espaço
        private int index;
        private int optionPlane;
        private double pointLocationX;
        private double pointLocationY;
        private double pointLocationZ;
        private String planEquation;

        private double coeficientePlaneX;
        private double coeficientePlaneY;
        private double coeficientePlaneZ;
        private double termoIndependentePlane;

        public static int optionPlaneStatic;
        public static double coeficienteX;
        public static double coeficienteY;
        public static double coeficienteZ;
        public static double termoIndenpendente;
    }
}
