﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.MathAlgo
{
    public class PointLocation
    {
        public PointLocation()
        {

        }

        public PointLocation(double pointX, double pointY, double pointZ)
        {
            this.coordX = pointX;
            this.coordY = pointY;
            this.coordZ = pointZ;
        }

        public double getX()
        {
            return coordX;
        }

        public double getY()
        {
            return coordY;
        }

        public double getZ()
        {
            return coordZ;

        }

        private double coordX;
        private double coordY;
        private double coordZ;
    }
}
