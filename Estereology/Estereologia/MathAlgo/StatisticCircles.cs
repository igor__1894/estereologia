﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.MathAlgo
{
    public class StatisticCircles
    {
        public StatisticCircles()
        {

        }

        public double maxRadiusCircle()
        {
            double maxRadius;

            listCircleRadius = new ArrayList();

            try
            {

                if (IntersectSolid.listCircle.Count > 0)
                {
                    for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                    {
                        listCircleRadius.Add(IntersectSolid.listCircle[i].getRadio());
                    }

                    listCircleRadius.Sort();

                    String strRadius = listCircleRadius[listCircleRadius.Count - 1].ToString();

                    maxRadius = Double.Parse(strRadius);

                    return maxRadius;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception statistic circle: " + exception);
            }

            return 0;
        }

        public double minRadiusCircle()
        {
            double minRadius;

            listCircleRadius = new ArrayList();

            try
            {
                if (IntersectSolid.listCircle.Count > 0)
                {
                    for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                    {
                        listCircleRadius.Add(IntersectSolid.listCircle[i].getRadio());
                    }

                    listCircleRadius.Sort();

                    String strRadius = listCircleRadius[0].ToString();

                    minRadius = Double.Parse(strRadius);

                    return minRadius;
                }

            }
            catch (Exception exception)
            {
                Console.WriteLine("Excpetion statistic circle: " + exception);
            }

            return 0;
        }

        public int getQuantClass()
        {
            int quantClass = Convert.ToInt32(Math.Sqrt(IntersectSolid.listCircle.Count));

            return quantClass;
        }

        public double maxAreaCircle()
        {
            double maxArea;

            listCircleArea = new ArrayList();

            try
            {
                if (IntersectSolid.listCircle.Count > 0)
                {
                    for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                    {
                        listCircleArea.Add(IntersectSolid.listCircle[i].getArea());
                    }

                    listCircleArea.Sort();

                    String strArea = listCircleArea[listCircleArea.Count - 1].ToString();

                    maxArea = Double.Parse(strArea);

                    return maxArea;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception max area: " + exception);
            }

            return 0;
        }

        public double minAreaCircle()
        {
            double minArea;

            listCircleArea = new ArrayList();

            try
            {
                if (IntersectSolid.listCircle.Count > 0)
                {
                    for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                    {
                        listCircleArea.Add(IntersectSolid.listCircle[i].getArea());
                    }

                    listCircleArea.Sort();

                    String strArea = listCircleArea[0].ToString();

                    minArea = Double.Parse(strArea);

                    return minArea;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception min area: " + exception);
            }

            return 0;
        }

        public double calcAmplitudeArea()
        {
            int quantClass = getQuantClass();
            double amplitude;
            double maxArea = maxAreaCircle();
            double minArea = minAreaCircle();

            try
            {
                if (quantClass != 0)
                {
                    double interval = Math.Abs(maxArea - minArea);

                    amplitude = interval / quantClass;

                    return amplitude;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception amplitude area: " + exception.ToString());
            }

            return 0;

        }

        public double calcAmplitudeRadius()
        {
            int quantClass = getQuantClass();
            double amplitude;
            double maxRadius = maxRadiusCircle();
            double minRadius = minRadiusCircle();

            try
            {
                if (quantClass != 0)
                {
                    if (maxRadius != minRadius)
                    {
                        double interval = Math.Abs(maxRadius - minRadius);

                        amplitude = interval / quantClass;

                        return amplitude;
                    }

                }
                else
                {
                    Console.WriteLine("Não existem classes para as circunferências");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception.ToString());
            }

            return 0;
        }

        private ArrayList listCircleRadius;
        private ArrayList listCircleArea;
    }
}
