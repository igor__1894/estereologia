﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estereology.MathAlgo
{
    class Statistic
    {

        public Statistic()
        {
        }

        public double maxRadiusSphere()
        {
            double maxRadius;

            listSpheresRadio = new ArrayList();

            for (int i = 0; i < Estereology.listEsferas.Count; i++)
            {
                listSpheresRadio.Add(Estereology.listEsferas[i].getRadio());
            }

            //Organiza a liste em ordem crescente para os diferentes raios das esferas
            listSpheresRadio.Sort();

            String strRadius = listSpheresRadio[listSpheresRadio.Count - 1].ToString();
            maxRadius = Double.Parse(strRadius);

            return maxRadius;
        }

        public double minRadiusSphere()
        {
            double minRadius;

            listSpheresRadio = new ArrayList();

            for(int i = 0; i < Estereology.listEsferas.Count; i++)
            {
                listSpheresRadio.Add(Estereology.listEsferas[i].getRadio());
            }

            listSpheresRadio.Sort();

            String strRadius = listSpheresRadio[0].ToString();
            minRadius = Double.Parse(strRadius);

            return minRadius;
        }

        //Retorna a quantidade de classes que devem ter o histograma (intervalo)
        public int getQuantClass()
        {
            int quantClass = Convert.ToInt32(Math.Sqrt(Estereology.listEsferas.Count));

            return quantClass;
        }

        public double calcAmplitude()
        {
            int quantClass = getQuantClass();
            double amplitude;
            double maxRadius = maxRadiusSphere();
            double minRadius = minRadiusSphere();

            try
            {
                if (quantClass != 0)
                {
                    if (maxRadius != minRadius)
                    {
                        double interval = Math.Abs(maxRadius - minRadius);

                        amplitude = interval / quantClass;

                        return amplitude;

                    }else
                    {

                        Console.WriteLine("Todas as esferas possuem o mesmo raio");
                    }
                   
                }

            }
            catch (Exception exception)
            {
                Console.WriteLine("Erro aritimético: " + exception);

            }

            return 0;
        }

        private ArrayList listSpheresRadio;
    }
}
