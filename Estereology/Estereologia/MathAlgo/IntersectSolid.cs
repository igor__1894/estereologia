﻿using Stereology.ComponentsGl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*Classe utilizada para procurar verificar as intersecções entre
  os planos e as esferas dispostas no espço. Atribuição do cálculo
  da área das circunferências que estão contidas no plano*/

namespace Stereology.MathAlgo
{
    public class IntersectSolid
    {
        public IntersectSolid()
        {

        }

        public IntersectSolid(List<Spheres> listSpheres, List<PlanEquation> listPlanes){

            this.listSpheres = listSpheres;
            this.listPlanes = listPlanes;

            listPlaneSpheres = new List<PlaneSpheres>();        //Lista de esferas interseccionadas
            listCircle = new List<Circle>();                    //Lista da secção secante que formam círculos no plano

            //showListSpheresConsole();
            //showListPlanesConsole();
         
            calcIntersectSpherePlane();
        }

        //Método para realizar o cálculo da intersecção entre as esferas e o plano no espaço
        public void calcIntersectSpherePlane()
        {
            if(this.listPlanes != null && this.listSpheres != null)
            {
                //Console.WriteLine(listSpheres[0].getPositionX() + " " + listSpheres[0].getPositionY() + listSpheres[0].getPositionZ() + " " + listSpheres[0].getRadio());

                for (int i = 0; i < listPlanes.Count; i++)
                {
                    double a = listPlanes[i].getCoeficienteX();
                    double b = listPlanes[i].getCoeficienteY();
                    double c = listPlanes[i].getCoeficienteZ();
                    double d = listPlanes[i].getTermoIndenpendente();

                    listSpheresIntersect = new List<Spheres>();

                    for (int j = 0; j < listSpheres.Count; j++)
                    {
                        double centerX = listSpheres[j].getPositionX();
                        double centerY = listSpheres[j].getPositionY();
                        double centerZ = listSpheres[j].getPositionZ();
                        double raio = listSpheres[j].getRadius();

                        double parametrizador;

                        try
                        {
                            if ((Math.Pow(a, 2) + Math.Pow(b, 2) + Math.Pow(c, 2)) != 0)
                            {
                                parametrizador = ((-1) * ((a * centerX) + (b * centerY) + (c * centerZ) + (d))) / (Math.Pow(a, 2) + Math.Pow(b, 2) + Math.Pow(c, 2));

                                //Posições da circunferência condida no plano se existir
                                double posX = centerX + (a * parametrizador);
                                double posY = centerY + (b * parametrizador);
                                double posZ = centerZ + (c * parametrizador);

                                //Distância entre o centro P da circunferência e o centro C da esfera
                                double distancia = Math.Sqrt(Math.Pow((centerX - posX), 2) +
                                                             Math.Pow((centerY - posY), 2) +
                                                             Math.Pow((centerZ - posZ), 2));

                                //Raio da circunferência pela intersecção da esferas com o plano

                                if (distancia < raio)
                                {
                                    double raioCircle = Math.Sqrt(Math.Pow(raio, 2) - Math.Pow(distancia, 2));

                                    //Esferas que interseccionam
                                    listSpheresIntersect.Add(listSpheres[j]);

                                    double area = Math.PI * Math.Pow(raioCircle, 2);

                                    listCircle.Add(new Circle(i, j, posX, posY, posZ, raioCircle, area, listSpheres[j]));
                                }
                                else
                                {

                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine("Exception: " + exception.ToString());
                        }
                    }

                    listPlaneSpheres.Add(new PlaneSpheres(listSpheresIntersect, listPlanes[i]));
                }

            }else
            {
                Console.WriteLine("Alguma lista para o cálculo das intersecções está vazia, por favor verificar");
            }

        }

        public void showListSpheresConsole()
        {
            for(int i = 0; i < listSpheres.Count; i++)
            {
                Console.WriteLine("Esfera (" + i + "): " + listSpheres[i].getPositionX() + " "
                                                         + listSpheres[i].getPositionY() + " "
                                                         + listSpheres[i].getPositionZ());
            }
        }

        public void showListPlanesConsole()
        {
            for(int i = 0; i < listPlanes.Count; i++)
            {
                Console.WriteLine("Plano: " + listPlanes[i].getCoeficienteX() + " "
                                            + listPlanes[i].getCoeficienteY() + " "
                                            + listPlanes[i].getCoeficienteZ() + " "
                                            + listPlanes[i].getTermoIndenpendente());
            }
        }

        private List<Spheres> listSpheres;
        private List<PlanEquation> listPlanes;
        private List<Spheres> listSpheresIntersect;

        public static List<PlaneSpheres> listPlaneSpheres;
        public static List<Circle> listCircle;
        public static bool enableOnlySpheres = false;
    }
}
