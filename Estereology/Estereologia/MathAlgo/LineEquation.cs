﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Definição das equações de reta

namespace Stereology.MathAlgo
{
    public class LineEquation
    {
        public LineEquation()
        {

        }

        public LineEquation(PointLocation pointA, PointLocation pointB)
        {
            this.pointA = pointA;
            this.pointB = pointB;

            createLineEquation();
        }

        public void createLineEquation()
        {
            vectorEquation = new VectorEquation(this.pointA, this.pointB);
        }

        public VectorEquation getVector()
        {
            return this.vectorEquation;
        }

        public PointLocation getPointA()
        {
            return this.pointA;
        }

        public PointLocation getPointB()
        {
            return this.pointB;
        }

        private VectorEquation vectorEquation;
        private PointLocation pointA;
        private PointLocation pointB;
    }
}
