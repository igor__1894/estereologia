﻿using Stereology.PanelStyles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.MathAlgo
{
    public class Saltykov
    {
        public Saltykov()
        {
           
        }

        //Método para criar o intervalo dos diâmetros
        public List<Interval> createIntervalDiameter()
        {
            double max = 1.0;
            double min = 0;
            double pow = 0.1;

            listIntervalDiameter = new List<Interval>();

            min = Math.Pow(10, -pow);
            listIntervalDiameter.Add(new Interval(max, min));

            for(int i = 1; i < quantClass; i++)
            {
                pow = pow + 0.1;
                max = min;
                min = Math.Pow(10, -pow);

                listIntervalDiameter.Add(new Interval(max, min));
            }

            return listIntervalDiameter;

        }

    
        public double findMaxRadio()
        {
            try
            {

                ArrayList listRadio = new ArrayList();

                for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                {
                    listRadio.Add(IntersectSolid.listCircle[i].getRadio());
                }

                listRadio.Sort();

                return (double)listRadio[listRadio.Count - 1];
            }
            catch (Exception exception)
            {
                Console.WriteLine("Nenhuma intersecação encontrada");
                PanelDialog panelDialog = new PanelDialog("Erro de intersecção", "Não foi possível encontrar intersecções com as esferas, \r\n verifique se os planos foram criados.");
            }
            
            return 0;
        }

        public double findMaxArea()
        {
            ArrayList listArea = new ArrayList();
            
            for(int i = 0; i < IntersectSolid.listCircle.Count; i++)
            {
                listArea.Add(IntersectSolid.listCircle[i].getArea());
            }

            listArea.Sort();

            return (double)listArea[listArea.Count - 1];
        }

        //Método para criar o intervalo da área
        public List<Interval> createIntervalArea()
        {
            double max = 1.0;
            double min = 0;
            double pow = 0.2;

            listIntervalArea = new List<Interval>();

            min = Math.Pow(10, -pow);
            listIntervalArea.Add(new Interval(max, min));

            for (int i = 1; i < quantClass; i++)
            {
                pow = pow + 0.2;
                max = min;
                min = Math.Pow(10, -pow);

                listIntervalArea.Add(new Interval(max, min));
            }

            return listIntervalArea;

        }

        public List<Interval> listIntervalDiameter;
        public List<Interval> listIntervalArea;
        public static int quantClass = 20;
    }
}
