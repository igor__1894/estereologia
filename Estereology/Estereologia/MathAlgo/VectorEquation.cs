﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.MathAlgo
{
    public class VectorEquation
    {

        public VectorEquation()
        {

        }

        public VectorEquation(PointLocation pointA, PointLocation pointB)
        {
            this.pointA = pointA;
            this.pointB = pointB;
            createVector();
        }

        public void createVector()
        {
            locationX = pointB.getX() - pointA.getX();
            locationY = pointB.getY() - pointA.getY();
            locationZ = pointB.getZ() - pointA.getZ();

            //Console.WriteLine("Vetor: " + "(" + locationX + ", " + locationY + ", " + locationZ + ")");
        }

        public double getX()
        {
            return locationX;
        }

        public double getY()
        {
            return locationY;
        }

        public double getZ()
        {
            return locationZ;
        }

        private double locationX;
        private double locationY;
        private double locationZ;
        private PointLocation pointA;
        private PointLocation pointB;
    }
}
