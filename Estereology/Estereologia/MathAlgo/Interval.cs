﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.MathAlgo
{
    public class Interval
    {
        public Interval()
        {

        }

        public Interval(double minInterval, double maxInterval)
        {
            this.coutElements = 0;
            this.minInterval = minInterval;
            this.maxInterval = maxInterval;
        }

        public double getIntervalMax()
        {
            return this.minInterval;
        }

        public double getIntervalMin()
        {
            return this.maxInterval;
        }

        public void incrementElements()
        {
            this.coutElements++;
        }

        public int getFrequence()
        {
            return this.coutElements;
        }

        private int coutElements;               //Contador de objetos inseridos na classe de intervalo definida
        private double minInterval;
        private double maxInterval;
    }
}
