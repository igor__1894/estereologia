﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stereology
{
    class Atributes
    {
        /*  Classe utilizada para atribuição direta de valores referentes à aplicação
         *  - Controle de escala
         *  - Controle de translação
         *  - Controle das esferas
         */

        //http://pt.stackoverflow.com/questions/124576/encontrar-o-centro-de-cada-circulo-na-imagem
        //http://stackoverflow.com/questions/24292200/what-are-the-differences-between-houghcircles-in-emgucv-and-opencv
        //http://stackoverflow.com/questions/17299134/how-can-i-get-the-center-of-a-circlef-in-emgu-c
           
        public static float scaleX = 1.0f;                     //escala de visão no eixo X
        public static float scaleY = 1.0f;                     //escala de visão no eixo Y
        public static float scaleZ = 1.0f;                    //escala de visão no eixo Z

        public static float scaleOrthoX = 1.0f;                //escala de visão para o eixo X ortogonal
        public static float scaleOrthoY = 1.0f;                //escala de visão para o eixo Y ortogonal
        public static float scaleOrthoZ = 1.0f;                //escala de visão para o eixo Z ortogonal

        //GlFrustrum params
        public static float leftFrustrumOrtho = 0.0f;
        public static float rightFrustrumOrtho = 0.0f;
        public static float bottomFrustrumOrtho = 0.0f;
        public static float topFrustrumOrtho = 0.0f;
        public static float nearFrustrumOrtho = 0.0f;
        public static float farFrustrumOrtho = 0.0f;

        //Ligth params
        public static float materialAmbientR = 0.5f;
        public static float materialAmbientG = 0.5f;
        public static float materialAmbientB = 0.5f;
        public static float materialAmbientAlpha = 0.0f;

        public static float materialDifuseR = 0.5f;
        public static float materialDifuseG = 0.5f;
        public static float materialDifuseB = 0.5f;
        public static float materialDifuseAlpha = 0.0f;

        public static float materialSpecularR = 0.5f;
        public static float materialSpecularG = 0.5f;
        public static float materialSpecularB = 0.5f;
        public static float materialSpecularAlpha = 0.0f;


        public static float materialShiness = 0.5f;

        public static float ambientLigthPosX = 0.5f;
        public static float ambientLightPosY = 0.5f;
        public static float ambientLightPosZ = 0.5f;

        public static float ambientLightX = 0.5f;
        public static float ambientLightY = 0.5f;
        public static float ambientLightZ = 0.5f;
        public static float ambientLightAlpha = 0.0f;

        //Plane color
        public static float colorPlaneRed = 100.0f;
        public static float colorPlaneGreen = 100.0f;
        public static float colorPlaneBlue = 255.0f;

        //Sphere radius atribute
        public static float radiusSphere = 0.02f;

        //Valores de ajusto do corte de plano glClipPlane
        public static double a0 = 0.0;
        public static double b0 = 0.0;
        public static double c0 = 0.0;
        public static double d0 = 0.0;
        public static double a1 = 0.0;
        public static double b1 = 0.0;
        public static double c1 = 0.0;
        public static double d1 = 0.0;
        public static double a2 = 0.0;
        public static double b2 = 0.0;
        public static double c2 = 0.0;
        public static double d2 = 0.0;
        public static double a3 = 0.0;
        public static double b3 = 0.0;
        public static double c3 = 0.0;
        public static double d3 = 0.0;
        public static double a4 = 0.0;
        public static double b4 = 0.0;
        public static double c4 = 0.0;
        public static double d4 = 0.0;
        public static double a5 = 0.0;
        public static double b5 = 0.0;
        public static double c5 = 0.0;
        public static double d5 = 0.0;

        //Quantidade de planos a serem criados automaticamente
        public static double quantPlanes = 200;

        //Quantidade de esferas a serem criadas automaticament
        public static double quantSpheres = 200;

        public static double positionPlane = 0;
    }
}
