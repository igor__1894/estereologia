﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;


/* Classe utilizada para o tratamento e aplicação de funcionalidade em imagens:
   exemplo: imagem de projeção após os cortes*/


//http://stackoverflow.com/questions/20803615/wrong-arguments-in-method-houghcircles-from-emgu-cv
//http://www.codeproject.com/Tips/997492/Cell-Circle-counting-from-input-image-Using-Open-C

namespace Stereology.ImageTool
{
    class ImageUtils
    {

        public ImageUtils()
        {

        }

        public ImageUtils(Bitmap bitmap)
        {
            this.bitmap = bitmap;
        }

        public ImageUtils(Bitmap bitmap, String fileName)
        {
            this.bitmap = bitmap;
            this.fileName = fileName;
        }

        public void createInterfaceImageProjection()
        {
            formImageTools = new System.Windows.Forms.Form();
            formImageTools.SetBounds(200, 30, 600, 630);
            formImageTools.Text = "Imagem Estereologia";

            createPanelBitmap();
            createButtons();

            formImageTools.Show();
        }

        public void createButtons()
        {
            System.Windows.Forms.Button buttonCircles = new System.Windows.Forms.Button();
            buttonCircles.SetBounds(180, 530, 200, 30);
            buttonCircles.Text = "Detectar Circunferência";
            buttonCircles.Click += new EventHandler(this.onClickDetectCircle);
            formImageTools.Controls.Add(buttonCircles);
        }

        public void createPanelBitmap()
        {

            pictureBox = new System.Windows.Forms.PictureBox();
            pictureBox.SetBounds(40, 10, 500, 500);
            pictureBox.BackColor = Color.Gray;

            pictureBox.Image = bitmap;

            formImageTools.Controls.Add(pictureBox);
        }

        public void onClickDetectCircle(object sender, EventArgs e)
        {

            try
            {

                
                Image<Bgr, Byte> img = new Image<Bgr, byte>(bitmap);
                Image<Gray, Byte> graySoft = img.Convert<Gray, Byte>().PyrDown().PyrUp();
                Image<Gray, Byte> gray = graySoft.SmoothGaussian(3);

                gray = gray.AddWeighted(graySoft, 1.5, -0.5, 0);

                Image<Gray, Byte> bin = gray.ThresholdBinary(new Gray(70), new Gray(255));
                Gray cannyThreshold = new Gray(150);
                Gray cannyThresholdLinking = new Gray(85);
                Gray circleAccumulatorThreshold = new Gray(15);

                
                Image<Gray, Byte> cannyEdges = bin.Canny(cannyThreshold, cannyThresholdLinking);

                pictureBox.Image = cannyEdges.ToBitmap();
            
                CircleF[] circles = bin.HoughCircles(cannyThreshold, circleAccumulatorThreshold, 5.0, 
                    //Resolutioncenters of the circles 
                    1, //min distance 
                    15, //min radius 
                    45 //max radius 
                    )
                    [0];

                int count = 0;

                foreach (CircleF circle in circles)
                {
                    img.Draw(circle, new Bgr(Color.Yellow), 2);
                    count++;
                }

                pictureBox.Image = img.ToBitmap();
            
                Console.WriteLine("Entrou: " + count);

                //Image<Bgr, Byte> img = new Image<Bgr, byte>(this.fileName).Resize(500, 500, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR, true);

                //Image<Gray, Byte> gray = img.Convert<Gray, Byte>().PyrDown().PyrUp();

                /*Gray cannyThreshold = new Gray(200);
                Gray cannyThresholdLinking = new Gray(50);
                Gray circleAccumulatorThreshold = new Gray(50);

                gray.HoughCircles()
                CircleF[] circles = gray.HoughCircles(cannyThreshold, circleAccumulatorThreshold,
                    5, //Resolution of the accumulator used to detect centers of the circles
                    1, //min distance 
                    0, //min radius
                    0 //max radius
                )[0];*/


                //Console.WriteLine("Quantidade de circunferências: " + circles.LongLength);

            }
            catch(Exception exception)
            {
                System.Windows.Forms.Form formError = new System.Windows.Forms.Form();
                formError.Text = "Error EmguCV";
                formError.MaximizeBox = false;
                formError.SetBounds(400, 100, 500, 300);

                System.Windows.Forms.Label labelError = new System.Windows.Forms.Label();
                labelError.Text = "Erro ao abrir o arquivo solicitado: " + exception;
                labelError.SetBounds(10, 10, 500, 200);
                formError.Controls.Add(labelError);

                formError.ShowDialog();
            }
        }

        private Bitmap bitmap;
        private String fileName;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Form formImageTools;
    }
}
