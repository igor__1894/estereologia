﻿using Stereology.ComponentsGl;
using Stereology.MathAlgo;
using Stereology.PanelStyles;
using System;
using System.Collections.Generic;
using System.Text;
using Tao.OpenGl;

namespace Stereology
{
    public struct Position
    {
        public float x;
        public float y;
        public float z;

        public Position(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public class Stereology
    {
        Camera camara = new Camera();

        public static List<Spheres> listEsferas;
        public static List<Line> listGrid1;
        public static List<Line> listGrid2;
        public static List<Line> listGrid3;

        private Plane plane = new Plane();

        //Eixos orientados
        Line axisX = new Line(-100.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f);
        Line axisY = new Line(0.0f, -100.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f, 100.0f, 0.0f);
        Line axisZ = new Line(0.0f, 0.0f, -100.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f, 100.0f);

        //Porção de projeção do volume
        Line lineVol1 = new Line(-1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol2 = new Line(-1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol3 = new Line(-1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol4 = new Line(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol5 = new Line(-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol6 = new Line(1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol7 = new Line(1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol8 = new Line(-1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol9 = new Line(-1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol10 = new Line(1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol11 = new Line(1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 100.0f, 100.0f, 100.0f);
        Line lineVol12 = new Line(-1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 100.0f, 100.0f, 100.0f);


        public Stereology()
        {

        }

        public void CreateScene()
        {

            //Equação das 12 retas que compõem o cubo representante do volume total
            listLine.Add(new LineEquation(new PointLocation(-1.0f, -1.0f, 1.0f), new PointLocation(1.0f, -1.0f, 1.0f)));        //equação da reta (reta volume1)
            listLine.Add(new LineEquation(new PointLocation(-1.0f, -1.0f, -1.0f), new PointLocation(1.0f, -1.0f, -1.0f)));      //equação da reta (reta volume2)
            listLine.Add(new LineEquation(new PointLocation(-1.0f, 1.0f, 1.0f), new PointLocation(1.0f, 1.0f, 1.0f)));          //equação da reta (reta volume3)
            listLine.Add(new LineEquation(new PointLocation(-1.0f, 1.0f, -1.0f), new PointLocation(1.0f, 1.0f, -1.0f)));        //equação da reta (reta volume4)
            listLine.Add(new LineEquation(new PointLocation(-1.0f, -1.0f, 1.0f), new PointLocation(-1.0f, 1.0f, 1.0f)));        //equação da reta (reta volume5)
            listLine.Add(new LineEquation(new PointLocation(1.0f, -1.0f, 1.0f), new PointLocation(1.0f, 1.0f, 1.0f)));          //equação da reta (reta volume6)            
            listLine.Add(new LineEquation(new PointLocation(1.0f, -1.0f, -1.0f), new PointLocation(1.0f, 1.0f, -1.0f)));        //equação da reta (reta volume7)
            listLine.Add(new LineEquation(new PointLocation(-1.0f, -1.0f, -1.0f), new PointLocation(-1.0f, 1.0f, -1.0f)));      //equação da reta (reta volume8)
            listLine.Add(new LineEquation(new PointLocation(-1.0f, 1.0f, -1.0f), new PointLocation(-1.0f, 1.0f, 1.0f)));        //equação da reta (reta volume9)
            listLine.Add(new LineEquation(new PointLocation(1.0f, 1.0f, -1.0f), new PointLocation(1.0f, 1.0f, 1.0f)));          //equação da reta (reta volume10)            
            listLine.Add(new LineEquation(new PointLocation(1.0f, -1.0f, -1.0f), new PointLocation(1.0f, -1.0f, 1.0f)));        //equação da reta (reta volume11)
            listLine.Add(new LineEquation(new PointLocation(-1.0f, -1.0f, -1.0f), new PointLocation(-1.0f, -1.0f, 1.0f)));      //equação da reta (reva volume12)
            //...................................................................................................................................................

            plane.createPlanes();   //criação dos planos para corte das esferas

            //listEsferas.Add(new Esferas(0.0f, 0.0f, 0.0f, 0.5f));
            //listEsferas.Add(new Esferas(0.0f, 0.0f, 1.0f));

            axisX.createLine();
            axisY.createLine();
            axisZ.createLine();


            //criação das linhas da porção volumosa
            lineVol1.createLine();
            lineVol2.createLine();
            lineVol3.createLine();
            lineVol4.createLine();
            lineVol5.createLine();
            lineVol6.createLine();
            lineVol7.createLine();
            lineVol8.createLine();
            lineVol9.createLine();
            lineVol10.createLine();
            lineVol11.createLine();
            lineVol12.createLine();

            //............................................................................................................
            //configuração das linhas de grid no volume onde as esferas estão contidas
            //createGridLines();


            //............................................................................................................
            //criação aleatória das esferas no espaço 3D

            ListEsferas = new List<Spheres>();
            //listEsferas.Add(new Esferas(0.0f, 0.0f, 0.0f, 1.0f));
            //listEsferas.Add(new Esferas(0.0f, -0.5f, 0.0f, 0.1f));
            //listEsferas.Add(new Esferas(0.0f, 0.0f, 0.0f, 0.05f));

            //listEsferas.Add(new Esferas(0.0f, 0.0f, -0.5f, 0.2f));

            //Condição para a criação de esferas no espaço com diferentes diâmetros
            createSpheres();
            createGridLines();

            //Criar as esferas no espaço de acordo com as entradas do usuário
            foreach (var item in ListEsferas)
            {
                item.createSpheres();
            }
        }

        public void createSpheres()
        {

            Random random = new Random();
            bool flag = false;

            double radius;

            double aleatorioX;
            double aleatorioY;
            double aleatorioZ;

            double random1 = 0;
            double random2 = 0;

            double minRadius = 0;
            double maxRadius = 1;
            double inputRandom1Radius = 0;      //Variável pseudoaleatório para os raios das esferas
            double inputRandom2Radius = 0;      //Variável pseudoaleatória para os raios das esferas

            double minAleatorio = 0;            //definição do mínimo range 
            double maxAleatorio = 1;            //definição do máximo range 

            double minRangeSpace = -0.9;
            double maxRangeSpace = 0.9;


            //Condição onde a criação de esfera será de diferentes diâmetros
            if (MainForm.enableChangeDiameter == true)
            {
                int i = 0;

                while (i < Atributes.quantSpheres)
                {
                    if (i == 0)
                    {

                        //Geracao de numeros pseudoaleatorios para entrada no calculo aleatorio
                        inputRandom1Radius = random.NextDouble() * (maxRadius - minRadius) + minRadius;                      
                        inputRandom2Radius = random.NextDouble() * (maxRadius - minRadius) + minRadius;

                        //Criação de raios para as esferas de forma aleatório (inputRandom1, inputRandom2)
                        radius = ((Math.Sqrt(-2 * (Math.Log(inputRandom1Radius))) * Math.Cos(2 * Math.PI * inputRandom2Radius))-2)/100;

                        random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                               Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                        {
                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        }
                        aleatorioX = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                        //..........

                        random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                               Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                        {
                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        }
                        aleatorioY = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                        //..........

                        random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                               Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                        {
                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        }
                        aleatorioZ = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                        ListEsferas.Add(new Spheres((float)aleatorioX, (float)aleatorioY, (float)aleatorioZ, (float)Math.Abs(radius)));

                    }
                    else
                    {
                        do
                        {

                            inputRandom1Radius = random.NextDouble() * (maxRadius - minRadius) + minRadius;
                            inputRandom2Radius = random.NextDouble() * (maxRadius - minRadius) + minRadius;
                            radius = ((Math.Sqrt(-2 * (Math.Log(inputRandom1Radius))) * Math.Cos(2 * Math.PI * inputRandom2Radius))-2)/100;

                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);

                            while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                                Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                            {
                                random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                                random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            }
                            aleatorioX = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                            //..........

                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);

                            while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                               Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                            {
                                random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                                random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            }
                            aleatorioY = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                            //..........

                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);

                            while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                              Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                            {
                                random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                                random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            }
                            aleatorioZ = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                            flag = detectCollision(aleatorioX, aleatorioY, aleatorioZ, Math.Abs(radius));

                        } while (flag);

                        if (!flag)
                        {
                            ListEsferas.Add(new Spheres((float)aleatorioX, (float)aleatorioY, (float)aleatorioZ, (float)Math.Abs(radius)));

                        }
                    }
                    i++;
                }

            }
            else
            {                 
                //............................ESFERAS COM O MESMO DIÂMETRO...............................

                int i = 0;

                while (i < Atributes.quantSpheres)
                {
                    if (i == 0)
                    {

                        radius = inputRadius;

                        random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                               Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                        {
                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        }
                        aleatorioX = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                        //..........

                        random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                               Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                        {
                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        }
                        aleatorioY = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                        //..........

                        random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                               Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                        {
                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                        }
                        aleatorioZ = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                        ListEsferas.Add(new Spheres((float)aleatorioX, (float)aleatorioY, (float)aleatorioZ, (float)radius));
                    }
                    else
                    {
                        do
                        {
                            radius = inputRadius;

                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);

                            while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                                Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                            {
                                random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                                random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            }
                            aleatorioX = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                            //..........

                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);

                            while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                               Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                            {
                                random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                                random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            }
                            aleatorioY = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                            //..........

                            random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            random2 = minAleatorio + (random.NextDouble() * maxAleatorio);

                            while (Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) >= maxRangeSpace ||
                              Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2) <= minRangeSpace)
                            {
                                random1 = minAleatorio + (random.NextDouble() * maxAleatorio);
                                random2 = minAleatorio + (random.NextDouble() * maxAleatorio);
                            }
                            aleatorioZ = Math.Sqrt(-2 * (Math.Log(random1))) * Math.Cos(2 * Math.PI * random2);

                            flag = detectCollision(aleatorioX, aleatorioY, aleatorioZ, radius);

                        } while (flag);

                        if (!flag)
                        {
                            //Incrementa a lista com as novas esferas que serão inseridas na visualização via openGl
                            ListEsferas.Add(new Spheres((float)aleatorioX, (float)aleatorioY, (float)aleatorioZ, (float)radius));
                        
                        }
                    }
                    i++;
                }
            }
        }

        //Método aplicado para verificar se existe a colisção entre a nova esfera que será criada
        //com as esferas já existentes no sistema de visualização
        //na condição de colisão a esfera mais nova é descartada
        public bool detectCollision(double posX, double posY, double posZ, double radius)
        {
            double distance = 0;
            int j = 0;
            bool stop = false;

            for (int i = 0; i < ListEsferas.Count && !stop; i++)
            {
                j = i;
                while (j < ListEsferas.Count && !stop)
                {
                    distance = Math.Sqrt(Math.Pow((ListEsferas[j].getPositionX() - posX), 2) +
                                         Math.Pow((ListEsferas[j].getPositionY() - posY), 2) +
                                         Math.Pow((ListEsferas[j].getPositionZ() - posZ), 2));

                    if (distance < Math.Abs(ListEsferas[j].getRadius() + radius))
                    {
                        stop = true;
                    }
                    j++;
                }
            }

            if (stop == true)
            {
                return true;

            }
            else
            {
                return false;
            }
        }

        public Camera Camara
        {
            get { return camara; }
        }

        internal List<Spheres> ListEsferas
        {
            get
            {
                return listEsferas;
            }

            set
            {
                listEsferas = value;
            }
        }

        public void drawSceneStereology()
        {
            switch (MainForm.optionView)
            {

                case 0:

                    cropPlane();
                    applyOrthoAxis();

                    if (MainForm.enableVolume)
                    {
                        applyLines();
                    }

                    if (MainForm.enableGrid == true)
                    {
                        applyGrid();
                    }

                    foreach (var item in ListEsferas)
                    {
                        item.paintSpheres();
                    }

                    break;

                case 1:

                    cropPlane();

                    if (MainForm.enableVolume)
                    {
                        applyLines();
                    }

                    if (MainForm.enableGrid == true)
                    {
                        applyGrid();
                    }

                    foreach (var item in ListEsferas)
                    {
                        item.paintSpheres();
                    }

                    break;

                case 2:

                    Gl.glPushMatrix();
                    cropPlane();
                    plane.drawPlane();
                    Gl.glPopMatrix();

                    if (!IntersectSolid.enableOnlySpheres)
                    {
                        foreach (var item in ListEsferas)
                        {
                            item.paintSpheres();
                        }
                    } else
                    {
                        try
                        {
                            List<Spheres> listEsferas = new List<Spheres>();
                            //Teste para esferas com intersecção.
                            listEsferas = IntersectSolid.listPlaneSpheres[PanelPlanes.indexPlanIntersect].getSpheres();

                            for (int i = 0; i < listEsferas.Count; i++)
                            {
                                listEsferas[i].createSpheres();
                                listEsferas[i].paintSpheres();
                            }

                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine("Não existem planos criados: " + exception.ToString());
                        }
                    }

                    if (MainForm.enableVolume)
                    {
                        applyLines();
                    }

                    if (MainForm.enableGrid == true)
                    {
                        applyGrid();
                    }

                    break;
                }
            }

            /*if (MainForm.optionView == 3)
            {
                Plane plane = new Plane();
                plane.createPlanes();

                Cube cube = new Cube(0, 0, 0, 2);
                cube.create();          //criação de um cubo volumétrico
                cube.paint();

                cropPlane();

                if (MainForm.enableVolume)
                {
                    lineVol1.drawLine();
                    lineVol2.drawLine();
                    lineVol3.drawLine();
                    lineVol4.drawLine();
                    lineVol5.drawLine();
                    lineVol6.drawLine();
                    lineVol7.drawLine();
                    lineVol8.drawLine();
                    lineVol9.drawLine();
                    lineVol10.drawLine();
                    lineVol11.drawLine();
                    lineVol12.drawLine();
                }

                foreach (var item in ListEsferas)
                {
                    item.paintSpheres();
                }

                float counter = -1.0f;


                while (counter < 1.0f)
                {
                    plane.drawFreePlane(counter, 1.0f, -1.0f, counter, 1.0f, 1.0f, counter, -1.0f, 1.0f, counter, -1.0f, -1.0f);

                    counter = counter + 0.001f;
                }
            }
        }*/

        public void cropPlane()
        {

            if (MainForm.optionView == 0 || MainForm.optionView == 1 || MainForm.optionView == 2 || MainForm.optionView == 3)
            {
                if (MainForm.optionView == 0)
                {
                    Atributes.a0 = 0.0;
                    Atributes.b1 = 0.0;
                    Atributes.c2 = 0.0;
                    Atributes.a3 = 0.0;
                    Atributes.b4 = 0.0;
                    Atributes.c5 = 0.0;

                    Atributes.d0 = 100.0;
                    Atributes.d1 = 0.0;
                    Atributes.d2 = 0.0;
                    Atributes.d3 = 0.0;
                    Atributes.d4 = 0.0;
                    Atributes.d5 = 0.0;

                    equation1[0] = Atributes.a0;
                    equation2[1] = Atributes.b1;
                    equation3[2] = Atributes.c2;
                    equation4[0] = Atributes.a3;
                    equation5[1] = Atributes.b4;
                    equation6[2] = Atributes.c5;

                    equation1[3] = Atributes.d0;
                    equation2[3] = Atributes.d1;
                    equation3[3] = Atributes.d2;
                    equation4[3] = Atributes.d3;
                    equation5[3] = Atributes.d4;
                    equation6[3] = Atributes.d4;

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE0, equation1);
                    Gl.glEnable(Gl.GL_CLIP_PLANE0);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE1, equation2);
                    Gl.glEnable(Gl.GL_CLIP_PLANE1);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE2, equation3);
                    Gl.glEnable(Gl.GL_CLIP_PLANE2);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE3, equation4);
                    Gl.glEnable(Gl.GL_CLIP_PLANE3);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE4, equation5);
                    Gl.glEnable(Gl.GL_CLIP_PLANE4);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE5, equation6);
                    Gl.glEnable(Gl.GL_CLIP_PLANE5);

                }

                if (MainForm.optionView == 1)
                {
                    Atributes.a0 = 0.0f;
                    Atributes.b0 = 0.0f;
                    Atributes.c0 = 0.0f;
                    Atributes.d0 = 100000.0f;
                    Atributes.d1 = 100000.0f;
                    Atributes.d2 = 100000.0f;
                    Atributes.d3 = 100000.0f;
                    Atributes.d4 = 100000.0f;
                    Atributes.d5 = 100000.0f;

                    equation1[0] = Atributes.a0;
                    equation1[1] = Atributes.b0;
                    equation1[2] = Atributes.c0;
                    equation1[3] = Atributes.d0;


                    Gl.glClipPlane(Gl.GL_CLIP_PLANE0, equation1);
                    Gl.glEnable(Gl.GL_CLIP_PLANE0);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE1, equation2);
                    Gl.glEnable(Gl.GL_CLIP_PLANE1);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE2, equation3);
                    Gl.glEnable(Gl.GL_CLIP_PLANE2);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE3, equation4);
                    Gl.glEnable(Gl.GL_CLIP_PLANE3);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE4, equation5);
                    Gl.glEnable(Gl.GL_CLIP_PLANE4);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE5, equation6);
                    Gl.glEnable(Gl.GL_CLIP_PLANE5);
                }

                if (MainForm.optionView == 2)
                {
                    Atributes.a0 = 1.0;
                    Atributes.b1 = 1.0;
                    Atributes.c2 = 1.0;
                    Atributes.a3 = -1.0;
                    Atributes.b4 = -1.0;
                    Atributes.c5 = -1.0;

                    Atributes.d0 = 1.0001;
                    Atributes.d1 = 1.0001;
                    Atributes.d2 = 1.0001;
                    Atributes.d3 = 1.0001;
                    Atributes.d4 = 1.0001;
                    Atributes.d5 = 1.0001;

                    equation1[0] = Atributes.a0;
                    equation2[1] = Atributes.b1;
                    equation3[2] = Atributes.c2;
                    equation4[0] = Atributes.a3;
                    equation5[1] = Atributes.b4;
                    equation6[2] = Atributes.c5;

                    equation1[3] = Atributes.d0;
                    equation2[3] = Atributes.d1;
                    equation3[3] = Atributes.d2;
                    equation4[3] = Atributes.d3;
                    equation5[3] = Atributes.d4;
                    equation6[3] = Atributes.d4;

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE0, equation1);
                    Gl.glEnable(Gl.GL_CLIP_PLANE0);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE1, equation2);
                    Gl.glEnable(Gl.GL_CLIP_PLANE1);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE2, equation3);
                    Gl.glEnable(Gl.GL_CLIP_PLANE2);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE3, equation4);
                    Gl.glEnable(Gl.GL_CLIP_PLANE3);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE4, equation5);
                    Gl.glEnable(Gl.GL_CLIP_PLANE4);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE5, equation6);
                    Gl.glEnable(Gl.GL_CLIP_PLANE5);

                }

                if (MainForm.optionView == 3)
                {
                    Atributes.a0 = 1.0f;
                    Atributes.b0 = 0.0f;
                    Atributes.c0 = 0.0f;
                    Atributes.d0 = (float)(Atributes.positionPlane / 100);

                    equation1[0] = Atributes.a0;
                    equation1[1] = Atributes.b0;
                    equation1[2] = Atributes.c0;
                    equation1[3] = Atributes.d0;

                    Atributes.d1 = 100000;
                    Atributes.d2 = 100000;
                    Atributes.d3 = 100000;
                    Atributes.d4 = 100000;
                    Atributes.d5 = 100000;

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE0, equation1);
                    Gl.glEnable(Gl.GL_CLIP_PLANE0);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE1, equation2);
                    Gl.glEnable(Gl.GL_CLIP_PLANE1);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE2, equation3);
                    Gl.glEnable(Gl.GL_CLIP_PLANE2);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE3, equation4);
                    Gl.glEnable(Gl.GL_CLIP_PLANE3);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE4, equation5);
                    Gl.glEnable(Gl.GL_CLIP_PLANE4);

                    Gl.glClipPlane(Gl.GL_CLIP_PLANE5, equation6);
                    Gl.glEnable(Gl.GL_CLIP_PLANE5);
                }
            }
        }

        public void createGridLines()
        {
            listGrid1 = new List<Line>();
            listGrid2 = new List<Line>();
            listGrid3 = new List<Line>();

            float step1 = -1.2f;
            float step2 = -1.2f;

            for (int i = 1; i < 12; i++)
            {
                step1 = step1 + 0.2f;

                for (int j = 0; j < 11; j++)
                {
                    step2 = step2 + 0.2f;
                    listGrid1.Add(new Line(step1, step2, 1.0f, step1, step2, -1.0f, 100.0f, 100.0f, 100.0f));
                }

                step2 = -1.2f;
            }

            foreach (Line line in listGrid1)
            {
                line.createLine();
            }

            step1 = -1.2f;
            step2 = -1.2f;

            for (int i = 1; i < 12; i++)
            {
                step1 = step1 + 0.2f;

                for (int j = 0; j < 11; j++)
                {
                    step2 = step2 + 0.2f;

                    listGrid2.Add(new Line(step1, -1.0f, step2, step1, 1.0f, step2, 100.0f, 100.0f, 100.0f));
                }
                step2 = -1.2f;
            }

            foreach (Line line in listGrid2)
            {
                line.createLine();
            }

            step1 = -1.2f;
            step2 = -1.2f;

            for (int i = 1; i < 12; i++)
            {
                step1 = step1 + 0.2f;

                for (int j = 0; j < 11; j++)
                {
                    step2 = step2 + 0.2f;

                    listGrid3.Add(new Line(-1.0f, step1, step2, 1.0f, step1, step2, 100.0f, 100.0f, 100.0f));
                }
                step2 = -1.2f;
            }

            foreach (Line line in listGrid3)
            {
                line.createLine();
            }

        }

        //Métodos aplicados para entradas nos tipos de visualização
        
        //Método para renderizar os eixos ortogonais
        public void applyOrthoAxis()
        {
            axisX.drawLine();
            axisY.drawLine();
            axisZ.drawLine();
        }

        //Método para renderizar as linhas de volume unitário
        public void applyLines()
        {
            lineVol1.drawLine();
            lineVol2.drawLine();
            lineVol3.drawLine();
            lineVol4.drawLine();
            lineVol5.drawLine();
            lineVol6.drawLine();
            lineVol7.drawLine();
            lineVol8.drawLine();
            lineVol9.drawLine();
            lineVol10.drawLine();
            lineVol11.drawLine();
            lineVol12.drawLine();
        }

        public void applyGrid()
        {
            foreach (Line line in listGrid1)
            {
                line.drawLine();
            }

            foreach (Line line in listGrid2)
            {
                line.drawLine();
            }

            foreach (Line line in listGrid3)
            {
                line.drawLine();
            }
        }
        //...............Atributos da classe Stereology..................

        private double inputRadius = 0.05;                                              //Definição manual do raio da esfera com o mesmo diâmetro

        public static List<LineEquation> listLine = new List<LineEquation>();           //lista de equações da reta para verificar a intersecção da reta com o plano

        //Equaçãos para corte de visualização no sistema openGl (Aplicação Crop)
        public static double[] equation1 = { Atributes.a0, Atributes.b0, Atributes.c0, Atributes.d0 };
        public static double[] equation2 = { Atributes.a1, Atributes.b1, Atributes.c1, Atributes.d1 };
        public static double[] equation3 = { Atributes.a2, Atributes.b2, Atributes.c2, Atributes.d2 };
        public static double[] equation4 = { Atributes.a3, Atributes.b3, Atributes.c3, Atributes.d3 };
        public static double[] equation5 = { Atributes.a4, Atributes.b4, Atributes.c4, Atributes.d4 };
        public static double[] equation6 = { Atributes.a5, Atributes.b5, Atributes.c5, Atributes.d5 };
    }
}
