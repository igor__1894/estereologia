﻿using System;
using System.Collections.Generic;
using System.Text;
using ShadowEngine.OpenGL;
using Tao.OpenGl;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Estereology.MathAlgo;
using Estereology.PanelStyles;

namespace Estereology
{
    class Plane
    {

        int list;

        public Plane()
        {

        }

        // Método para criar os planos
        public void createPlanes() {

            Glu.GLUquadric quadratic = Glu.gluNewQuadric();
            Glu.gluQuadricNormals(quadratic, Glu.GLU_FLAT);
            list = Gl.glGenLists(1);
            Gl.glNewList(list, Gl.GL_COMPILE);
            Gl.glPushMatrix();

            Gl.glPopMatrix();
            Gl.glEnd();
            Gl.glEndList();
        }

        public void drawPlane()
        {
            Gl.glPushMatrix();

            Gl.glBegin(Gl.GL_QUADS);

            if (MenuPanel.listIntersect != null)
            {
                float red = (float) MenuPanel.scrollColorPlaneR.Value/255;
                float green = (float) MenuPanel.scrollColorPlaneG.Value/255;
                float blue = (float) MenuPanel.scrollColorPlaneB.Value/255;

                Gl.glColor3f( red, green, blue);
    
                for(int i = 0; i < MenuPanel.listIntersect.Count; i++)
                {
                    float x = (float)MenuPanel.listIntersect[i].getX();
                    float y = (float)MenuPanel.listIntersect[i].getY();
                    float z = (float)MenuPanel.listIntersect[i].getZ();

                    Gl.glVertex3f(x,y,z);

                }

            }

            Gl.glCallList(list);
            Gl.glPopMatrix();
        }

        public void cropPlane()
        {
                //double[] equation1 = { 1.0, 0.0, 0.0, 1.001 };
                Gl.glClipPlane(Gl.GL_CLIP_PLANE0, equation1);
                Gl.glEnable(Gl.GL_CLIP_PLANE0);

                //double[] equation2 = { 0.0, 1.0, 0.0, 1.001 };
                Gl.glClipPlane(Gl.GL_CLIP_PLANE1, equation2);
                Gl.glEnable(Gl.GL_CLIP_PLANE1);

                //double[] equation3 = { 0.0, 0.0, 1.0, 1.001 };
                Gl.glClipPlane(Gl.GL_CLIP_PLANE2, equation3);
                Gl.glEnable(Gl.GL_CLIP_PLANE2);

                //double[] equation4 = { -1.0, 0.0, 0.0, 1.001 };
                Gl.glClipPlane(Gl.GL_CLIP_PLANE3, equation4);
                Gl.glEnable(Gl.GL_CLIP_PLANE3);

                //double[] equation5 = { 0.0, -1.0, 0.0, 1.001 };
                Gl.glClipPlane(Gl.GL_CLIP_PLANE4, equation5);
                Gl.glEnable(Gl.GL_CLIP_PLANE4);

                double[] equation6 = { 0.0, 0.0, -1.0, 1.01 };
                Gl.glClipPlane(Gl.GL_CLIP_PLANE5, equation6);
                Gl.glEnable(Gl.GL_CLIP_PLANE5);
           
        }

        public static double[] equation1 = { 1.0, 0.0, 0.0, 1.001 };
        public static double[] equation2 = { 0.0, 1.0, 0.0, 1.001 };
        public static double[] equation3 = { 0.0, 0.0, 1.0, 1.001 };
        public static double[] equation4 = { -1.0, 0.0, 0.0, 1.001 };
        public static double[] equation5 = { 0.0, -1.0, 0.0, 1.001 };
        public static double[] equation6 = { 0.0, 0.0, -1.0, 1.0-1 };
    }
}
