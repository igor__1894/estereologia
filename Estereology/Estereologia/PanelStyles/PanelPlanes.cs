﻿using Stereology.ComponentsGl;
using Stereology.MathAlgo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * Classe responsável pelo gerenciamento da informações referente
 * a projeção dos planos no espaço tridimensional com a aplicação 
 * dos cortes nas esferas.
     */

namespace Stereology.PanelStyles
{
    public class PanelPlanes
    {
        public PanelPlanes()
        {

        }

        public PanelPlanes(PlaneUtils planeUtils)
        {
            this.planeUtils = planeUtils;
        }

        public PanelPlanes(int positionX, int positionY, int sizeX, int sizeY, bool isVisible)
        {
            this.positionX = positionX;
            this.positionY = positionY;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            this.isVisible = isVisible;

        }

        public void createPanelPlane(System.Windows.Forms.Panel panelInfo)
        {
            panelPlane = new System.Windows.Forms.Panel();
            panelPlane.SetBounds(this.positionX, this.positionY, this.sizeX, this.sizeY);
            panelPlane.BackColor = System.Drawing.Color.FromArgb(200, 200, 200);

            if (this.isVisible)
            {
                panelPlane.Visible = true;
            }else
            {
                panelPlane.Visible = false;
            }

            createLabels();
            createListPlanes();
            createButtons();

            panelInfo.Controls.Add(panelPlane);
        }

        public void setVisible(bool visible)
        {
            panelPlane.Visible = visible;
        }

        public void createLabels()
        {
            System.Windows.Forms.Label labelTitlePlane = new System.Windows.Forms.Label();
            labelTitlePlane.Text = "Planes Management";
            labelTitlePlane.Font = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
            labelTitlePlane.ForeColor = System.Drawing.Color.Black;
            labelTitlePlane.SetBounds(65, 10, 300, 20);
            panelPlane.Controls.Add(labelTitlePlane);
        }

        public void createButtons()
        {
            checkBoxIntersec = new System.Windows.Forms.CheckBox();
            checkBoxIntersec.Text = "Intersection(spherical model)";
            checkBoxIntersec.SetBounds(10, 370, 200, 20);
            checkBoxIntersec.Click += new System.EventHandler(this.onClickIntersect);
            panelPlane.Controls.Add(checkBoxIntersec);

        }

        public void onClickIntersect(object sender, EventArgs e)
        {
            if (checkBoxIntersec.Checked)
            {
                IntersectSolid.enableOnlySpheres = true;
            }
            else
            {
                IntersectSolid.enableOnlySpheres = false;
            }
        }

        public void addElementsInListView(PlaneUtils planeUtils)
        {
            try
            {
                if (planeUtils.getListPlaneEquation().Count != 0)
                {
                    listViewPlanes.Items.Clear();

                    listItem = new List<ListViewItem>();

                    double[] coeficienteX = new double[100000];
                    double[] coeficienteY = new double[100000];
                    double[] coeficienteZ = new double[100000];
                    double[] independente = new double[100000];
                    int[] index = new int[100000];


                    for (int i = 0; i < planeUtils.getListPlaneEquation().Count; i++)
                    {
                        listItem.Add(new ListViewItem());
                    }

                    for (int i = 0; i < planeUtils.getListPlaneEquation().Count; i++)
                    {
                        coeficienteX[i] = planeUtils.getListPlaneEquation()[i].getCoeficienteX();
                        coeficienteY[i] = planeUtils.getListPlaneEquation()[i].getCoeficienteY();
                        coeficienteZ[i] = planeUtils.getListPlaneEquation()[i].getCoeficienteZ();
                        independente[i] = planeUtils.getListPlaneEquation()[i].getTermoIndenpendente();
                        index[i] = planeUtils.getListPlaneEquation()[i].getIndexOptionPlane();
                    }

                    for (int i = 0; i < planeUtils.getListPlaneEquation().Count; i++)
                    {
                        //String[] planesArray = new String[6];
                        planesArray = new String[6];
                        planesArray[0] = "Plano: " + (i + 1).ToString();
                        planesArray[1] = coeficienteX[i].ToString();
                        planesArray[2] = coeficienteY[i].ToString();
                        planesArray[3] = coeficienteZ[i].ToString();
                        planesArray[4] = independente[i].ToString();
                        planesArray[5] = index[i].ToString();

                        listItem[i] = new ListViewItem(planesArray);
                        listViewPlanes.Items.Add(listItem[i]);
                    }
                }
                else
                {
                    Console.WriteLine("Não existem objetos");
                }

            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception error: " + exception.ToString());
            }
        }

        public void createListPlanes()
        {
            listViewPlanes = new System.Windows.Forms.ListView();
            listViewPlanes.SetBounds(10, 40, 405, 320);

            listViewPlanes.View = System.Windows.Forms.View.Details;
            listViewPlanes.LabelEdit = false;
            listViewPlanes.AllowColumnReorder = false;
            listViewPlanes.CheckBoxes = false;
            listViewPlanes.FullRowSelect = true;
            listViewPlanes.GridLines = true;

            listViewPlanes.Columns.Add("Plano", 82, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("ax", 80, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("by", 80, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("cz", 80, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("d", 80, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("Index", 80, HorizontalAlignment.Left);

            listViewPlanes.SelectedIndexChanged += new System.EventHandler(this.onSelectedRowPlaneView);

            panelPlane.Controls.Add(listViewPlanes);

        }

        public void onSelectedRowPlaneView(object sender, EventArgs e)
        {

            String[] separator = { "{", "}", " ", "ListViewSubItem", ":" };
            String[] valuesPlan;
            String expression;

            ListView.SelectedListViewItemCollection collection = this.listViewPlanes.SelectedItems;

            foreach (ListViewItem item in collection)
            {
                expression = item.SubItems[0].ToString() + item.SubItems[1].ToString() + item.SubItems[2].ToString() + item.SubItems[3].ToString() + item.SubItems[4] + item.SubItems[5];
                   
                valuesPlan = expression.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                indexPlanIntersect = (Int32.Parse(valuesPlan[1])) - 1 ;

                PlanEquation.coeficienteX = Double.Parse(valuesPlan[2]);
                PlanEquation.coeficienteY = Double.Parse(valuesPlan[3]);
                PlanEquation.coeficienteZ = Double.Parse(valuesPlan[4]);
                PlanEquation.termoIndenpendente = Double.Parse(valuesPlan[5]);
                PlanEquation.optionPlaneStatic = Int32.Parse(valuesPlan[6]);

                calculateIntersectionsPlane();
            }
        }
   
        public void calculateIntersectionsPlane()
        {

            MenuPanel.listIntersect = new List<PointLocation>();

            foreach (LineEquation line in Stereology.listLine)
            {
                double coordPointX = line.getPointA().getX();
                double coordPointY = line.getPointA().getY();
                double coordPointZ = line.getPointA().getZ();

                double coordVecX = line.getVector().getX();
                double coordVecY = line.getVector().getY();
                double coordVecZ = line.getVector().getZ();

                double numerador = -1 * ((PlanEquation.coeficienteX * line.getPointA().getX()) +
                                         (PlanEquation.coeficienteY * line.getPointA().getY()) +
                                         (PlanEquation.coeficienteZ * line.getPointA().getZ()) +
                                         (PlanEquation.termoIndenpendente));

                double denominador = (PlanEquation.coeficienteX * line.getVector().getX()) +
                                     (PlanEquation.coeficienteY * line.getVector().getY()) +
                                     (PlanEquation.coeficienteZ * line.getVector().getZ());

                if (denominador != 0)
                {
                    double parametro = numerador / denominador;

                    double x = line.getPointA().getX() + (parametro * line.getVector().getX());
                    double y = line.getPointA().getY() + (parametro * line.getVector().getY());
                    double z = line.getPointA().getZ() + (parametro * line.getVector().getZ());

                    MenuPanel.listIntersect.Add(new PointLocation(x, y, z));
                }
            }
        }


        public String[] planesArray;
        public List<ListViewItem> listItem;

        private System.Windows.Forms.ListView listViewPlanes;
        private System.Windows.Forms.CheckBox checkBoxIntersec;
        private PlaneUtils planeUtils;

        private int positionX;
        private int positionY;
        private int sizeX;
        private int sizeY;
        private bool isVisible;

        private System.Windows.Forms.Panel panelPlane;

        public static List<PointLocation> listIntersect;
        public static int indexPlanIntersect;
    }
}
