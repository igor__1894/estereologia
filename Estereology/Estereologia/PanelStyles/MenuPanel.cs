﻿using Stereology.ComponentsGl;
using Stereology.GraphicsPlot;
using Stereology.MathAlgo;
using Stereology.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Stereology.PanelStyles
{

    public class MenuPanel
    {
        //public delegate void InvokeDelegate();
        //public delegate void InvokeDelegate(ListView listViewCircleArea);

        public MenuPanel()
        {

        }

        public MenuPanel(System.Windows.Forms.Menu menu, System.Windows.Forms.MainMenu mainMenu)
        {

            this.menu = menu;
            this.mainMenu = mainMenu;

            staticListPlaneEquation = new List<PlanEquation>();

            createMenuAdds();
        }

        //Método para criar as componentes do menu e configuração
        //..........................................................................................................................
        public void createMenuAdds()
        {
            //....................................MENU PARA AS ESFERAS..................................................

            System.Windows.Forms.MenuItem menuSpheres = new System.Windows.Forms.MenuItem("&Spheres");

            System.Windows.Forms.MenuItem menuRadius = new System.Windows.Forms.MenuItem("&Radius");
            menuRadius.Click += new System.EventHandler(this.onClickMenuRadius);
            menuSpheres.MenuItems.Add(menuRadius);

            System.Windows.Forms.MenuItem menuInfoSpheres = new System.Windows.Forms.MenuItem("Information");
            menuInfoSpheres.Click += new System.EventHandler(this.onClickMenuInfoSpheres);
            menuSpheres.MenuItems.Add(menuInfoSpheres);

            mainMenu.MenuItems.Add(menuSpheres);

            //....................................MENU PARA OS PLANOS..................................................

            System.Windows.Forms.MenuItem menuPlane = new System.Windows.Forms.MenuItem("&Planes");

            System.Windows.Forms.MenuItem criarPlano = new System.Windows.Forms.MenuItem("&Create planes");
            criarPlano.Click += new System.EventHandler(this.onClickMenuCreatePlane);
            menuPlane.MenuItems.Add(criarPlano);

            System.Windows.Forms.MenuItem showCircleArea = new System.Windows.Forms.MenuItem("&Secction Area");
            showCircleArea.Click += new System.EventHandler(this.onClickCircleArea);
            menuPlane.MenuItems.Add(showCircleArea);

            mainMenu.MenuItems.Add(menuPlane);

            //....................................MENU PARA OS HISTOGRAMAS..................................................

            System.Windows.Forms.MenuItem menuHistogram = new System.Windows.Forms.MenuItem("&Histogram");

            System.Windows.Forms.MenuItem showHistArea = new System.Windows.Forms.MenuItem("&Histogram Area");
            showHistArea.Click += new System.EventHandler(this.onClickHistArea);
            menuHistogram.MenuItems.Add(showHistArea);

            System.Windows.Forms.MenuItem showHistRadius = new System.Windows.Forms.MenuItem("&Histogram Radius");
            showHistRadius.Click += new System.EventHandler(this.onClickHistDiameter);
            menuHistogram.MenuItems.Add(showHistRadius);

            System.Windows.Forms.MenuItem histSaltRadio = new System.Windows.Forms.MenuItem("&Saltykov Raio");
            histSaltRadio.Click += new System.EventHandler(this.onClickHistSaltyRadio);
            menuHistogram.MenuItems.Add(histSaltRadio);

            System.Windows.Forms.MenuItem histSaltArea = new System.Windows.Forms.MenuItem("&Saltykov Area");
            histSaltArea.Click += new System.EventHandler(this.onClickHistSaltyArea);
            menuHistogram.MenuItems.Add(histSaltArea);

            mainMenu.MenuItems.Add(menuHistogram);

        }

        public void createLabelHistogramArea(System.Windows.Forms.Form formHistArea, ArrayList listAreaCircle, int quantInterval, List<Interval> listIntervalCircle)
        {
            double maximumArea = (double)listAreaCircle[listAreaCircle.Count - 1];
            double minimumArea = (double)listAreaCircle[0];

            System.Windows.Forms.Label labelTitleHist = new System.Windows.Forms.Label();
            labelTitleHist.SetBounds(420, 10, 200, 20);
            labelTitleHist.Text = "Information";
            labelTitleHist.Font = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
            formHistArea.Controls.Add(labelTitleHist);

            System.Windows.Forms.Label labelQuantArea = new System.Windows.Forms.Label();
            labelQuantArea.SetBounds(420, 40, 300, 20);
            labelQuantArea.Text = "Number of circles: " + IntersectSolid.listCircle.Count;
            labelQuantArea.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            formHistArea.Controls.Add(labelQuantArea);

            System.Windows.Forms.Label labelMinArea = new System.Windows.Forms.Label();
            labelMinArea.SetBounds(420, 60, 400, 20);
            labelMinArea.Text = "Smallest area: " + minimumArea;
            labelMinArea.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            formHistArea.Controls.Add(labelMinArea);

            System.Windows.Forms.Label labelMaxArea = new System.Windows.Forms.Label();
            labelMaxArea.SetBounds(420, 80, 300, 20);
            labelMaxArea.Text = "Largest area: " + maximumArea;
            labelMaxArea.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            formHistArea.Controls.Add(labelMaxArea);

            listViewIntervalFrequence = new System.Windows.Forms.ListView();
            listViewIntervalFrequence.SetBounds(420, 130, 350, 280);

            listViewIntervalFrequence.View = System.Windows.Forms.View.Details;
            listViewIntervalFrequence.LabelEdit = false;
            listViewIntervalFrequence.AllowColumnReorder = false;
            listViewIntervalFrequence.CheckBoxes = false;
            listViewIntervalFrequence.FullRowSelect = true;
            listViewIntervalFrequence.GridLines = true;

            listViewIntervalFrequence.Columns.Add("Interval", 150, HorizontalAlignment.Left);
            listViewIntervalFrequence.Columns.Add("Average", 150, HorizontalAlignment.Left);
            listViewIntervalFrequence.Columns.Add("Frequence", 150, HorizontalAlignment.Left);

            if (IntersectSolid.listCircle.Count > 0)
            {
                List<ListViewItem> listItem = new List<ListViewItem>();

                String[] interval = new String[2000000];
                double[] media = new double[2000000];
                int[] frequencia = new int[2000000];

                for (int i = 0; i < quantInterval; i++)
                {
                    listItem.Add(new ListViewItem());
                }


                for (int i = 0; i < quantInterval; i++)
                {
                    interval[i] = "(" + listIntervalCircle[i].getIntervalMax() + " - " + listIntervalCircle[i].getIntervalMin() + ")";
                    media[i] = (listIntervalCircle[i].getIntervalMax() + listIntervalCircle[i].getIntervalMin()) / 2;
                    frequencia[i] = listIntervalCircle[i].getFrequence();
                }

                int soma = 0;

                for (int i = 0; i < quantInterval; i++)
                {
                    String[] circleArray = new String[3];
                    circleArray[0] = interval[i].ToString();
                    circleArray[1] = media[i].ToString();
                    circleArray[2] = frequencia[i].ToString();

                    soma = soma + frequencia[i];

                    listItem[i] = new ListViewItem(circleArray);
                    listViewIntervalFrequence.Items.Add(listItem[i]);
                }

            }

            System.Windows.Forms.Button buttonExport = new System.Windows.Forms.Button();
            buttonExport.SetBounds(10, 420, 100, 30);
            buttonExport.Text = "Export Worksheet";
            buttonExport.Click += new System.EventHandler(this.exportListIntervalFrequence);
            formHistArea.Controls.Add(buttonExport);

            formHistArea.Controls.Add(listViewIntervalFrequence);

        }

        public void createLabelHistogramRadius(System.Windows.Forms.Form formHistRadius, ArrayList listRadiusCircle, int quantInterval, List<Interval> listIntervalCircle)
        {
            double maximumRadius = (double)listRadiusCircle[listRadiusCircle.Count - 1];
            double minimumRadius = (double)listRadiusCircle[0];

            System.Windows.Forms.Label labelTitleHist = new System.Windows.Forms.Label();
            labelTitleHist.SetBounds(420, 10, 200, 20);
            labelTitleHist.Text = "Information";
            labelTitleHist.Font = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
            formHistRadius.Controls.Add(labelTitleHist);

            System.Windows.Forms.Label labelQuantCircle = new System.Windows.Forms.Label();
            labelQuantCircle.SetBounds(420, 40, 300, 20);
            labelQuantCircle.Text = "Number of circles: " + IntersectSolid.listCircle.Count;
            labelQuantCircle.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            formHistRadius.Controls.Add(labelQuantCircle);

            System.Windows.Forms.Label labelMinRaio = new System.Windows.Forms.Label();
            labelMinRaio.SetBounds(420, 60, 400, 20);
            labelMinRaio.Text = "Smallest area: " + minimumRadius;
            labelMinRaio.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            formHistRadius.Controls.Add(labelMinRaio);

            System.Windows.Forms.Label labelMaxRaio = new System.Windows.Forms.Label();
            labelMaxRaio.SetBounds(420, 80, 300, 20);
            labelMaxRaio.Text = "Largest area: " + maximumRadius;
            labelMaxRaio.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            formHistRadius.Controls.Add(labelMaxRaio);

            listViewIntervalFrequence = new System.Windows.Forms.ListView();
            listViewIntervalFrequence.SetBounds(420, 130, 350, 280);

            listViewIntervalFrequence.View = System.Windows.Forms.View.Details;
            listViewIntervalFrequence.LabelEdit = false;
            listViewIntervalFrequence.AllowColumnReorder = false;
            listViewIntervalFrequence.CheckBoxes = false;
            listViewIntervalFrequence.FullRowSelect = true;
            listViewIntervalFrequence.GridLines = true;

            listViewIntervalFrequence.Columns.Add("Intervalo", 150, HorizontalAlignment.Left);
            listViewIntervalFrequence.Columns.Add("Média", 150, HorizontalAlignment.Left);
            listViewIntervalFrequence.Columns.Add("Frequência", 150, HorizontalAlignment.Left);

            if (IntersectSolid.listCircle.Count > 0)
            {
                List<ListViewItem> listItem = new List<ListViewItem>();

                String[] interval = new String[2000000];
                double[] media = new double[2000000];
                int[] frequencia = new int[2000000];

                for (int i = 0; i < quantInterval; i++)
                {
                    listItem.Add(new ListViewItem());
                }


                for (int i = 0; i < quantInterval; i++)
                {
                    interval[i] = "(" + listIntervalCircle[i].getIntervalMax() + " - " + listIntervalCircle[i].getIntervalMin() + ")";
                    media[i] = (listIntervalCircle[i].getIntervalMax() + listIntervalCircle[i].getIntervalMin()) / 2;
                    frequencia[i] = listIntervalCircle[i].getFrequence();
                }

                int soma = 0;

                for (int i = 0; i < quantInterval; i++)
                {
                    String[] circleArray = new String[3];
                    circleArray[0] = interval[i].ToString();
                    circleArray[1] = media[i].ToString();
                    circleArray[2] = frequencia[i].ToString();

                    soma = soma + frequencia[i];

                    listItem[i] = new ListViewItem(circleArray);
                    listViewIntervalFrequence.Items.Add(listItem[i]);
                }

            }

            System.Windows.Forms.Button buttonExport = new System.Windows.Forms.Button();
            buttonExport.SetBounds(10, 420, 100, 30);
            buttonExport.Text = "Exportar Planilha";
            buttonExport.Click += new System.EventHandler(this.exportListIntervalFrequence);
            formHistRadius.Controls.Add(buttonExport);

            formHistRadius.Controls.Add(listViewIntervalFrequence);
        }

        //Métodos de scroll e select
        //..........................................................................................................................
        public void onSelectRowPlane(object sender, EventArgs e)
        {
            String[] separator = { "{", "}", " ", "ListViewSubItem", ":" };
            String[] valuesPlan;
            String expression;

            ListView.SelectedListViewItemCollection collection = this.listViewPlanes.SelectedItems;

            foreach (ListViewItem item in collection)
            {
                expression = item.SubItems[0].ToString() + item.SubItems[1].ToString() + item.SubItems[2].ToString() + item.SubItems[3].ToString() + item.SubItems[4];

                valuesPlan = expression.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                PlanEquation.coeficienteX = Double.Parse(valuesPlan[2]);
                PlanEquation.coeficienteY = Double.Parse(valuesPlan[3]);
                PlanEquation.coeficienteZ = Double.Parse(valuesPlan[4]);
                PlanEquation.termoIndenpendente = Double.Parse(valuesPlan[5]);

                calculateIntersectionsPlane();

            }
        }

        private void onScrollRadius(object sender, System.EventArgs e)
        {
            Atributes.radiusSphere = (float)scrollRadius.Value / 1000;

            float valueScroll = (float)scrollRadius.Value / 1000;
            textBoxRadius.Text = (valueScroll).ToString();
        }

        public void onScrollQuantPlane(object sender, EventArgs e)
        {
            textBoxQuantPlanes.Text = scrollQuantPlanes.Value.ToString();
            Atributes.quantPlanes = scrollQuantPlanes.Value;
        }

        //Métodos para exibição de histogramas de área, diâmetro e raio
        //..........................................................................................................................
        public void onClickHistDiameter(object sender, EventArgs e)
        {
            int maximumFrequence = 0;       //Máximo global da frequencia
            double maximumInterval = 0;     //Máximo global do intervalo
            double maximumRadius = 0;       //Máximo global do raio
            double minimumRadius = 0;       //Mínimo global do raio

            System.Windows.Forms.Form formHistRadius = new System.Windows.Forms.Form();
            formHistRadius.SetBounds(400, 100, 800, 510);
            formHistRadius.Text = "Radius Histogram";

            try
            {
                StatisticCircles statisticSpheres = new StatisticCircles();
                int quantInterval = 0;
                int quantClass = statisticSpheres.getQuantClass();
                double maxRadius = statisticSpheres.maxRadiusCircle();
                double minRadius = statisticSpheres.minRadiusCircle();
                double amplitude = statisticSpheres.calcAmplitudeRadius();

                double minInterval = minRadius;
                double maxInterval = 0;
                double radius;

                List<Interval> listIntervalCircle = new List<Interval>();

                if (maxRadius != minRadius)
                {
                    while (quantInterval < quantClass)
                    {
                        maxInterval = minInterval + amplitude;
                        listIntervalCircle.Add(new Interval(Math.Round(minInterval, 8), Math.Round(maxInterval, 8)));
                        minInterval = maxInterval;
                        quantInterval++;
                    }
                }

                maximumInterval = listIntervalCircle[listIntervalCircle.Count - 1].getIntervalMin();

                ArrayList listRadiusCircle = new ArrayList();   //Minha lista de remoção das circunferências adotadas em um intervalo do histograma
                ArrayList listRadiusFix = new ArrayList();      //Minha lista fixa das circunferências

                for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                {
                    listRadiusCircle.Add(IntersectSolid.listCircle[i].getRadio());
                }

                listRadiusCircle.Sort();

                for (int i = 0; i < listRadiusCircle.Count; i++)
                {
                    listRadiusFix.Add(listRadiusCircle[i]);
                }

                maximumRadius = (double)listRadiusCircle[listRadiusCircle.Count - 1];
                minimumRadius = (double)listRadiusCircle[0];

                int count = 0;

                for (int i = 0; i < listIntervalCircle.Count; i++)
                {
                    for (int j = 0; j < listRadiusCircle.Count; j++)
                    {
                        radius = Math.Round(Convert.ToDouble(listRadiusCircle[j]), 8);

                        if ((radius >= listIntervalCircle[i].getIntervalMax()) && (radius <= listIntervalCircle[i].getIntervalMin()))
                        {

                            double min = listIntervalCircle[i].getIntervalMax();
                            double max = listIntervalCircle[i].getIntervalMin();

                            count++;
                            listIntervalCircle[i].incrementElements();
                            listRadiusCircle.RemoveAt(j);
                            j = 0;
                        }
                    }
                }

                //Ajuste para remoção de todos elementos
                if (listRadiusCircle.Count > 0)
                {
                    for (int i = 0; i < listIntervalCircle.Count; i++)
                    {
                        radius = Math.Round(Convert.ToDouble(listRadiusCircle[0]), 8);

                        if ((radius >= listIntervalCircle[i].getIntervalMax()) && (radius <= listIntervalCircle[i].getIntervalMin()))
                        {
                            double min = listIntervalCircle[i].getIntervalMax();
                            double max = listIntervalCircle[i].getIntervalMin();
                            count++;
                            listIntervalCircle[i].incrementElements();
                        }
                    }
                }
                listRadiusCircle.Clear();

                maximumFrequence = listIntervalCircle[listIntervalCircle.Count - 1].getFrequence();

                //Criação do gráfico do histograma do diâmetro e frequência
                try
                {
                    Chart chart = new Chart();
                    chart.SetBounds(10, 10, 400, 400);
                    chart.BackColor = System.Drawing.Color.Gray;

                    List<String> listAxisX = new List<String>();
                    for (int i = 0; i < listIntervalCircle.Count; i++)
                    {
                        listAxisX.Add((listIntervalCircle[i].getIntervalMax() / maximumInterval).ToString());
                    }

                    List<double> listAxisY = new List<double>();
                    for (int i = 0; i < listIntervalCircle.Count; i++)
                    {
                        double relativeFrequence = (double)(listIntervalCircle[i].getFrequence() / (double)IntersectSolid.listCircle.Count);

                        listAxisY.Add(relativeFrequence);
                    }

                    ChartArea chartArea = new ChartArea("chart1");
                    chartArea.AxisX.Title = "Radius";
                    chartArea.AxisY.Title = "Frequence";
                    chart.ChartAreas.Add(chartArea);

                    Series barSeries = new Series();
                    barSeries.Points.DataBindXY(listAxisX, listAxisY);
                    barSeries.ChartType = SeriesChartType.Column;
                    barSeries.ChartArea = "chart1";
                    chart.Series.Add(barSeries);

                    formHistRadius.Controls.Add(chart);

                    createLabelHistogramRadius(formHistRadius, listRadiusFix, quantInterval, listIntervalCircle);
                }
                catch (Exception exception)
                {
                    Console.WriteLine("Exception graph histogram diameter frequency: " + exception.ToString());
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception histogram radius: " + exception.ToString());
            }
            formHistRadius.Show();
        }

        public void onClickHistArea(object sender, EventArgs e)
        {
            int maximumFrequence = 0;           //Máximo global da frequencia
            double maximumInterval = 0;         //Máximo global do intervalo
            double maximumArea = 0;             //Máximo global do raio
            double minimumArea = 0;             //Mínimo global do raio

            System.Windows.Forms.Form formHistArea = new System.Windows.Forms.Form();
            formHistArea.SetBounds(400, 100, 800, 510);
            formHistArea.Text = "Area Histogram";

            try
            {
                StatisticCircles statisticSpheres = new StatisticCircles();
                int quantInterval = 0;
                int quantClass = statisticSpheres.getQuantClass();
                double maxArea = statisticSpheres.maxAreaCircle();
                double minArea = statisticSpheres.minAreaCircle();
                double amplitude = statisticSpheres.calcAmplitudeArea();

                double minInterval = minArea;
                double maxInterval = 0;
                double area;

                List<Interval> listIntervalCircle = new List<Interval>();

                if (maxArea != minArea)
                {
                    while (quantInterval < quantClass)
                    {
                        maxInterval = minInterval + amplitude;
                        listIntervalCircle.Add(new Interval(Math.Round(minInterval, 8), Math.Round(maxInterval, 8)));
                        minInterval = maxInterval;
                        quantInterval++;
                    }
                }

                maximumInterval = listIntervalCircle[listIntervalCircle.Count - 1].getIntervalMin();

                ArrayList listAreaCircle = new ArrayList();   //Minha lista de remoção das circunferências adotadas em um intervalo do histograma
                ArrayList listAreaFix = new ArrayList();      //Minha lista fixa das circunferências

                for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                {
                    listAreaCircle.Add(IntersectSolid.listCircle[i].getArea());
                }

                listAreaCircle.Sort();

                for (int i = 0; i < listAreaCircle.Count; i++)
                {
                    listAreaFix.Add(listAreaCircle[i]);
                }

                maximumArea = (double)listAreaCircle[listAreaCircle.Count - 1];
                minimumArea = (double)listAreaCircle[0];

                int count = 0;

                for (int i = 0; i < listIntervalCircle.Count; i++)
                {
                    for (int j = 0; j < listAreaCircle.Count; j++)
                    {
                        area = Math.Round(Convert.ToDouble(listAreaCircle[j]), 8);

                        if ((area >= listIntervalCircle[i].getIntervalMax()) && (area <= listIntervalCircle[i].getIntervalMin()))
                        {

                            double min = listIntervalCircle[i].getIntervalMax();
                            double max = listIntervalCircle[i].getIntervalMin();

                            count++;
                            listIntervalCircle[i].incrementElements();
                            listAreaCircle.RemoveAt(j);
                            j = 0;
                        }
                    }
                }

                //Ajuste para remoção de todos elementos
                if (listAreaCircle.Count > 0)
                {
                    for (int i = 0; i < listIntervalCircle.Count; i++)
                    {
                        area = Math.Round(Convert.ToDouble(listAreaCircle[0]), 8);

                        if ((area >= listIntervalCircle[i].getIntervalMax()) && (area <= listIntervalCircle[i].getIntervalMin()))
                        {
                            double min = listIntervalCircle[i].getIntervalMax();
                            double max = listIntervalCircle[i].getIntervalMin();
                            count++;
                            listIntervalCircle[i].incrementElements();
                        }
                    }
                }

                listAreaCircle.Clear();

                maximumFrequence = listIntervalCircle[listIntervalCircle.Count - 1].getFrequence();

                try
                {
                    Chart chart = new Chart();
                    chart.SetBounds(10, 10, 400, 400);
                    chart.BackColor = System.Drawing.Color.Gray;

                    List<String> listAxisX = new List<String>();
                    for (int i = 0; i < listIntervalCircle.Count; i++)
                    {
                        listAxisX.Add((listIntervalCircle[i].getIntervalMax() / maximumInterval).ToString());
                    }

                    List<double> listAxisY = new List<double>();
                    for (int i = 0; i < listIntervalCircle.Count; i++)
                    {
                        double relativeFrequence = (double)(listIntervalCircle[i].getFrequence() / (double)IntersectSolid.listCircle.Count);

                        listAxisY.Add(relativeFrequence);
                    }

                    ChartArea chartArea = new ChartArea("chart1");
                    chartArea.AxisX.Title = "Area";
                    chartArea.AxisY.Title = "Frequence";
                    chart.ChartAreas.Add(chartArea);

                    Series barSeries = new Series();
                    barSeries.Points.DataBindXY(listAxisX, listAxisY);
                    barSeries.ChartType = SeriesChartType.Column;
                    barSeries.ChartArea = "chart1";
                    chart.Series.Add(barSeries);

                    formHistArea.Controls.Add(chart);

                    createLabelHistogramArea(formHistArea, listAreaFix, quantInterval, listIntervalCircle);
                }
                catch (Exception exception)
                {
                    Console.WriteLine("Histo Error: " + exception);
                }

            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception.ToString());
            }

            formHistArea.Show();
        }

        public void onClickHistSaltyArea(object sender, EventArgs e)
        {

            this.saltVolumn = new SaltykovVolumn();
            this.saltVolumn.createFreqSectionArea();
            this.saltVolumn.createListFactor();
            this.saltVolumn.testCalcSaltykov(Saltykov.quantClass);

            System.Windows.Forms.Form formSaltyHist = new System.Windows.Forms.Form();
            formSaltyHist.SetBounds(400, 100, 800, 560);

            this.histArea = new HistoSaltyArea();

            formSaltyHist.Controls.Add(histArea.createHistogramn(saltVolumn.getListSaltyIntervalArea(), saltVolumn.getListfrequenceSectionArea(), "Area Interval", "Number of sections (NA(k,k))", 10,50,400,400));

            System.Windows.Forms.ListView listViewPlanSpheres = new System.Windows.Forms.ListView();
            listViewPlanSpheres.SetBounds(420, 50, 355, 400);

            listViewPlanSpheres.View = System.Windows.Forms.View.Details;
            listViewPlanSpheres.LabelEdit = false;
            listViewPlanSpheres.AllowColumnReorder = false;
            listViewPlanSpheres.CheckBoxes = false;
            listViewPlanSpheres.FullRowSelect = true;
            listViewPlanSpheres.GridLines = true;

            listViewPlanSpheres.Columns.Add("Plane Index", 150, HorizontalAlignment.Left);
            listViewPlanSpheres.Columns.Add("Number Spheres", 150, HorizontalAlignment.Left);


            if (IntersectSolid.listPlaneSpheres.Count != 0 & IntersectSolid.listPlaneSpheres != null)
            {
                List<ListViewItem> listItem = new List<ListViewItem>();

                String[] plane = new String[2000000];
                String[] sphere = new String[2000000];

                for (int i = 0; i < IntersectSolid.listPlaneSpheres.Count; i++)
                {
                    listItem.Add(new ListViewItem());
                }

                int totalCircles = 0;

                for (int i = 0; i < IntersectSolid.listPlaneSpheres.Count; i++)
                {
                    plane[i] = "Plane: " + ((i + 1)).ToString();
                    totalCircles = totalCircles + IntersectSolid.listPlaneSpheres[i].getSpheres().Count;
                    sphere[i] = IntersectSolid.listPlaneSpheres[i].getSpheres().Count.ToString() + " spheres/unit area";

                }

                for (int i = 0; i < IntersectSolid.listPlaneSpheres.Count; i++)
                {
                    String[] circleArray = new String[3];
                    circleArray[0] = plane[i].ToString();
                    circleArray[1] = sphere[i].ToString();

                    listItem[i] = new ListViewItem(circleArray);
                    listViewPlanSpheres.Items.Add(listItem[i]);
                }

                System.Windows.Forms.Label labelTotCirc = new System.Windows.Forms.Label();
                labelTotCirc.Text = "Number circles: " + totalCircles;
                labelTotCirc.SetBounds(420, 30, 200, 20);
                formSaltyHist.Controls.Add(labelTotCirc);
            }

            formSaltyHist.Controls.Add(listViewPlanSpheres);

            System.Windows.Forms.Button buttonQuantInterval = new System.Windows.Forms.Button();
            buttonQuantInterval.SetBounds(10, 450, 200, 30);
            buttonQuantInterval.Click += new System.EventHandler(this.onClickValuesIntervalSaltyArea);
            buttonQuantInterval.Text = "Interval";
            formSaltyHist.Controls.Add(buttonQuantInterval);

            formSaltyHist.Show();
        }

        public void onClickHistSaltyRadio(object sender, EventArgs e)
        {
            double radius;
            double maxRadius;

            try
            {
                System.Windows.Forms.Form formSaltyHist = new System.Windows.Forms.Form();
                formSaltyHist.SetBounds(400, 100, 800, 510);

                Saltykov saltykov = new Saltykov();

                maxRadius = saltykov.findMaxRadio();

                List<Interval> saltyInterval = saltykov.createIntervalDiameter();

                ArrayList listRemoveRadio = new ArrayList();

                for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                {
                    listRemoveRadio.Add(IntersectSolid.listCircle[i].getRadio()/maxRadius);
                }

                for (int i = 0; i < saltyInterval.Count; i++)
                {
                    for (int j = 0; j < listRemoveRadio.Count; j++)
                    {
                        radius = (double)listRemoveRadio[j];


                        if ((radius >= saltyInterval[i].getIntervalMin()) && (radius <= saltyInterval[i].getIntervalMax()))
                        {
                            double min = saltyInterval[i].getIntervalMax();
                            double max = saltyInterval[i].getIntervalMin();

                            saltyInterval[i].incrementElements();
                            listRemoveRadio.RemoveAt(j);
                            j = 0;
                        }
                    }
                }

                //Criação do histrograma com intervalos referências ao Saltykov
                HistoSaltyRadio hist = new HistoSaltyRadio(saltyInterval, maxRadius);
                formSaltyHist.Controls.Add(hist.createHistogramn());

                System.Windows.Forms.ListView listViewPlanSpheres = new System.Windows.Forms.ListView();
                listViewPlanSpheres.SetBounds(420, 50, 355, 400);

                listViewPlanSpheres.View = System.Windows.Forms.View.Details;
                listViewPlanSpheres.LabelEdit = false;
                listViewPlanSpheres.AllowColumnReorder = false;
                listViewPlanSpheres.CheckBoxes = false;
                listViewPlanSpheres.FullRowSelect = true;
                listViewPlanSpheres.GridLines = true;

                listViewPlanSpheres.Columns.Add("Plane Index", 150, HorizontalAlignment.Left);
                listViewPlanSpheres.Columns.Add("Number Spheres", 150, HorizontalAlignment.Left);

                try{

                    if (IntersectSolid.listPlaneSpheres.Count != 0 & IntersectSolid.listPlaneSpheres != null)
                    {
                        List<ListViewItem> listItem = new List<ListViewItem>();

                        String[] plane = new String[2000000];
                        String[] sphere = new String[2000000];

                        for (int i = 0; i < IntersectSolid.listPlaneSpheres.Count; i++)
                        {
                            listItem.Add(new ListViewItem());
                        }

                        int totalCircles = 0;

                        for (int i = 0; i < IntersectSolid.listPlaneSpheres.Count; i++)
                        {
                            plane[i] = "Plano: " + ((i+1)).ToString();
                            totalCircles = totalCircles + IntersectSolid.listPlaneSpheres[i].getSpheres().Count;
                            sphere[i] = IntersectSolid.listPlaneSpheres[i].getSpheres().Count.ToString() + " spheres/unit area";
 
                        }

                        for (int i = 0; i < IntersectSolid.listPlaneSpheres.Count; i++)
                        {
                            String[] circleArray = new String[3];
                            circleArray[0] = plane[i].ToString();
                            circleArray[1] = sphere[i].ToString();
 
                            listItem[i] = new ListViewItem(circleArray);
                            listViewPlanSpheres.Items.Add(listItem[i]);
                        }

                        System.Windows.Forms.Label labelTotCirc = new System.Windows.Forms.Label();
                        labelTotCirc.Text = "Total circles: " + totalCircles;
                        labelTotCirc.SetBounds(420, 30, 200, 20);
                        formSaltyHist.Controls.Add(labelTotCirc);

                    }
                }
                catch (Exception exception)
                {

                }
                
                formSaltyHist.Controls.Add(listViewPlanSpheres);


                formSaltyHist.Show();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception.ToString());
            }
        }

        //Métodos para a abertura de abas
        //..........................................................................................................................

        //Método onClickValuesIntervalSaltArea: resultado da interação do algoritmo de Saltykov
        //para o resultado da quantidade de esfereas no volume unitário
        public void onClickValuesIntervalSaltyArea(object sender, EventArgs e)
        {
            double somaFrequenciaAbsoluta = 0;
            double somaFrequenciaRelativa = 0;

            System.Windows.Forms.Form formSaltyAreaInterval = new System.Windows.Forms.Form();
            formSaltyAreaInterval.SetBounds(450, 100, 1200, 550);
            formSaltyAreaInterval.Text = "Created Intervals";

            this.histArea = new HistoSaltyArea(this.saltVolumn.getListSaltyIntervalArea(), this.saltVolumn.getListSaltyIntervalRadius(), this.saltVolumn.getMaxArea());
            formSaltyAreaInterval.Controls.Add(histArea.createHistogramn(saltVolumn.getListSaltyIntervalArea(), this.saltVolumn.getListQuantSecByDiamSize(), "Intervalo de área", "Quantidade de secções (NA)", 770, 40, 400, 400));


            System.Windows.Forms.Label labelTitle = new System.Windows.Forms.Label();
            labelTitle.Text = "Result of the cross-sections by area interval of the spherical sections: ";
            labelTitle.SetBounds(10,20, 300, 20);
            formSaltyAreaInterval.Controls.Add(labelTitle);

            System.Windows.Forms.Label labelResultSaltykov = new System.Windows.Forms.Label();
            labelResultSaltykov.SetBounds(10, 470, 500, 30);
            labelResultSaltykov.Font = new System.Drawing.Font("Arial", 14, System.Drawing.FontStyle.Regular);
            labelResultSaltykov.Text = "Result of applying the method: ";
            formSaltyAreaInterval.Controls.Add(labelResultSaltykov);

            System.Windows.Forms.ListView listViewResultInterval = new System.Windows.Forms.ListView();
            listViewResultInterval.SetBounds(10, 40, 750, 200);

            listViewResultInterval.View = System.Windows.Forms.View.Details;
            listViewResultInterval.LabelEdit = false;
            listViewResultInterval.AllowColumnReorder = false;
            listViewResultInterval.CheckBoxes = false;
            listViewResultInterval.FullRowSelect = true;
            listViewResultInterval.GridLines = true;

            listViewResultInterval.Columns.Add("Index", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Interval", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Absolute Frequence", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Relative Frequence", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Index Number Sections", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Total by Diameter", 150, HorizontalAlignment.Left);

            List<double> listTest = new List<double>();

                if (this.saltVolumn.getListSaltyIntervalArea().Count != 0)
                {
                    List<ListViewItem> listItem = new List<ListViewItem>();

                    String[] index = new String[2000000];
                    String[] intervalo = new String[2000000];
                    String[] quantidade = new String[2000000];
                    String[] quantidadeRel = new String[2000000];
                    String[] indexNa = new String[2000000];
                    String[] numeroNa = new String[2000000];


                    for (int i = 0; i < this.saltVolumn.getListSaltyIntervalArea().Count; i++)
                    {
                        listItem.Add(new ListViewItem());

                        index[i] = "Index: " + ((i + 1)).ToString();
                        intervalo[i] = Math.Round((this.saltVolumn.getListSaltyIntervalArea()[i].getIntervalMax()), 4) + "-" + Math.Round(this.saltVolumn.getListSaltyIntervalArea()[i].getIntervalMin(), 4);
                        quantidade[i] = this.saltVolumn.getListSaltyIntervalArea()[i].getFrequence().ToString();
                        quantidadeRel[i] = ((double)this.saltVolumn.getListSaltyIntervalArea()[i].getFrequence()/(double)this.histArea.getTotalFrequence()).ToString();
                        indexNa[i] = ("N(" + (i+1) + "," + (i+1) + ")").ToString();
                        numeroNa[i] = this.saltVolumn.getListQuantSecByDiamSize()[i].ToString();
                        
                        listTest.Add((double)this.saltVolumn.getListSaltyIntervalArea()[i].getFrequence() / (double)histArea.getTotalFrequence());

                        somaFrequenciaAbsoluta = somaFrequenciaAbsoluta + this.saltVolumn.getListSaltyIntervalArea()[i].getFrequence();
                        somaFrequenciaRelativa = somaFrequenciaRelativa + (double) this.saltVolumn.getListSaltyIntervalArea()[i].getFrequence() /(double) this.saltVolumn.getListCircle().Count;
                    }


                for (int i = 0; i < this.saltVolumn.getListSaltyIntervalArea().Count; i++)
                    {
                        String[] circleArray = new String[6];
                        circleArray[0] = index[i].ToString();
                        circleArray[1] = intervalo[i].ToString();
                        circleArray[2] = quantidade[i].ToString();
                        circleArray[3] = quantidadeRel[i].ToString();
                        circleArray[4] = indexNa[i].ToString();
                        circleArray[5] = numeroNa[i].ToString();

                        listItem[i] = new ListViewItem(circleArray);
                        listViewResultInterval.Items.Add(listItem[i]);
                    }

                    System.Windows.Forms.Label labelFactors = new System.Windows.Forms.Label();
                    labelFactors.Text = "Weighting Factor";
                    labelFactors.SetBounds(10, 250, 300, 20);
                    formSaltyAreaInterval.Controls.Add(labelFactors);

                    System.Windows.Forms.ListView listViewFactors = new System.Windows.Forms.ListView();
                    listViewFactors.SetBounds(10, 270, 300, 170);

                    listViewFactors.View = System.Windows.Forms.View.Details;
                    listViewFactors.LabelEdit = false;
                    listViewFactors.AllowColumnReorder = false;
                    listViewFactors.CheckBoxes = false;
                    listViewFactors.FullRowSelect = true;
                    listViewFactors.GridLines = true;

                    listViewFactors.Columns.Add("Index", 150, HorizontalAlignment.Left);
                    listViewFactors.Columns.Add("Weighting Factor", 150, HorizontalAlignment.Left);

                    String[] indexFactor = new String[300];
                    String[] pondFactor = new String[300];

                    List<ListViewItem> listItemFactor = new List<ListViewItem>();

                    for (int i = 0; i < this.saltVolumn.getListFactor().Count; i++)
                    {
                        listItemFactor.Add(new ListViewItem());
                        indexFactor[i] = (i).ToString();
                        pondFactor[i] = this.saltVolumn.getListFactor()[i].ToString();
                    }

                    for (int i = 0; i < this.saltVolumn.getListFactor().Count; i++)
                    {
                        String[] circleArray = new String[2];
                        circleArray[0] = indexFactor[i].ToString();
                        circleArray[1] = pondFactor[i].ToString();
                        
                        listItemFactor[i] = new ListViewItem(circleArray);
                        listViewFactors.Items.Add(listItemFactor[i]);
                    }
                formSaltyAreaInterval.Controls.Add(listViewFactors);
                createFullListFactorsCalcSaltykov(formSaltyAreaInterval);

                double prop = saltVolumn.getProportionalRoot();
                double na = SaltykovVolumn.listSaltykovFactors[0].getListFactorResult()[0];
                double totPlans = (double)Atributes.quantPlanes;
                double radio = (double)saltVolumn.getListCircle()[0].getSphere().getRadius();
                double razao = totPlans / na;

                double quantTest = (prop / razao) / radio;

                labelResultSaltykov.Text = "Application Result (Saltykov): " + quantTest;

                System.Windows.Forms.Button buttonMatrix = new System.Windows.Forms.Button();
                buttonMatrix.Text = "Factor Matrix";
                buttonMatrix.Click += new System.EventHandler(this.onClickMatrixValuesFactor);
                buttonMatrix.SetBounds(800, 455, 200, 30);
                formSaltyAreaInterval.Controls.Add(buttonMatrix);

            }
            else
            {
                Console.WriteLine("Não existem esferas no systemStereology");
            }

            formSaltyAreaInterval.Controls.Add(listViewResultInterval);
            formSaltyAreaInterval.ShowDialog();
        }

        public void onClickMatrixValuesFactor(object sender, EventArgs e)
        {
            System.Windows.Forms.Form formMatrix = new System.Windows.Forms.Form();
            formMatrix.SetBounds(100, 400, 700, 500);
            formMatrix.Text = "Matrix de frequencias ponderadas";

            System.Windows.Forms.ListView listViewMatrix = new System.Windows.Forms.ListView();
            listViewMatrix.SetBounds(10, 10, 670, 430);

            listViewMatrix.View = System.Windows.Forms.View.Details;
            listViewMatrix.LabelEdit = false;
            listViewMatrix.AllowColumnReorder = false;
            listViewMatrix.CheckBoxes = false;
            listViewMatrix.FullRowSelect = true;
            listViewMatrix.GridLines = true;

            for(int i = 0; i < SaltykovVolumn.listSaltykovFactors.Count; i++)
            {
                listViewMatrix.Columns.Add("Índice", 150, HorizontalAlignment.Left);
            }

            string[,] pondFactor = new string[200,200];

            List<ListViewItem> listItemFactor = new List<ListViewItem>();

            for(int i = 0; i < SaltykovVolumn.listSaltykovFactors.Count; i++)
            {
                listItemFactor.Add(new ListViewItem());

                for(int j = 0; j < SaltykovVolumn.listSaltykovFactors[i].getListFactorResult().Count; j++)
                {
                    pondFactor[i,j] = SaltykovVolumn.listSaltykovFactors[i].getListFactorResult()[j].ToString();
                }
            }

            for(int i = 0; i < SaltykovVolumn.listSaltykovFactors.Count; i++)
            {
                String[] factorConsolid = new String[1000];

                for (int j = 0; j < SaltykovVolumn.listSaltykovFactors[i].getListFactorResult().Count; j++)
                {
                    factorConsolid[j] = pondFactor[i, j];
                }

                listItemFactor[i] = new ListViewItem(factorConsolid);
                listViewMatrix.Items.Add(listItemFactor[i]);
            }

            formMatrix.Controls.Add(listViewMatrix);

            formMatrix.ShowDialog();

        }

        public void onClickValuesSatyArea(object sender, EventArgs e){

            System.Windows.Forms.Form formValuesSalty = new System.Windows.Forms.Form();
            formValuesSalty.SetBounds(400, 100, 800, 500);
            formValuesSalty.Text = "Atributos do cálculo";

            System.Windows.Forms.Label labelTitle = new System.Windows.Forms.Label();
            labelTitle.SetBounds(10, 10, 300, 20);
            labelTitle.Text = "Atibutos para o cálculo do método Saltykov: ";
            formValuesSalty.Controls.Add(labelTitle);

            System.Windows.Forms.ListView listViewResultInterval = new System.Windows.Forms.ListView();
            listViewResultInterval.SetBounds(10, 40, 500, 300);

            listViewResultInterval.View = System.Windows.Forms.View.Details;
            listViewResultInterval.LabelEdit = false;
            listViewResultInterval.AllowColumnReorder = false;
            listViewResultInterval.CheckBoxes = false;
            listViewResultInterval.FullRowSelect = true;
            listViewResultInterval.GridLines = true;


            listViewResultInterval.Columns.Add("Índice", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Intervalo", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Frequencia Absoluta", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Frequência Relativa", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Raio", 150, HorizontalAlignment.Left);
            listViewResultInterval.Columns.Add("Constante", 150, HorizontalAlignment.Left);

            try
            {
                if(this.saltyIntervalArea.Count != 0)
                {
                    List<ListViewItem> listItem = new List<ListViewItem>();

                    String[] index = new String[2000000];
                    String[] intervalo = new String[2000000];
                    String[] quantidade = new String[2000000];
                    String[] constVal = new String[2000000];
                    String[] raio = new String[2000000];
                    String[] frequencePerc = new String[2000000];

                    double[] constant = { 1.6461, 0.4561, 0.1162, 0.0415, 0.0173, 0.0079, 0.0038, 0.0018, 0.0010, 0.0003, 0.0002, 0.0002 };

                    for (int i =0; i < this.saltyIntervalArea.Count; i++)
                    {                   
                        listItem.Add(new ListViewItem());

                        index[i] = "Índice: " + ((i + 1)).ToString();
                        intervalo[i] = Math.Round(saltyIntervalArea[i].getIntervalMax(), 4) + "-" + Math.Round(saltyIntervalArea[i].getIntervalMin(), 4); 
                        quantidade[i] = saltyIntervalArea[i].getFrequence().ToString();
                        constVal[i] = constant[11-i].ToString();
                        raio[i] = saltyIntervalRadius[i].getIntervalMax().ToString();
                        frequencePerc[i] = ((double)saltyIntervalArea[i].getFrequence() / histArea.getTotalFrequence()).ToString();
                    }

                    for (int i = 0; i < this.saltyIntervalArea.Count; i++)
                    {
                        String[] circleArray = new String[6];
                        circleArray[0] = index[i].ToString();
                        circleArray[1] = intervalo[i].ToString();
                        circleArray[2] = quantidade[i].ToString();
                        circleArray[3] = frequencePerc[i].ToString();
                        circleArray[4] = raio[i].ToString();
                        circleArray[5] = constVal[i].ToString();

                        listItem[i] = new ListViewItem(circleArray);
                        listViewResultInterval.Items.Add(listItem[i]);
                    }
                }

            }catch(Exception exception)
            {

            }

            formValuesSalty.Controls.Add(listViewResultInterval);

            formValuesSalty.Show();
        }

        public void onClickCreatePlanes(object sender, EventArgs e)
        {

            planeUtils = new PlaneUtils();

            Random random1 = new Random();
            Random random2 = new Random();
            Random random3 = new Random();
            Random random4 = new Random();
            Random randPlan = new Random();

            initTimer();

            for (int i = 0; i < Atributes.quantPlanes; i++)
            {
                PlanEquation planeEquation = new PlanEquation(i);

                planOption = randPlan.Next(1, 4);

                switch (planOption)
                {
                    case 1:

                        planeEquation.createPerpRandomPointSerieA(random1, random2, 0.0f, 1.0f);
                        planeEquation.setIndexOptionPlane(1);
                        planeEquation.getPlanEquation();

                        planeUtils.addPlaneInListEquation(planeEquation);
                        staticListPlaneEquation.Add(planeEquation);

                        break;

                    case 2:

                        planeEquation.createPerpRandomPointSerieB(random3, random4, 0.0f, 1.0f);
                        planeEquation.setIndexOptionPlane(2);
                        planeEquation.getPlanEquation();

                        planeUtils.addPlaneInListEquation(planeEquation);
                        staticListPlaneEquation.Add(planeEquation);

                        break;

                    case 3:

                        planeEquation.createPerpRandomPointSerieC(random1, random2, 0.0f, 1.0f);
                        planeEquation.setIndexOptionPlane(3);
                        planeEquation.getPlanEquation();

                        planeUtils.addPlaneInListEquation(planeEquation);
                        staticListPlaneEquation.Add(planeEquation);

                        break;
                }

                progressBar.Increment(1);
            }

            try
            {
                IntersectSolid intersectSolid = new IntersectSolid(Stereology.listEsferas, PlaneUtils.staticListPlaneEquation);

            }
            catch (Exception exception)
            {
               
            }


            MainForm.panelPlanes.addElementsInListView(planeUtils);

        }

        private void onClickMenuCreatePlane(object sender, EventArgs e)
        {
            MainForm.optionView = 2;
            MainForm.enableVolume = true;

            formCreatePlane = new System.Windows.Forms.Form();
            formCreatePlane.SetBounds(400, 100, 500, 500);
            formCreatePlane.Text = "Create Planes";

            System.Windows.Forms.Label labelInfo = new System.Windows.Forms.Label();
            labelInfo.Text = "Create planes for intersecting with spheres";
            labelInfo.Font = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Bold);
            labelInfo.SetBounds(10, 10, 500, 20);
            formCreatePlane.Controls.Add(labelInfo);

            Random random = new Random();

            PlanEquation planEquation = new PlanEquation();
            planEquation.createRandomPoints(random, -0.5f, 0.5f);
            planEquation.getPlanEquation();

            //Cálculo da intersecção dos planos com as esferas no espaço unitário
            calculateIntersectionsPlane();

            System.Windows.Forms.Label labelVectorAB = new System.Windows.Forms.Label();
            labelVectorAB.Text = "Vector coordinates 1: " + "( " + planEquation.getVectorAB().getX().ToString("0.00") + " ; " +
                                                                    planEquation.getVectorAB().getY().ToString("0.00") + " ; " +
                                                                    planEquation.getVectorAB().getZ().ToString("0.00") + " )";
            labelVectorAB.SetBounds(10, 40, 450, 20);
            formCreatePlane.Controls.Add(labelVectorAB);

            System.Windows.Forms.Label labelVectorAC = new System.Windows.Forms.Label();
            labelVectorAC.Text = "Vector coordinates 2: " + "( " + planEquation.getVectorAC().getX().ToString("0.00") + " ;" +
                                                                    planEquation.getVectorAC().getY().ToString("0.00") + " ;" +
                                                                    planEquation.getVectorAC().getZ().ToString("0.00") + " )";
            labelVectorAC.SetBounds(10, 60, 450, 20);
            formCreatePlane.Controls.Add(labelVectorAC);

            System.Windows.Forms.Label labelPlanEquation = new System.Windows.Forms.Label();
            labelPlanEquation.Text = "Equation of the plane: " + planEquation.getPlanEquation();
            labelPlanEquation.SetBounds(10, 80, 450, 20);
            formCreatePlane.Controls.Add(labelPlanEquation);

            System.Windows.Forms.Label labelPointReference = new System.Windows.Forms.Label();
            labelPointReference.Text = "Point of reference in space: " + "(" + PlanEquation.listPoint[0].getX().ToString("0.00") + ", " +
                                                                                 PlanEquation.listPoint[0].getY().ToString("0.00") + ", " +
                                                                                 PlanEquation.listPoint[0].getZ().ToString("0.00") + ")";
            labelPointReference.SetBounds(10, 100, 450, 20);
            formCreatePlane.Controls.Add(labelPointReference);

            System.Windows.Forms.Label labelColorPlane = new System.Windows.Forms.Label();
            labelColorPlane.Text = "Setting the color of the planes: ";
            labelColorPlane.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            labelColorPlane.SetBounds(10, 130, 450, 20);
            formCreatePlane.Controls.Add(labelColorPlane);

            scrollColorPlaneR = new System.Windows.Forms.HScrollBar();
            scrollColorPlaneR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollColorPlaneR.SetBounds(10, 160, 200, 15);
            scrollColorPlaneR.Maximum = 264;
            scrollColorPlaneR.Minimum = 0;
            scrollColorPlaneR.TabIndex = 0;
            scrollColorPlaneR.Value = (int)(Atributes.colorPlaneRed);
            scrollColorPlaneR.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollRedPlane);
            formCreatePlane.Controls.Add(scrollColorPlaneR);

            scrollColorPlaneG = new System.Windows.Forms.HScrollBar();
            scrollColorPlaneG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollColorPlaneG.SetBounds(10, 180, 200, 15);
            scrollColorPlaneG.Maximum = 264;
            scrollColorPlaneG.Minimum = 0;
            scrollColorPlaneG.TabIndex = 1;
            scrollColorPlaneG.Value = (int)(Atributes.colorPlaneGreen);
            scrollColorPlaneG.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollGreenPlane);
            formCreatePlane.Controls.Add(scrollColorPlaneG);

            scrollColorPlaneB = new System.Windows.Forms.HScrollBar();
            scrollColorPlaneB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollColorPlaneB.SetBounds(10, 200, 200, 15);
            scrollColorPlaneB.Maximum = 264;
            scrollColorPlaneB.Minimum = 0;
            scrollColorPlaneB.TabIndex = 2;
            scrollColorPlaneB.Value = (int)(Atributes.colorPlaneBlue);
            scrollColorPlaneB.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollBluePlane);
            formCreatePlane.Controls.Add(scrollColorPlaneB);

            textBoxPlaneR = new System.Windows.Forms.TextBox();
            textBoxPlaneR.SetBounds(230, 160, 30, 30);
            textBoxPlaneR.Enabled = false;
            textBoxPlaneR.Text = (Atributes.colorPlaneRed).ToString();
            formCreatePlane.Controls.Add(textBoxPlaneR);

            textBoxPlaneG = new System.Windows.Forms.TextBox();
            textBoxPlaneG.SetBounds(230, 180, 30, 30);
            textBoxPlaneG.Enabled = false;
            textBoxPlaneG.Text = (Atributes.colorPlaneGreen).ToString();
            formCreatePlane.Controls.Add(textBoxPlaneG);

            textBoxPlaneB = new System.Windows.Forms.TextBox();
            textBoxPlaneB.SetBounds(230, 200, 30, 30);
            textBoxPlaneB.Enabled = false;
            textBoxPlaneB.Text = (Atributes.colorPlaneBlue).ToString();
            formCreatePlane.Controls.Add(textBoxPlaneB);

            System.Windows.Forms.Label labelAutoPlane = new System.Windows.Forms.Label();
            labelAutoPlane.Text = "Create Planes: ";
            labelAutoPlane.SetBounds(10, 250, 200, 30);
            labelAutoPlane.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            formCreatePlane.Controls.Add(labelAutoPlane);


            scrollQuantPlanes = new System.Windows.Forms.HScrollBar();
            scrollQuantPlanes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollQuantPlanes.SetBounds(10, 280, 200, 15);
            scrollQuantPlanes.Maximum = 100009;
            scrollQuantPlanes.Minimum = 1;
            scrollQuantPlanes.Value = (int)(Atributes.quantPlanes);
            scrollQuantPlanes.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollQuantPlane);
            formCreatePlane.Controls.Add(scrollQuantPlanes);

            textBoxQuantPlanes = new System.Windows.Forms.TextBox();
            textBoxQuantPlanes.SetBounds(230, 280, 50, 30);
            textBoxQuantPlanes.Enabled = false;
            textBoxQuantPlanes.Text = Atributes.quantPlanes.ToString();
            formCreatePlane.Controls.Add(textBoxQuantPlanes);

            System.Windows.Forms.Button buttonCreatePlanes = new System.Windows.Forms.Button();
            buttonCreatePlanes.SetBounds(10, 310, 150, 25);
            buttonCreatePlanes.Text = "Create random plane sequences:";
            buttonCreatePlanes.Click += new System.EventHandler(this.onClickCreatePlanes);
            formCreatePlane.Controls.Add(buttonCreatePlanes);

            System.Windows.Forms.Button buttonShowListPlanes = new System.Windows.Forms.Button();
            buttonShowListPlanes.SetBounds(170, 310, 150, 25);
            buttonShowListPlanes.Text = "Show list of planes";
            buttonShowListPlanes.Click += new System.EventHandler(this.onClickShowListPlanes);
            formCreatePlane.Controls.Add(buttonShowListPlanes);

            System.Windows.Forms.Label labelProgress = new System.Windows.Forms.Label();
            labelProgress.SetBounds(10, 350, 200, 20);
            labelProgress.Text = "Progress";
            formCreatePlane.Controls.Add(labelProgress);

            progressBar = new ProgressBar();
            progressBar.SetBounds(10, 380, 465, 30);
            progressBar.BackColor = System.Drawing.Color.Red;
            formCreatePlane.Controls.Add(progressBar);

            formCreatePlane.ShowDialog();

        }

        private void onClickCircleArea(object sender, EventArgs e)
        {
            System.Windows.Forms.Form formArea = new System.Windows.Forms.Form();
            formArea.Text = "Área das circunferências";
            formArea.SetBounds(100, 400, 500, 500);

            System.Windows.Forms.ListView listViewCircleArea = new System.Windows.Forms.ListView();
            listViewCircleArea.SetBounds(10, 30, 465, 420);

            listViewCircleArea.View = System.Windows.Forms.View.Details;
            listViewCircleArea.LabelEdit = false;
            listViewCircleArea.AllowColumnReorder = false;
            listViewCircleArea.CheckBoxes = false;
            listViewCircleArea.FullRowSelect = true;
            listViewCircleArea.GridLines = true;

            listViewCircleArea.Columns.Add("Circunferência", 150, HorizontalAlignment.Left);
            listViewCircleArea.Columns.Add("Área", 150, HorizontalAlignment.Left);
            listViewCircleArea.Columns.Add("Raio", 150, HorizontalAlignment.Left);
            listViewCircleArea.Columns.Add("Diâmetro", 150, HorizontalAlignment.Left);
            listViewCircleArea.Columns.Add("Plano", 150, HorizontalAlignment.Left);
            listViewCircleArea.Columns.Add("Esfera", 150, HorizontalAlignment.Left);

            try
            {
                if (IntersectSolid.listCircle.Count > 0)
                {
                    List<ListViewItem> listItem = new List<ListViewItem>();

                    double[] area = new double[5000000];
                    double[] radius = new double[5000000];
                    double[] diameter = new double[500000];
                    String[] plane = new String[5000000];
                    String[] sphere = new String[5000000];


                    for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                    {
                        listItem.Add(new ListViewItem());
                    }

                    for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                    {
                        area[i] = IntersectSolid.listCircle[i].getArea();
                        radius[i] = IntersectSolid.listCircle[i].getRadio();
                        diameter[i] = 2 * IntersectSolid.listCircle[i].getRadio();
                        plane[i] = "Plano " + (IntersectSolid.listCircle[i].getIndexPlane() + 1).ToString();
                        sphere[i] = "Esfera " + IntersectSolid.listCircle[i].getIndexSpheres().ToString();
                    }

                    for (int i = 0; i < IntersectSolid.listCircle.Count; i++)
                    {
                        String[] circleArray = new String[6];
                        circleArray[0] = "Circunferência: " + (i + 1).ToString();
                        circleArray[1] = area[i].ToString();
                        circleArray[2] = radius[i].ToString();
                        circleArray[3] = diameter[i].ToString();
                        circleArray[4] = plane[i];
                        circleArray[5] = sphere[i];

                        listItem[i] = new ListViewItem(circleArray);
                        listViewCircleArea.Items.Add(listItem[i]);
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception);
            }

            //Thread threadListAreaCircle = new Thread(() => createListArea(listViewCircleArea, formArea));
            //threadListAreaCircle.Start();

            formArea.Controls.Add(listViewCircleArea);
            formArea.Show();
        }

        private void onClickMenuInfoSpheres(object sender, EventArgs e)
        {

            formInfo = new System.Windows.Forms.Form();
            formInfo.SetBounds(100, 100, 680, 465);
            formInfo.Text = "Informações das Spheres";

            System.Windows.Forms.Label labelQuantSpheres = new System.Windows.Forms.Label();
            labelQuantSpheres.SetBounds(20, 20, 200, 20);
            labelQuantSpheres.Text = "Quantidade de esferas na matriz: " + Stereology.listEsferas.Count + " esfera(s).";
            formInfo.Controls.Add(labelQuantSpheres);

            System.Windows.Forms.Label labelAvarageRadius = new System.Windows.Forms.Label();
            labelAvarageRadius.SetBounds(20, 40, 200, 20);
            labelAvarageRadius.Text = "Raio médio das esferas: " + calculateAvarageRadius().ToString("0.00") + " u.c";
            formInfo.Controls.Add(labelAvarageRadius);

            System.Windows.Forms.Label labelVolumnSpheres = new System.Windows.Forms.Label();
            labelVolumnSpheres.SetBounds(20, 60, 200, 20);
            labelVolumnSpheres.Text = "Volume total das esferas: " + calculateTotalVolumn().ToString("0.0000") + " u.v";
            formInfo.Controls.Add(labelVolumnSpheres);

            System.Windows.Forms.Label labelVolumnMatrix = new System.Windows.Forms.Label();
            labelVolumnMatrix.SetBounds(20, 80, 200, 20);
            labelVolumnMatrix.Text = "Volume da matriz: " + calculateTotalMatrix().ToString("0.0000") + "u.v";
            formInfo.Controls.Add(labelVolumnMatrix);

            System.Windows.Forms.Label labelTitleListSpheres = new System.Windows.Forms.Label();
            labelTitleListSpheres.Text = "Lista das esferas criadas";
            labelTitleListSpheres.SetBounds(20, 100, 200, 20);
            labelTitleListSpheres.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            formInfo.Controls.Add(labelTitleListSpheres);


            System.Windows.Forms.Button buttonListSpheres = new System.Windows.Forms.Button();
            buttonListSpheres.SetBounds(20, 120, 150, 30);
            buttonListSpheres.Text = "Exibir lista de esferas";
            buttonListSpheres.Click += new System.EventHandler(this.onClickListSpheres);
            formInfo.Controls.Add(buttonListSpheres);

            StatisticSpheres statistic = new StatisticSpheres();
            int quantInterval = 0;                              //quantidade de intervalos criados
            int quantClass = statistic.getQuantClass();         //retorna a quantidade de classes que o histograma terá
            double maxRadius = statistic.maxRadiusSphere();     //retorna o maior raio da lista de esferas
            double minRadius = statistic.minRadiusSphere();     //retorna o menor raio da lista de esferas
            double amplitude = statistic.calcAmplitude();

            //Início do cálculo para criar os intervalos

            double minInterval = minRadius;
            double maxInterval = 0;
            double radius;

            List<Interval> listInterval = new List<Interval>();

            //Somente executo essa operação se tenho esferas com diferentes raios
            if (maxRadius != minRadius)
            {
                while (quantInterval < quantClass)
                {
                    maxInterval = minInterval + amplitude;

                    listInterval.Add(new Interval(Math.Round(minInterval, 8), Math.Round(maxInterval, 8)));

                    minInterval = maxInterval;

                    quantInterval++;
                }
            }

            //Criação de uma lista para obtenção dos raios das esferas
            ArrayList listRadius = new ArrayList();

            for (int i = 0; i < Stereology.listEsferas.Count; i++)
            {
                listRadius.Add(Stereology.listEsferas[i].getRadius());
            }

            listRadius.Sort();

            for (int i = 0; i < listInterval.Count; i++)
            {
                for (int j = 0; j < listRadius.Count; j++)
                {
                    radius = Math.Round(Convert.ToDouble(listRadius[j]), 8);

                    if (radius < listInterval[0].getIntervalMax())
                    {
                    }

                    if (radius > listInterval[listInterval.Count - 1].getIntervalMin())
                    {
                    }

                    if ((radius >= listInterval[i].getIntervalMax()) && (radius <= listInterval[i].getIntervalMin()))
                    {
                        double min = listInterval[i].getIntervalMax();
                        double max = listInterval[i].getIntervalMin();

                        listInterval[i].incrementElements();
                    }
                }
            }

            try
            {
                //Criação de um gráfico na forma de histograma para exibir as informaçãos referesntes ao raio da esfera
                Chart chart = new Chart();
                chart.SetBounds(250, 10, 400, 400);
                chart.BackColor = System.Drawing.Color.Gray;

                //Criação dos valores que serão apresentados no histograma

                List<String> listAxisX = new List<String>();
                for (int i = 0; i < listInterval.Count; i++)
                {
                    listAxisX.Add(listInterval[i].getIntervalMin().ToString());
                }

                List<int> listAxisY = new List<int>();
                for (int i = 0; i < listInterval.Count; i++)
                {
                    listAxisY.Add(listInterval[i].getFrequence());
                }

                ChartArea chartArea = new ChartArea("chart1");
                chartArea.AxisX.Title = "Raio";
                chartArea.AxisY.Title = "Frequência";
                chart.ChartAreas.Add(chartArea);

                Series barSeries = new Series();
                barSeries.Points.DataBindXY(listAxisX, listAxisY);
                barSeries.ChartType = SeriesChartType.Column;
                barSeries.ChartArea = "chart1";
                chart.Series.Add(barSeries);

                formInfo.Controls.Add(chart);

            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception chart: " + exception);
            }


            formInfo.Show();
        }

        private void onClickMenuRadius(object sender, EventArgs e)
        {
            formRadius = new System.Windows.Forms.Form();
            formRadius.SetBounds(400, 100, 300, 130);
            formRadius.Text = "Raio das Spheres";
            formRadius.MaximizeBox = false;

            System.Windows.Forms.Label labelTitle = new System.Windows.Forms.Label();
            labelTitle.Text = "Configuração do raio das esferas";
            labelTitle.SetBounds(10, 10, 200, 30);
            formRadius.Controls.Add(labelTitle);

            scrollRadius = new System.Windows.Forms.HScrollBar();
            scrollRadius.SetBounds(10, 40, 200, 15);
            scrollRadius.Minimum = 0;
            scrollRadius.Maximum = 209;
            scrollRadius.Value = (int)(Atributes.radiusSphere * 1000);
            scrollRadius.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollRadius);
            formRadius.Controls.Add(scrollRadius);

            textBoxRadius = new System.Windows.Forms.TextBox();
            textBoxRadius.SetBounds(220, 40, 40, 30);
            textBoxRadius.Enabled = false;
            textBoxRadius.Text = (Atributes.radiusSphere).ToString();
            formRadius.Controls.Add(textBoxRadius);

            formRadius.ShowDialog();
        }

        //Métodos referentes aos cálculos de médias e quantidades
        //..........................................................................................................................
        public void calculateIntersectionsPlane()
        {

            //showInformations();

            listIntersect = new List<PointLocation>();

            foreach (LineEquation line in Stereology.listLine)
            {
                double coordPointX = line.getPointA().getX();
                double coordPointY = line.getPointA().getY();
                double coordPointZ = line.getPointA().getZ();

                double coordVecX = line.getVector().getX();
                double coordVecY = line.getVector().getY();
                double coordVecZ = line.getVector().getZ();

                double numerador = -1 * ((PlanEquation.coeficienteX * line.getPointA().getX()) +
                                         (PlanEquation.coeficienteY * line.getPointA().getY()) +
                                         (PlanEquation.coeficienteZ * line.getPointA().getZ()) +
                                         (PlanEquation.termoIndenpendente));

                double denominador = (PlanEquation.coeficienteX * line.getVector().getX()) +
                                     (PlanEquation.coeficienteY * line.getVector().getY()) +
                                     (PlanEquation.coeficienteZ * line.getVector().getZ());

                if (denominador != 0)
                {
                    double parametro = numerador / denominador;

                    double x = line.getPointA().getX() + (parametro * line.getVector().getX());
                    double y = line.getPointA().getY() + (parametro * line.getVector().getY());
                    double z = line.getPointA().getZ() + (parametro * line.getVector().getZ());

                    listIntersect.Add(new PointLocation(x, y, z));
                }
            }
        }

        public float calculateAvarageRadius()
        {
            float sum = 0;
            float avarage = 0;

            foreach (Spheres item in Stereology.listEsferas)
            {
                sum = sum + item.getRadius();
            }

            avarage = sum / Stereology.listEsferas.Count;

            return avarage;
        }

        public double calculateTotalVolumn()
        {
            double volume = 0;

            foreach (Spheres esfera in Stereology.listEsferas)
            {
                volume = volume + ((4 / 3) * Math.PI * (Math.Pow(esfera.getRadius(), 3)));
            }
            return volume;
        }

        public double calculateTotalMatrix()
        {
            double volumnMatrix = 0;
            double volumeTotal = 1.0;
            double volumeEsferas;

            volumeEsferas = calculateTotalVolumn();

            volumnMatrix = volumeTotal - volumeEsferas;

            return volumnMatrix;
        }

        //Métodos para alterações de cores
        //..........................................................................................................................
        public void onScrollRedPlane(object sender, EventArgs e)
        {
            textBoxPlaneR.Text = scrollColorPlaneR.Value.ToString();

            Atributes.colorPlaneRed = (float)scrollColorPlaneR.Value;
        }

        public void onScrollGreenPlane(object sender, EventArgs e)
        {
            textBoxPlaneG.Text = scrollColorPlaneG.Value.ToString();

            Atributes.colorPlaneGreen = (float)scrollColorPlaneG.Value;
        }

        public void onScrollBluePlane(object sender, EventArgs e)
        {
            textBoxPlaneB.Text = scrollColorPlaneB.Value.ToString();

            Atributes.colorPlaneBlue = (float)scrollColorPlaneB.Value;
        }

        //Métodos criados para a exibição de informações do sistema como listagens de esferas, circunferências e planos
        //..........................................................................................................................
        
        //Método para construir a listagem completa com todos os cálculos das quantidades de secções por diâmetro de esfera
        public void createFullListFactorsCalcSaltykov(System.Windows.Forms.Form formInput)
        {
            System.Windows.Forms.ListView listViewQuantSection = new System.Windows.Forms.ListView();
            listViewQuantSection.SetBounds(330, 270, 430, 170);

            listViewQuantSection.View = System.Windows.Forms.View.Details;
            listViewQuantSection.LabelEdit = false;
            listViewQuantSection.AllowColumnReorder = false;
            listViewQuantSection.CheckBoxes = false;
            listViewQuantSection.FullRowSelect = true;
            listViewQuantSection.GridLines = true;

            listViewQuantSection.Columns.Add("Índice", 150, HorizontalAlignment.Left);
            listViewQuantSection.Columns.Add("Indexador Secção", 150, HorizontalAlignment.Left);
            listViewQuantSection.Columns.Add("Quantidade de Secções", 150, HorizontalAlignment.Left);

            try
            {
                int fixedRowx = 0;

                List<ListViewItem> listItemQuant = new List<ListViewItem>();

                String[] index = new String[2000000];
                String[] indexColumn = new String[200000];
                String[] indexSection = new String[2000000];
                String[] quantSection = new String[2000000];

                for(int i = 0; i < SaltykovVolumn.listSaltykovFactors.Count; i++)
                {
                    
                    for(int j = 0; j < SaltykovVolumn.listSaltykovFactors[i].getListFactorResult().Count; j++)
                    {
                        listItemQuant.Add(new ListViewItem());
                        index[j] = "Índice: " + ((i+1)).ToString();
                        indexColumn[j] = (j+1).ToString();
                        quantSection[j] = SaltykovVolumn.listSaltykovFactors[i].getListFactorResult()[j].ToString();
                    }

                    for (int k = 0; k < SaltykovVolumn.listSaltykovFactors[i].getListFactorResult().Count; k++)
                    {
                        String[] circleArray = new String[3];
                        circleArray[0] = index[k].ToString();
                        circleArray[1] = indexColumn[k].ToString();
                        circleArray[2] = quantSection[k].ToString();
                        //circleArray[2] = quantSection[i].ToString();

                        listItemQuant[k] = new ListViewItem(circleArray);
                        listViewQuantSection.Items.Add(listItemQuant[k]);
                    }
                }

                /*for (int i = 0; i < SaltykovVolumn.listSizeK0.Count; i++)
                {
                    listItemQuant.Add(new ListViewItem());
                    index[fixedRowx] = "Índice: " + ((i)).ToString();
                    indexSection[fixedRowx] = ("N(" + (i+1) + "," + "1" + ")").ToString();
                    quantSection[fixedRowx] = SaltykovVolumn.listSizeK0[i].ToString();

                    fixedRowx = fixedRowx + 1;
                }

                for(int i = 0; i < SaltykovVolumn.listSizeK1.Count - 1; i++)
                {
                    listItemQuant.Add(new ListViewItem());
                    index[fixedRowx] = "índice: " + (i);
                    indexSection[fixedRowx] = ("N(" + (i+2) + "," + "2" + ")").ToString();
                    quantSection[fixedRowx] = SaltykovVolumn.listSizeK1[i].ToString();

                    fixedRowx = fixedRowx + 1;
                }
            
                for (int i = 0; i < SaltykovVolumn.listSizeK2.Count - 2; i++)
                {
                    listItemQuant.Add(new ListViewItem());
                    index[fixedRowx] = "índice: " + (i);
                    indexSection[fixedRowx] = ("N(" + (i+3) + "," + "3" + ")").ToString();
                    quantSection[fixedRowx] = SaltykovVolumn.listSizeK2[i].ToString();

                    fixedRowx = fixedRowx + 1;
                }

                for (int i = 0; i < SaltykovVolumn.listSizeK3.Count - 3; i++)
                {
                    listItemQuant.Add(new ListViewItem());
                    index[fixedRowx] = "índice: " + (i);
                    indexSection[fixedRowx] = ("N(" + (i+4) + "," + "4" + ")").ToString();
                    quantSection[fixedRowx] = SaltykovVolumn.listSizeK3[i].ToString();

                    fixedRowx = fixedRowx + 1;
                }

                for (int i = 0; i < SaltykovVolumn.listSizeK4.Count - 4; i++)
                {
                    listItemQuant.Add(new ListViewItem());
                    index[fixedRowx] = "índice: " + (i);
                    indexSection[fixedRowx] = ("N(" + (i+5) + "," + "5" + ")").ToString();
                    quantSection[fixedRowx] = SaltykovVolumn.listSizeK4[i].ToString();

                    fixedRowx = fixedRowx + 1;
                }

                for (int i = 0; i < SaltykovVolumn.listSizeK5.Count - 5; i++)
                {
                    listItemQuant.Add(new ListViewItem());
                    index[fixedRowx] = "índice: " + (i);
                    indexSection[fixedRowx] = ("N(" + (i+6) + "," + "6" + ")").ToString();
                    quantSection[fixedRowx] = SaltykovVolumn.listSizeK5[i].ToString();

                    fixedRowx = fixedRowx + 1;
                }

                for (int i = 0; i < SaltykovVolumn.listSizeK6.Count - 6; i++)
                {
                    listItemQuant.Add(new ListViewItem());
                    index[fixedRowx] = "índice: " + (i);
                    indexSection[fixedRowx] = ("N(" + (i+7) + "," + "7" + ")").ToString();
                    quantSection[fixedRowx] = SaltykovVolumn.listSizeK6[i].ToString();

                    fixedRowx = fixedRowx + 1;
                }*/

                for (int i = 0; i < SaltykovVolumn.listSizeK7.Count - 7; i++)
                {
                    listItemQuant.Add(new ListViewItem());
                    index[fixedRowx] = "índice: " + (i);
                    indexSection[fixedRowx] = ("N(" + (i+8) + "," + "8" + ")").ToString();
                    quantSection[fixedRowx] = SaltykovVolumn.listSizeK7[i].ToString();

                    fixedRowx = fixedRowx + 1;
                }

                int totalSize = SaltykovVolumn.listSizeK0.Count + SaltykovVolumn.listSizeK1.Count + SaltykovVolumn.listSizeK2.Count +
                                SaltykovVolumn.listSizeK3.Count + SaltykovVolumn.listSizeK4.Count + SaltykovVolumn.listSizeK4.Count +
                                SaltykovVolumn.listSizeK5.Count + SaltykovVolumn.listSizeK6.Count + SaltykovVolumn.listSizeK7.Count;
    
                for (int i = 0; i < totalSize; i++)
                {
                    String[] circleArray = new String[3];
                    circleArray[0] = index[i].ToString();
                    circleArray[1] = indexSection[i].ToString();
                    circleArray[2] = quantSection[i].ToString();

                    listItemQuant[i] = new ListViewItem(circleArray);
                    listViewQuantSection.Items.Add(listItemQuant[i]);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception.ToString());
            }

            formInput.Controls.Add(listViewQuantSection);

        }

        public void onClickShowListPlanes(object sender, EventArgs e)
        {
            formListPlane = new System.Windows.Forms.Form();
            formListPlane.SetBounds(400, 100, 700, 560);
            formListPlane.Text = "Equação dos Planos";
            formListPlane.MaximizeBox = false;

            listViewPlanes = new System.Windows.Forms.ListView();
            listViewPlanes.SetBounds(10, 10, 660, 440);

            listViewPlanes.View = System.Windows.Forms.View.Details;
            listViewPlanes.LabelEdit = false;
            listViewPlanes.AllowColumnReorder = false;
            listViewPlanes.CheckBoxes = false;
            listViewPlanes.FullRowSelect = true;
            listViewPlanes.GridLines = true;
            //listViewPlanes.Sorting = false;

            listViewPlanes.Columns.Add("Plano", 150, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("ax", 150, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("by", 150, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("cz", 150, HorizontalAlignment.Left);
            listViewPlanes.Columns.Add("d", 150, HorizontalAlignment.Left);

            listViewPlanes.SelectedIndexChanged += new System.EventHandler(this.onSelectRowPlane);

            try
            {
                //Verificação se a lista com os planos não está vazia
                if (planeUtils.getListPlaneEquation().Count != 0)
                {
                    listItem = new List<ListViewItem>();

                    double[] coeficienteX = new double[200000];
                    double[] coeficienteY = new double[200000];
                    double[] coeficienteZ = new double[200000];
                    double[] independente = new double[200000];

                    for (int i = 0; i < planeUtils.getListPlaneEquation().Count; i++)
                    {
                        listItem.Add(new ListViewItem());
                    }

                    for (int i = 0; i < planeUtils.getListPlaneEquation().Count; i++)
                    {
                        coeficienteX[i] = planeUtils.getListPlaneEquation()[i].getCoeficienteX();
                        coeficienteY[i] = planeUtils.getListPlaneEquation()[i].getCoeficienteY();
                        coeficienteZ[i] = planeUtils.getListPlaneEquation()[i].getCoeficienteZ();
                        independente[i] = planeUtils.getListPlaneEquation()[i].getTermoIndenpendente();
                    }


                    for (int i = 0; i < planeUtils.getListPlaneEquation().Count; i++)
                    {
                        String[] planesArray = new String[5];
                        planesArray[0] = "Plano: " + (i + 1).ToString();
                        planesArray[1] = coeficienteX[i].ToString();
                        planesArray[2] = coeficienteY[i].ToString();
                        planesArray[3] = coeficienteZ[i].ToString();
                        planesArray[4] = independente[i].ToString();

                        listItem[i] = new ListViewItem(planesArray);
                        listViewPlanes.Items.Add(listItem[i]);
                    }

                    formListPlane.Controls.Add(listViewPlanes);

                    System.Windows.Forms.Button buttonExport = new System.Windows.Forms.Button();
                    buttonExport.SetBounds(10, 470, 100, 30);
                    buttonExport.Text = "Exportar arquivo";
                    buttonExport.Click += new System.EventHandler(this.exportListPlanes);
                    formListPlane.Controls.Add(buttonExport);

                    formListPlane.ShowDialog();
                }
                else
                {
                    Console.WriteLine("A lista de planos está vazia");
                }
            }
            catch (Exception excpetion)
            {
                Console.WriteLine("Lista sem elementos: " + excpetion.ToString());
            }
        }

        public void onClickListSpheres(object sender, EventArgs e)
        {
            formListSpheres = new System.Windows.Forms.Form();
            formListSpheres.Text = "Lista de Spheres";
            formListSpheres.SetBounds(100, 100, 700, 600);
            formListSpheres.MaximizeBox = false;

            listViewSpheres = new System.Windows.Forms.ListView();
            listViewSpheres.SetBounds(10, 10, 660, 440);

            listViewSpheres.View = System.Windows.Forms.View.Details;
            listViewSpheres.LabelEdit = false;
            listViewSpheres.AllowColumnReorder = false;
            listViewSpheres.CheckBoxes = false;
            listViewSpheres.FullRowSelect = true;
            listViewSpheres.GridLines = true;

            listViewSpheres.Columns.Add("Esfera", 150, HorizontalAlignment.Left);
            listViewSpheres.Columns.Add("C(x)", 150, HorizontalAlignment.Left);
            listViewSpheres.Columns.Add("C(y)", 150, HorizontalAlignment.Left);
            listViewSpheres.Columns.Add("C(z)", 150, HorizontalAlignment.Left);
            listViewSpheres.Columns.Add("Raio", 150, HorizontalAlignment.Left);

            System.Windows.Forms.Button buttonExportSpheres = new System.Windows.Forms.Button();
            buttonExportSpheres.SetBounds(10, 460, 100, 30);
            buttonExportSpheres.Text = "Exportar Lista";
            buttonExportSpheres.Click += new System.EventHandler(this.exportListSpheres);
            formListSpheres.Controls.Add(buttonExportSpheres);

            try
            {

                if (Stereology.listEsferas.Count > 0)
                {
                    double[] centroEsferaX = new double[10000];
                    double[] centroEsferaY = new double[10000];
                    double[] centroEsferaZ = new double[10000];
                    double[] raioEsfera = new double[10000];

                    List<ListViewItem> listItem = new List<ListViewItem>();

                    for (int i = 0; i < Stereology.listEsferas.Count; i++)
                    {
                        listItem.Add(new ListViewItem());
                    }

                    for (int i = 0; i < Stereology.listEsferas.Count; i++)
                    {
                        centroEsferaX[i] = Stereology.listEsferas[i].getPositionX();
                        centroEsferaY[i] = Stereology.listEsferas[i].getPositionY();
                        centroEsferaZ[i] = Stereology.listEsferas[i].getPositionZ();
                        raioEsfera[i] = Stereology.listEsferas[i].getRadius();
                    }

                    for (int i = 0; i < Stereology.listEsferas.Count; i++)
                    {
                        String[] spheresList = new String[5];
                        spheresList[0] = "Esfera: " + i.ToString();
                        spheresList[1] = centroEsferaX[i].ToString();
                        spheresList[2] = centroEsferaY[i].ToString();
                        spheresList[3] = centroEsferaZ[i].ToString();
                        spheresList[4] = raioEsfera[i].ToString();

                        listItem[i] = new ListViewItem(spheresList);

                        listViewSpheres.Items.Add(listItem[i]);
                    }
                }

            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception.ToString());
            }

            formListSpheres.Controls.Add(listViewSpheres);

            formListSpheres.ShowDialog();
        }

        public void showInformations()
        {

            for (int i = 0; i < Stereology.listLine.Count; i++)
            {
                double vectorX = Stereology.listLine[i].getVector().getX();
                double vectorY = Stereology.listLine[i].getVector().getY();
                double vectorZ = Stereology.listLine[i].getVector().getZ();
            }

            for (int i = 0; i < Stereology.listLine.Count; i++)
            {
                double pointX = Stereology.listLine[i].getPointA().getX();
                double pointY = Stereology.listLine[i].getPointA().getY();
                double pointZ = Stereology.listLine[i].getPointA().getZ();

            }
        }

        //Timer para processos longos que necessitam de threads
        //..........................................................................................................................
        private void initTimer()
        {
            progressBar.Maximum = (int)Atributes.quantPlanes;
            progressBar.Minimum = 0;
            timer.Interval = 250;
            timer.Tick += new EventHandler(runningTimer);
            timer.Start();
        }

        private void runningTimer(object sender, EventArgs e)
        {

            if (progressBar.Value == progressBar.Maximum)
            {
                timer.Stop();
            }

        }


        //Aplicação dos métodos para exportação das planilhas para o Excel
        //..........................................................................................................................
        public void exportListIntervalFrequence(object sender, EventArgs e)
        {
            ResourceExcel resource = new ResourceExcel(listViewIntervalFrequence);
            Thread threadExport = new Thread(new ParameterizedThreadStart(resource.toExcel));
            threadExport.Start();
        }

        public void exportListPlanes(object sender, EventArgs e)
        {
            ResourceExcel resource = new ResourceExcel(listViewPlanes);
            Thread threadExport = new Thread(new ParameterizedThreadStart(resource.toExcel));
            threadExport.Start();
        }

        public void exportListSpheres(object sender, EventArgs e)
        {
            ResourceExcel resource = new ResourceExcel(listViewSpheres);
            //resource.toExcel();
        } 

        /*Atributos da classe MenuPanel utilizado nos métodos*/
        //..........................................................................................................................
        
        public static int planOption;
        public static PlaneUtils planeUtils;
        private List<ListViewItem> listItem;
        public List<Interval> saltyIntervalRadius;
        public List<Interval> saltyIntervalArea;
        private System.Windows.Forms.ListView listViewPlanes;
        private System.Windows.Forms.ListView listViewSpheres;
        private System.Windows.Forms.ListView listViewIntervalFrequence;

        public static List<PlanEquation> staticListPlaneEquation;

        private HistoSaltyArea histArea;
        private System.Windows.Forms.Form formListPlane;
        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.Menu menu;

        private System.Windows.Forms.Form formInfo;
        private System.Windows.Forms.Form formRadius;
        private System.Windows.Forms.Form formCreatePlane;
        private System.Windows.Forms.Form formListSpheres;
            
        private System.Windows.Forms.TextBox textBoxRadius;
        private System.Windows.Forms.HScrollBar scrollRadius;

        public static System.Windows.Forms.HScrollBar scrollColorPlaneR;
        public static System.Windows.Forms.HScrollBar scrollColorPlaneG;
        public static System.Windows.Forms.HScrollBar scrollColorPlaneB;
        public static System.Windows.Forms.HScrollBar scrollQuantPlanes;

        private System.Windows.Forms.TextBox textBoxPlaneR;
        private System.Windows.Forms.TextBox textBoxPlaneG;
        private System.Windows.Forms.TextBox textBoxPlaneB;
        private System.Windows.Forms.TextBox textBoxQuantPlanes;

        public static List<PointLocation> listIntersect;

        public SaltykovVolumn saltVolumn;

        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private ProgressBar progressBar;
    }
}
