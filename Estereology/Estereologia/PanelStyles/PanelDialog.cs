﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.PanelStyles
{
    public class PanelDialog
    {

        public PanelDialog()
        {

        }

        public PanelDialog(String title, String message)
        {
            this.title = title;
            this.message = message;

            createPanelDialog();
        }

        public void createPanelDialog()
        {
            dialog = new System.Windows.Forms.Form();
            dialog.SetBounds(400, 100, 400, 200);
            dialog.Text = this.title;

            labelMessage = new System.Windows.Forms.Label();
            labelMessage.SetBounds(10, 10, 500, 50);
            labelMessage.Text = this.message;
            labelMessage.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            dialog.Controls.Add(labelMessage);

            dialog.ShowDialog();
        }

        private String title;
        private String message;
        private System.Windows.Forms.Label labelMessage;
        public System.Windows.Forms.Form dialog;
    }
}
