﻿using Stereology;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estereology.PanelStyles
{
    public class MenuOptions
    {
        public MenuOptions(System.Windows.Forms.Form frame)
        {
            Console.WriteLine("Entrou no sistema de criação do menu de opções");

            this.panelOptions = new System.Windows.Forms.Panel();
            this.panelOptions.SetBounds(20, 10, 960, 50);
            this.panelOptions.BackColor = System.Drawing.Color.FromArgb(150, 150, 150);

            this.buttonLight = new System.Windows.Forms.Button();
            this.buttonLight.SetBounds(5, 5, 40, 40);
            this.buttonLight.BackColor = Color.FromArgb(0, 0, 0, 0);
            this.buttonLight.Image = Image.FromFile("C:\\Users\\igor\\Documents\\Visual Studio 2015\\Projects\\Estereology\\SistemaSolar\\Resources\\ligth.png");
            this.buttonLight.ImageAlign = ContentAlignment.MiddleRight;
            this.buttonLight.TextAlign = ContentAlignment.MiddleLeft;
            this.buttonLight.Click += new System.EventHandler(this.onClickLight);
            this.panelOptions.Controls.Add(this.buttonLight);

            this.buttonPosLight = new System.Windows.Forms.Button();
            this.buttonPosLight.SetBounds(50, 5, 40, 40);
            this.buttonPosLight.BackColor = Color.FromArgb(0, 0, 0, 0);
            this.buttonPosLight.Image = Image.FromFile("C:\\Users\\igor\\Documents\\Visual Studio 2015\\Projects\\Estereology\\SistemaSolar\\Resources\\light_pos.png");
            this.buttonPosLight.ImageAlign = ContentAlignment.MiddleRight;
            this.buttonPosLight.TextAlign = ContentAlignment.MiddleLeft;
            this.buttonPosLight.Click += new System.EventHandler(this.onClickLightPos);
            this.panelOptions.Controls.Add(this.buttonPosLight);

            frame.Controls.Add(panelOptions);
        }

        public void onClickLightPos(object sender, EventArgs args)
        {
            System.Windows.Forms.Form formLightPos = new System.Windows.Forms.Form();
            formLightPos.SetBounds(100, 40, 300, 500);
            formLightPos.Text = "Ambient Light";

            this.labelAmbientLightPosX = new System.Windows.Forms.Label();
            this.labelAmbientLightPosX.SetBounds(10, 10, 300, 20);
            this.labelAmbientLightPosX.Text = "Material Ambient Position X: " + (100 * Atributes.ambientLigthPosX).ToString();
            this.labelAmbientLightPosX.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLightPos.Controls.Add(this.labelAmbientLightPosX);

            this.scrollLightPositionX = new System.Windows.Forms.HScrollBar();
            this.scrollLightPositionX.SetBounds(10, 30, 250, 15);
            this.scrollLightPositionX.Minimum = 0;
            this.scrollLightPositionX.Maximum = 109;
            this.scrollLightPositionX.Value = (int)(100 * Atributes.ambientLigthPosX);
            this.scrollLightPositionX.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightPositionX);
            formLightPos.Controls.Add(this.scrollLightPositionX);

            this.labelAmbientLightPosY = new System.Windows.Forms.Label();
            this.labelAmbientLightPosY.SetBounds(10, 60, 300, 20);
            this.labelAmbientLightPosY.Text = "Material Ambient Position Y: " + (100 * Atributes.ambientLightPosY).ToString();
            this.labelAmbientLightPosY.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLightPos.Controls.Add(this.labelAmbientLightPosY);

            this.scrollLightPositionY = new System.Windows.Forms.HScrollBar();
            this.scrollLightPositionY.SetBounds(10, 80, 250, 15);
            this.scrollLightPositionY.Minimum = 0;
            this.scrollLightPositionY.Maximum = 109;
            this.scrollLightPositionY.Value = (int)(100 * Atributes.ambientLightPosY);
            this.scrollLightPositionY.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightPositionY);
            formLightPos.Controls.Add(this.scrollLightPositionY);

            this.labelAmbientLightPosZ = new System.Windows.Forms.Label();
            this.labelAmbientLightPosZ.SetBounds(10, 110, 300, 20);
            this.labelAmbientLightPosZ.Text = "Material Ambient Position Z: " + (100 * Atributes.ambientLightPosZ).ToString();
            this.labelAmbientLightPosZ.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLightPos.Controls.Add(this.labelAmbientLightPosZ);

            this.scrollLightPositionZ = new System.Windows.Forms.HScrollBar();
            this.scrollLightPositionZ.SetBounds(10, 130, 250, 15);
            this.scrollLightPositionZ.Minimum = 0;
            this.scrollLightPositionZ.Maximum = 109;
            this.scrollLightPositionZ.Value = (int)(100 * Atributes.ambientLightPosZ);
            this.scrollLightPositionZ.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightPositionZ);
            formLightPos.Controls.Add(this.scrollLightPositionZ);

            this.labelAmbientLightX = new System.Windows.Forms.Label();
            this.labelAmbientLightX.SetBounds(10, 160, 300, 20);
            this.labelAmbientLightX.Text = "Ambient Light (Red): " + (100 * Atributes.ambientLightX).ToString();
            this.labelAmbientLightX.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLightPos.Controls.Add(this.labelAmbientLightX);

            this.scrollLightAmbientX = new System.Windows.Forms.HScrollBar();
            this.scrollLightAmbientX.SetBounds(10, 180, 250, 15);
            this.scrollLightAmbientX.Minimum = 0;
            this.scrollLightAmbientX.Maximum = 109;
            this.scrollLightAmbientX.Value = (int)(100 * Atributes.ambientLightX);
            this.scrollLightAmbientX.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightAmbientX);
            formLightPos.Controls.Add(this.scrollLightAmbientX);

            this.labelAmbientLightY = new System.Windows.Forms.Label();
            this.labelAmbientLightY.SetBounds(10, 210, 300, 20);
            this.labelAmbientLightY.Text = "Ambient Light (Green): " + (100 * Atributes.ambientLightY).ToString();
            this.labelAmbientLightY.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLightPos.Controls.Add(this.labelAmbientLightY);

            this.scrollLightAmbientY = new System.Windows.Forms.HScrollBar();
            this.scrollLightAmbientY.SetBounds(10, 230, 250, 15);
            this.scrollLightAmbientY.Minimum = 0;
            this.scrollLightAmbientY.Maximum = 109;
            this.scrollLightAmbientY.Value = (int)(100 * Atributes.ambientLightX);
            this.scrollLightAmbientY.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightAmbientY);
            formLightPos.Controls.Add(this.scrollLightAmbientY);

            this.labelAmbientLightZ = new System.Windows.Forms.Label();
            this.labelAmbientLightZ.SetBounds(10, 260, 300, 20);
            this.labelAmbientLightZ.Text = "Ambient Light (Blue): " + (100 * Atributes.ambientLightY).ToString();
            this.labelAmbientLightZ.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLightPos.Controls.Add(this.labelAmbientLightZ);

            this.scrollLightAmbientZ = new System.Windows.Forms.HScrollBar();
            this.scrollLightAmbientZ.SetBounds(10, 280, 250, 15);
            this.scrollLightAmbientZ.Minimum = 0;
            this.scrollLightAmbientZ.Maximum = 109;
            this.scrollLightAmbientZ.Value = (int)(100 * Atributes.ambientLightX);
            this.scrollLightAmbientZ.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightAmbientZ);
            formLightPos.Controls.Add(this.scrollLightAmbientZ);



            formLightPos.Show();
        }

        public void onClickLight(object sender, EventArgs args)
        {
            System.Windows.Forms.Form formLigth = new System.Windows.Forms.Form();
            formLigth.SetBounds(100, 20, 300, 700);
            formLigth.Text = "Lighting Adjustment";

            //..................................Material Ambient (R,G,B,Alpha)..................................................

            this.labelMaterialAmbientRed = new System.Windows.Forms.Label();
            this.labelMaterialAmbientRed.SetBounds(10,10, 300, 20);
            this.labelMaterialAmbientRed.Text = "Material Ambient (Red): " + (100*Atributes.materialAmbientR).ToString();
            this.labelMaterialAmbientRed.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialAmbientRed);

            this.scrollAmbientRed = new System.Windows.Forms.HScrollBar();
            this.scrollAmbientRed.SetBounds(10, 30, 250, 15);
            this.scrollAmbientRed.Minimum = 0;
            this.scrollAmbientRed.Maximum = 109;
            this.scrollAmbientRed.Value = (int)(100 * Atributes.materialAmbientR);
            this.scrollAmbientRed.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialAmbientR);
            formLigth.Controls.Add(scrollAmbientRed);

            this.labelMaterialAmbientGreen = new System.Windows.Forms.Label();
            this.labelMaterialAmbientGreen.SetBounds(10, 60, 300, 20);
            this.labelMaterialAmbientGreen.Text = "Material Ambient (Green): " + (100 * Atributes.materialAmbientG).ToString();
            this.labelMaterialAmbientGreen.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialAmbientGreen);

            this.scrollAmbientGreen = new System.Windows.Forms.HScrollBar();
            this.scrollAmbientGreen.SetBounds(10, 80, 250, 15);
            this.scrollAmbientGreen.Minimum = 0;
            this.scrollAmbientGreen.Maximum = 109;
            this.scrollAmbientGreen.Value = (int)(100 * Atributes.materialAmbientG);
            this.scrollAmbientGreen.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialAmbientG);
            formLigth.Controls.Add(scrollAmbientGreen);

            this.labelMaterialAmbientBlue = new System.Windows.Forms.Label();
            this.labelMaterialAmbientBlue.SetBounds(10, 110, 300, 20);
            this.labelMaterialAmbientBlue.Text = "Material Ambient (Blue): " + (100 * Atributes.materialAmbientB).ToString();
            this.labelMaterialAmbientBlue.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialAmbientBlue);

            this.scrollAmbientBlue = new System.Windows.Forms.HScrollBar();
            this.scrollAmbientBlue.SetBounds(10, 130, 250, 15);
            this.scrollAmbientBlue.Minimum = 0;
            this.scrollAmbientBlue.Maximum = 109;
            this.scrollAmbientBlue.Value = (int)(100 * Atributes.materialAmbientB);
            this.scrollAmbientBlue.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialAmbientB);
            formLigth.Controls.Add(scrollAmbientBlue);

            this.labelMaterialAmbientAlpha = new System.Windows.Forms.Label();
            this.labelMaterialAmbientAlpha.SetBounds(10, 160, 300, 20);
            this.labelMaterialAmbientAlpha.Text = "Material Ambient (Alpha): " + (100 * Atributes.materialAmbientAlpha).ToString();
            this.labelMaterialAmbientAlpha.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialAmbientAlpha);

            this.scrollAmbientAlpha = new System.Windows.Forms.HScrollBar();
            this.scrollAmbientAlpha.SetBounds(10, 180, 250, 15);
            this.scrollAmbientAlpha.Minimum = 0;
            this.scrollAmbientAlpha.Maximum = 109;
            this.scrollAmbientAlpha.Value = (int)(100 * Atributes.materialAmbientAlpha);
            this.scrollAmbientAlpha.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightMaterialAmbientAlpha);
            formLigth.Controls.Add(scrollAmbientAlpha);

            //..................................Material Difuse (R,G,B,Alpha)..................................................

            this.labelMaterialDifuseRed = new System.Windows.Forms.Label();
            this.labelMaterialDifuseRed.SetBounds(10, 210, 300, 20);
            this.labelMaterialDifuseRed.Text = "Material Difuse (Red): " + (100 * Atributes.materialDifuseR).ToString();
            this.labelMaterialDifuseRed.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialDifuseRed);

            this.scrollDifuseRed = new System.Windows.Forms.HScrollBar();
            this.scrollDifuseRed.SetBounds(10, 230, 250, 15);
            this.scrollDifuseRed.Minimum = 0;
            this.scrollDifuseRed.Maximum = 109;
            this.scrollDifuseRed.Value = (int)(100 * Atributes.materialDifuseR);
            this.scrollDifuseRed.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialDifuseR);
            formLigth.Controls.Add(scrollDifuseRed);

            this.labelMaterialDifuseGreen = new System.Windows.Forms.Label();
            this.labelMaterialDifuseGreen.SetBounds(10, 260, 300, 20);
            this.labelMaterialDifuseGreen.Text = "Material Difuse (Green): " + (100 * Atributes.materialDifuseG).ToString();
            this.labelMaterialDifuseGreen.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialDifuseGreen);

            this.scrollDifuseGreen = new System.Windows.Forms.HScrollBar();
            this.scrollDifuseGreen.SetBounds(10, 280, 250, 15);
            this.scrollDifuseGreen.Minimum = 0;
            this.scrollDifuseGreen.Maximum = 109;
            this.scrollDifuseGreen.Value = (int)(100 * Atributes.materialDifuseG);
            this.scrollDifuseGreen.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialDifuseG);
            formLigth.Controls.Add(scrollDifuseGreen);

            this.labelMaterialDifuseBlue = new System.Windows.Forms.Label();
            this.labelMaterialDifuseBlue.SetBounds(10, 310, 300, 20);
            this.labelMaterialDifuseBlue.Text = "Material Difuse (Blue): " + (100 * Atributes.materialDifuseB).ToString();
            this.labelMaterialDifuseBlue.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialDifuseBlue);

            this.scrollDifuseBlue = new System.Windows.Forms.HScrollBar();
            this.scrollDifuseBlue.SetBounds(10, 330, 250, 15);
            this.scrollDifuseBlue.Minimum = 0;
            this.scrollDifuseBlue.Maximum = 109;
            this.scrollDifuseBlue.Value = (int)(100 * Atributes.materialDifuseB);
            this.scrollDifuseBlue.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialDifuseB);
            formLigth.Controls.Add(scrollDifuseBlue);

            this.labelMaterialDifuseAlpha = new System.Windows.Forms.Label();
            this.labelMaterialDifuseAlpha.SetBounds(10, 360, 300, 20);
            this.labelMaterialDifuseAlpha.Text = "Material Difuse (Alpha): " + (100 * Atributes.materialDifuseAlpha).ToString();
            this.labelMaterialDifuseAlpha.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialDifuseAlpha);

            this.scrollDifuseAlpha = new System.Windows.Forms.HScrollBar();
            this.scrollDifuseAlpha.SetBounds(10, 380, 250, 15);
            this.scrollDifuseAlpha.Minimum = 0;
            this.scrollDifuseAlpha.Maximum = 109;
            this.scrollDifuseAlpha.Value = (int)(100 * Atributes.materialDifuseAlpha);
            this.scrollDifuseAlpha.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightMaterialDifuseAlpha);
            formLigth.Controls.Add(scrollDifuseAlpha);

            //..................................Material Specular (R,G,B,Alpha)..................................................

            this.labelMaterialSpecularRed = new System.Windows.Forms.Label();
            this.labelMaterialSpecularRed.SetBounds(10, 410, 300, 20);
            this.labelMaterialSpecularRed.Text = "Material Specular (Red): " + (100 * Atributes.materialDifuseR).ToString();
            this.labelMaterialSpecularRed.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialSpecularRed);

            this.scrollMaterialSpecularRed = new System.Windows.Forms.HScrollBar();
            this.scrollMaterialSpecularRed.SetBounds(10, 430, 250, 15);
            this.scrollMaterialSpecularRed.Minimum = 0;
            this.scrollMaterialSpecularRed.Maximum = 109;
            this.scrollMaterialSpecularRed.Value = (int)(100 * Atributes.materialDifuseR);
            this.scrollMaterialSpecularRed.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialSpecularR);
            formLigth.Controls.Add(scrollMaterialSpecularRed);

            this.labelMaterialSpecularGreen = new System.Windows.Forms.Label();
            this.labelMaterialSpecularGreen.SetBounds(10, 460, 300, 20);
            this.labelMaterialSpecularGreen.Text = "Material Specular (Green): " + (100 * Atributes.materialDifuseG).ToString();
            this.labelMaterialSpecularGreen.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialSpecularGreen);

            this.scrollMaterialSpecularGreen = new System.Windows.Forms.HScrollBar();
            this.scrollMaterialSpecularGreen.SetBounds(10, 480, 250, 15);
            this.scrollMaterialSpecularGreen.Minimum = 0;
            this.scrollMaterialSpecularGreen.Maximum = 109;
            this.scrollMaterialSpecularGreen.Value = (int)(100 * Atributes.materialDifuseG);
            this.scrollMaterialSpecularGreen.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialSpecularG);
            formLigth.Controls.Add(scrollMaterialSpecularGreen);

            this.labelMaterialSpecularBlue = new System.Windows.Forms.Label();
            this.labelMaterialSpecularBlue.SetBounds(10, 510, 300, 20);
            this.labelMaterialSpecularBlue.Text = "Material Specular (Blue): " + (100 * Atributes.materialSpecularB).ToString();
            this.labelMaterialSpecularBlue.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialSpecularBlue);

            this.scrollMaterialSpecularBlue = new System.Windows.Forms.HScrollBar();
            this.scrollMaterialSpecularBlue.SetBounds(10, 530, 250, 15);
            this.scrollMaterialSpecularBlue.Minimum = 0;
            this.scrollMaterialSpecularBlue.Maximum = 109;
            this.scrollMaterialSpecularBlue.Value = (int)(100 * Atributes.materialSpecularB);
            this.scrollMaterialSpecularBlue.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLigthMaterialSpecularB);
            formLigth.Controls.Add(scrollMaterialSpecularBlue);

            this.labelMaterialSpecularAlpha = new System.Windows.Forms.Label();
            this.labelMaterialSpecularAlpha.SetBounds(10, 560, 300, 20);
            this.labelMaterialSpecularAlpha.Text = "Material Specular (Alpha): " + (100 * Atributes.materialSpecularAlpha).ToString();
            this.labelMaterialSpecularAlpha.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(labelMaterialSpecularAlpha);

            this.scrollMaterialSpecularAlpha = new System.Windows.Forms.HScrollBar();
            this.scrollMaterialSpecularAlpha.SetBounds(10, 580, 250, 15);
            this.scrollMaterialSpecularAlpha.Minimum = 0;
            this.scrollMaterialSpecularAlpha.Maximum = 109;
            this.scrollMaterialSpecularAlpha.Value = (int)(100 * Atributes.materialDifuseAlpha);
            this.scrollMaterialSpecularAlpha.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollLightMaterialSpecularAlpha);
            formLigth.Controls.Add(scrollMaterialSpecularAlpha);

            //..................................Material Shiness (R,G,B,Alpha)..................................................

            this.labelMaterialShiness = new System.Windows.Forms.Label();
            this.labelMaterialShiness.SetBounds(10, 610, 300, 20);
            this.labelMaterialShiness.Text = "Material Shiness: " + (100 * Atributes.materialShiness).ToString();
            this.labelMaterialShiness.Font = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold);
            formLigth.Controls.Add(this.labelMaterialShiness);

            this.scrollMaterialShiness = new System.Windows.Forms.HScrollBar();
            this.scrollMaterialShiness.SetBounds(10, 630, 250, 15);
            this.scrollMaterialShiness.Minimum = 0;
            this.scrollMaterialShiness.Maximum = 109;
            this.scrollMaterialShiness.Value = (int)(100 * Atributes.materialShiness);
            this.scrollMaterialShiness.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollMaterialShinness);
            formLigth.Controls.Add(this.scrollMaterialShiness);


            formLigth.Show();
        }
        
        public void onScrollLightPositionX(object sender, System.EventArgs e)
        {
            Atributes.ambientLigthPosX = (float)this.scrollLightPositionX.Value / 100;
            this.labelAmbientLightPosX.Text = "Material Ambient Position X: " + (100 * Atributes.ambientLigthPosX).ToString();
        }

        public void onScrollLightPositionY(object sender, System.EventArgs e)
        {
            Atributes.ambientLightPosY = (float)this.scrollLightPositionY.Value / 100;
            this.labelAmbientLightPosY.Text = "Material Ambient Position Y: " + (100 * Atributes.ambientLightPosY).ToString();
        }

        public void onScrollLightPositionZ(object sender, System.EventArgs e)
        {
            Atributes.ambientLightPosZ = (float)this.scrollLightPositionZ.Value / 100;
            this.labelAmbientLightPosZ.Text = "Material Ambient Position Z: " + (100 * Atributes.ambientLightPosZ).ToString();
        }

        //..........................................................................................................

        public void onScrollLightAmbientX(object sender, System.EventArgs e)
        {
            Atributes.ambientLightX = (float)this.scrollLightAmbientX.Value / 100;
            this.labelAmbientLightX.Text = "Ambient Light (Red): " + (100 * Atributes.ambientLightX).ToString();
        }

        public void onScrollLightAmbientY(object sender, System.EventArgs e)
        {
            Atributes.ambientLightY = (float)this.scrollLightAmbientY.Value / 100;
            this.labelAmbientLightY.Text = "Ambient Light (Green): " + (100 * Atributes.ambientLightY).ToString();
        }

        public void onScrollLightAmbientZ(object sender, System.EventArgs e)
        {
            Atributes.ambientLightZ = (float)this.scrollLightAmbientZ.Value / 100;
            this.labelAmbientLightZ.Text = "Ambient Light (Blue): " + (100 * Atributes.ambientLightZ).ToString();
        }

        //...........................................................................................................

        public void onScrollLigthMaterialAmbientR(object sender, System.EventArgs e)
        {
            Atributes.materialAmbientR = (float)scrollAmbientRed.Value / 100;
            this.labelMaterialAmbientRed.Text = "Material Ambient (Red): " + (100 * Atributes.materialAmbientR).ToString();
        }

        public void onScrollLigthMaterialAmbientG(object sender, System.EventArgs e)
        {
            Atributes.materialAmbientG = (float)scrollAmbientGreen.Value / 100;
            this.labelMaterialAmbientGreen.Text = "Material Ambient (Green): " + (100 * Atributes.materialAmbientG).ToString();
        }

        public void onScrollLigthMaterialAmbientB(object sender, System.EventArgs e)
        {
            Atributes.materialAmbientB = (float)scrollAmbientBlue.Value / 100;
            this.labelMaterialAmbientBlue.Text = "Material Ambient (Blue): " + (100 * Atributes.materialAmbientB).ToString();
        }

        public void onScrollLightMaterialAmbientAlpha(object sender, System.EventArgs e)
        {
            Atributes.materialAmbientAlpha = (float) scrollAmbientAlpha.Value / 100;
            this.labelMaterialAmbientAlpha.Text = "Material Ambient (Alpha): " + (100 * Atributes.materialAmbientAlpha).ToString();
        }

        //.........................................DIFUSÃO..............................................

        public void onScrollLigthMaterialDifuseR(object sender, System.EventArgs e)
        {
            Atributes.materialDifuseR = (float)scrollDifuseRed.Value / 100;
            this.labelMaterialDifuseRed.Text = "Material Difuse (Red): " + (100 * Atributes.materialDifuseR).ToString();
        }

        public void onScrollLigthMaterialDifuseG(object sender, System.EventArgs e)
        {
            Atributes.materialDifuseG = (float)scrollDifuseGreen.Value / 100;
            this.labelMaterialDifuseGreen.Text = "Material Difuse (Green): " + (100 * Atributes.materialDifuseG).ToString();
        }

        public void onScrollLigthMaterialDifuseB(object sender, System.EventArgs e)
        {
            Atributes.materialDifuseB= (float)scrollDifuseBlue.Value / 100;
            this.labelMaterialDifuseBlue.Text = "Material Difuse (Blue): " + (100 * Atributes.materialDifuseB).ToString();
        }

        public void onScrollLightMaterialDifuseAlpha(object sender, System.EventArgs e)
        {
            Atributes.materialDifuseAlpha = (float)scrollDifuseAlpha.Value / 100;
            this.labelMaterialDifuseAlpha.Text = "Material Difuse (Alpha): " + (100 * Atributes.materialDifuseAlpha).ToString();
        }

        //.........................................Especular..............................................

        public void onScrollLigthMaterialSpecularR(object sender, System.EventArgs e)
        {
            Atributes.materialSpecularR = (float)scrollMaterialSpecularRed.Value / 100;
            this.labelMaterialSpecularRed.Text = "Material Specular (Red): " + (100 * Atributes.materialSpecularR).ToString();
        }

        public void onScrollLigthMaterialSpecularG(object sender, System.EventArgs e)
        {
            Atributes.materialSpecularG = (float)scrollMaterialSpecularGreen.Value / 100;
            this.labelMaterialSpecularGreen.Text = "Material Specular (Green): " + (100 * Atributes.materialSpecularG).ToString();
        }

        public void onScrollLigthMaterialSpecularB(object sender, System.EventArgs e)
        {
            Atributes.materialSpecularB = (float)scrollMaterialSpecularBlue.Value / 100;
            this.labelMaterialSpecularBlue.Text = "Material Specular (Blue): " + (100 * Atributes.materialSpecularB).ToString();
        }

        public void onScrollLightMaterialSpecularAlpha(object sender, System.EventArgs e)
        {
            Atributes.materialSpecularAlpha = (float)scrollMaterialSpecularAlpha.Value / 100;
            this.labelMaterialSpecularAlpha.Text = "Material Specular (Alpha): " + (100 * Atributes.materialSpecularAlpha).ToString();
        }

        //.........................................Shiness..............................................

        public void onScrollMaterialShinness(object sender, System.EventArgs e)
        {
            Atributes.materialShiness = (float)scrollMaterialShiness.Value / 100;
            this.labelMaterialShiness.Text = "Material Shiness: " + (100 * Atributes.materialShiness).ToString();
        }

        private System.Windows.Forms.Label labelMaterialAmbientRed;
        private System.Windows.Forms.Label labelMaterialAmbientGreen;
        private System.Windows.Forms.Label labelMaterialAmbientBlue;
        private System.Windows.Forms.Label labelMaterialAmbientAlpha;

        private System.Windows.Forms.HScrollBar scrollAmbientRed;
        private System.Windows.Forms.HScrollBar scrollAmbientGreen;
        private System.Windows.Forms.HScrollBar scrollAmbientBlue;
        private System.Windows.Forms.HScrollBar scrollAmbientAlpha;

        private System.Windows.Forms.Label labelMaterialDifuseRed;
        private System.Windows.Forms.Label labelMaterialDifuseGreen;
        private System.Windows.Forms.Label labelMaterialDifuseBlue;
        private System.Windows.Forms.Label labelMaterialDifuseAlpha;

        private System.Windows.Forms.HScrollBar scrollDifuseRed;
        private System.Windows.Forms.HScrollBar scrollDifuseGreen;
        private System.Windows.Forms.HScrollBar scrollDifuseBlue;
        private System.Windows.Forms.HScrollBar scrollDifuseAlpha;


        private System.Windows.Forms.Label labelMaterialSpecularRed;
        private System.Windows.Forms.Label labelMaterialSpecularGreen;
        private System.Windows.Forms.Label labelMaterialSpecularBlue;
        private System.Windows.Forms.Label labelMaterialSpecularAlpha;

        private System.Windows.Forms.HScrollBar scrollMaterialSpecularRed;
        private System.Windows.Forms.HScrollBar scrollMaterialSpecularGreen;
        private System.Windows.Forms.HScrollBar scrollMaterialSpecularBlue;
        private System.Windows.Forms.HScrollBar scrollMaterialSpecularAlpha;

        private System.Windows.Forms.Label labelMaterialShiness;

        private System.Windows.Forms.HScrollBar scrollMaterialShiness;

        private System.Windows.Forms.Label labelAmbientLightPosX;
        private System.Windows.Forms.Label labelAmbientLightPosY;
        private System.Windows.Forms.Label labelAmbientLightPosZ;

        private System.Windows.Forms.Label labelAmbientLightX;
        private System.Windows.Forms.Label labelAmbientLightY;
        private System.Windows.Forms.Label labelAmbientLightZ;

        private System.Windows.Forms.HScrollBar scrollLightPositionX;
        private System.Windows.Forms.HScrollBar scrollLightPositionY;
        private System.Windows.Forms.HScrollBar scrollLightPositionZ;

        private System.Windows.Forms.HScrollBar scrollLightAmbientX;
        private System.Windows.Forms.HScrollBar scrollLightAmbientY;
        private System.Windows.Forms.HScrollBar scrollLightAmbientZ;

        private System.Windows.Forms.Button buttonLight;
        private System.Windows.Forms.Button buttonPosLight;

        private System.Windows.Forms.Panel panelOptions;
    }
}
