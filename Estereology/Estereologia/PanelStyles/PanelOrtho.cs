﻿using Stereology.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/* Esta classe é usada para criar a interface de gerenciamento para o modelo
 * ortogonal de visualização das esferas no espaço tridimensional*/

namespace Stereology.PanelStyles
{
    public class PanelOrtho
    {
        public PanelOrtho()
        {

        }

        public PanelOrtho(int positionX, int positionY, int sizeX, int sizeY)
        {
            this.positionX = positionX;
            this.positionY = positionY;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
        }

        public void createPanel(System.Windows.Forms.Panel  panelInfo)
        {
            panelOrthoView = new System.Windows.Forms.Panel();
            panelOrthoView.SetBounds(positionX, positionY, sizeX, sizeY);
            panelOrthoView.BackColor = System.Drawing.Color.FromArgb(200, 200, 200);

            createColorPanel();
            createScrollScale();
            createLabels();
            createButtons();

            panelInfo.Controls.Add(panelOrthoView);
        }

        public void setVisible(bool isVisible)
        {
            if (isVisible)
            {
                panelOrthoView.Visible = true;
            }else
            {
                panelOrthoView.Visible = false;
            }

        }

        //Métodos para adicionar as labels no panel
        public void createLabels()
        {
            System.Windows.Forms.Label labelTitle = new System.Windows.Forms.Label();
            labelTitle.Text = "Orthogonal Projection Management";
            labelTitle.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            labelTitle.ForeColor = System.Drawing.Color.Black;
            labelTitle.SetBounds(65, 10, 300, 20);
            panelOrthoView.Controls.Add(labelTitle);

            System.Windows.Forms.Label labelColor = new System.Windows.Forms.Label();
            labelColor.Text = "Set color of spheres: ";
            labelColor.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            labelColor.ForeColor = System.Drawing.Color.Black;
            labelColor.SetBounds(10, 50, 300, 20);
            panelOrthoView.Controls.Add(labelColor);

            System.Windows.Forms.Label labelScale = new System.Windows.Forms.Label();
            labelScale.Text = "Display scale: ";
            labelScale.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            labelScale.ForeColor = System.Drawing.Color.Black;
            labelScale.SetBounds(10, 140, 300, 20);
            panelOrthoView.Controls.Add(labelScale);

            System.Windows.Forms.Label labelMovement = new System.Windows.Forms.Label();
            labelMovement.Text = "Display rotation: ";
            labelMovement.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            labelMovement.ForeColor = System.Drawing.Color.Black;
            labelMovement.SetBounds(10, 190, 300, 20);
            panelOrthoView.Controls.Add(labelMovement);

            System.Windows.Forms.Label labelInfo = new System.Windows.Forms.Label();
            labelInfo.Text = "Information: ";
            labelInfo.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            labelInfo.ForeColor = System.Drawing.Color.Black;
            labelInfo.SetBounds(10, 270, 110, 20);
            panelOrthoView.Controls.Add(labelInfo);

            labelQuantSpheres = new System.Windows.Forms.Label();
            labelQuantSpheres.Text = "Number of spheres: " + Atributes.quantSpheres;
            labelQuantSpheres.SetBounds(10, 290, 300, 20);
            panelOrthoView.Controls.Add(labelQuantSpheres);

            labelVolumeSpheres = new System.Windows.Forms.Label();
            labelVolumeSpheres.Text = "Total volume of spheres: " + calculateTotalVolumn();
            labelVolumeSpheres.SetBounds(10, 310, 350, 20);
            panelOrthoView.Controls.Add(labelVolumeSpheres);

            labelVolumeMatrix = new System.Windows.Forms.Label();
            labelVolumeMatrix.Text = "Unit volume: " + calculateTotalMatrix();
            labelVolumeMatrix.SetBounds(10, 330, 300, 20);
            panelOrthoView.Controls.Add(labelVolumeMatrix);

            labelTotalVolume = new System.Windows.Forms.Label();
            labelTotalVolume.Text = "Total volume: " + (calculateTotalVolumn() + calculateTotalMatrix()).ToString();
            labelTotalVolume.SetBounds(10, 350, 300, 20);
            panelOrthoView.Controls.Add(labelTotalVolume);
        }

        //Método utilizado para criar a escala de visão
        public void createScrollScale()
        {
            scrollScale = new System.Windows.Forms.HScrollBar();
            scrollScale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollScale.SetBounds(10, 160, 200, 15);
            scrollScale.Maximum = 109;
            scrollScale.Minimum = 10;
            scrollScale.TabIndex = 0;
            scrollScale.Value = 50;
            scrollScale.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollScale);
            panelOrthoView.Controls.Add(scrollScale);

            textScale = new System.Windows.Forms.TextBox();
            textScale.SetBounds(220, 155, 30, 20);
            textScale.Enabled = false;
            textScale.Text = "50";
            panelOrthoView.Controls.Add(textScale);
        }

        public void createColorPanel()
        {
            picSample = new System.Windows.Forms.PictureBox();
            picSample.SetBounds(260, 73, 55, 55);
            picSample.BackColor = Color.Black;
            picSample.BackColor = Color.FromArgb(100, 100, 100);
            panelOrthoView.Controls.Add(picSample);

            scrollR = new System.Windows.Forms.HScrollBar();
            scrollR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollR.SetBounds(10, 70, 200, 15);
            scrollR.Maximum = 264;
            scrollR.TabIndex = 29;
            scrollR.Value = 100;
            scrollR.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrRGB_Scroll) ;
            panelOrthoView.Controls.Add(scrollR);

            scrollG = new System.Windows.Forms.HScrollBar();
            scrollG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollG.SetBounds(10, 90, 200, 15);
            scrollG.Maximum = 264;
            scrollG.TabIndex = 31;
            scrollG.Value = 100;
            scrollG.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrRGB_Scroll);
            panelOrthoView.Controls.Add(scrollG);

            scrollB = new System.Windows.Forms.HScrollBar();
            scrollB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollB.SetBounds(10, 110, 200, 15);
            scrollB.Maximum = 264;
            scrollB.TabIndex = 30;
            scrollB.Value = 100;
            scrollB.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrRGB_Scroll);
            panelOrthoView.Controls.Add(scrollB);

            this.scrollS = new System.Windows.Forms.HScrollBar();
            this.scrollS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
           | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollS.SetBounds(10, 110, 200, 15);
            this.scrollS.LargeChange = 100;
            this.scrollS.Maximum = 1099;
            this.scrollS.SmallChange = 10;
            this.scrollS.TabIndex = 39;
            this.scrollS.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrHLS_Scroll);
            //panelOrthoView.Controls.Add(scrollS);

            this.scrollL = new System.Windows.Forms.HScrollBar();
            this.scrollL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
           | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollL.SetBounds(10, 130, 200, 15);
            this.scrollL.LargeChange = 100;
            this.scrollL.Maximum = 1099;
            this.scrollL.SmallChange = 10;
            this.scrollL.TabIndex = 39;
            this.scrollL.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrHLS_Scroll);
            //panelOrthoView.Controls.Add(scrollL);

            this.scrollH = new System.Windows.Forms.HScrollBar();
            this.scrollH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
           | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollH.SetBounds(10, 150, 200, 15);
            this.scrollH.LargeChange = 100;
            this.scrollH.Maximum = 1099;
            this.scrollH.SmallChange = 10;
            this.scrollH.TabIndex = 39;
            this.scrollH.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrHLS_Scroll);
            //panelOrthoView.Controls.Add(scrollH);

            this.textR = new System.Windows.Forms.TextBox();
            this.textR.SetBounds(220, 70, 30, 20);
            this.textR.Enabled = false;
            this.textR.Text = "100";
            panelOrthoView.Controls.Add(textR);

            this.textG = new System.Windows.Forms.TextBox();
            this.textG.SetBounds(220, 90, 30, 20);
            this.textG.Enabled = false;
            this.textG.Text = "100";
            panelOrthoView.Controls.Add(textG);

            this.textB = new System.Windows.Forms.TextBox();
            this.textB.SetBounds(220, 110, 30, 20);
            this.textB.Enabled = false;
            this.textB.Text = "100";
            panelOrthoView.Controls.Add(textB);

        }

        public void createButtons()
        {
            buttonLeft = new System.Windows.Forms.Button();
            buttonLeft.SetBounds(10, 210, 50, 50);
            buttonLeft.Image = Image.FromFile("C:\\Users\\igor\\Documents\\Visual Studio 2015\\Projects\\Estereology\\SistemaSolar\\Resources\\sLeft.png");
            buttonLeft.ImageAlign = ContentAlignment.MiddleRight;
            buttonLeft.ImageAlign = ContentAlignment.MiddleLeft;
            buttonLeft.Click += new System.EventHandler(this.onClickLeftButton);
            panelOrthoView.Controls.Add(buttonLeft);

            buttonRight = new System.Windows.Forms.Button();
            buttonRight.SetBounds(120, 210, 50, 50);
            buttonRight.Image = Image.FromFile("C:\\Users\\igor\\Documents\\Visual Studio 2015\\Projects\\Estereology\\SistemaSolar\\Resources\\sRigth.png");
            buttonRight.ImageAlign = ContentAlignment.MiddleRight;
            buttonRight.ImageAlign = ContentAlignment.MiddleLeft;
            buttonRight.Click += new System.EventHandler(this.onClickRightButton);
            panelOrthoView.Controls.Add(buttonRight);

            buttonPause = new System.Windows.Forms.Button();
            buttonPause.SetBounds(65, 210, 50, 50);
            buttonPause.Image = Image.FromFile("C:\\Users\\igor\\Documents\\Visual Studio 2015\\Projects\\Estereology\\SistemaSolar\\Resources\\pause.png");
            buttonPause.ImageAlign = ContentAlignment.MiddleRight;
            buttonPause.ImageAlign = ContentAlignment.MiddleLeft;
            buttonPause.Click += new System.EventHandler(this.onClickPauseButton);
            panelOrthoView.Controls.Add(buttonPause);

        }

        //Método para iniciar o evento de rotação para a esquerda
        private void onClickLeftButton(object sender, System.EventArgs e)
        {
            MainForm.rotateInScene = 1;
        }

        //Método para iniciar o evento de rotação para a esquerda
        private void onClickRightButton(object sender, System.EventArgs e)
        {
            MainForm.rotateInScene = -1;
        }

        //Método para iniciar o evento para pausar as rotações
        private void onClickPauseButton(object sender, System.EventArgs e)
        {
            Camera.rotate = 0;
            MainForm.rotateInScene = 0;

        }

        private void scrRGB_Scroll(object sender, ScrollEventArgs e)
        {
            // Save the selected color and display a sample.
            int R = scrollR.Value;
            int G = scrollG.Value;
            int B = scrollB.Value;
            picSample.BackColor = Color.FromArgb(R, G, B);

            // Convert to HLS.
            double H, L, S;
            ColorManager.RgbToHls(R, G, B, out H, out L, out S);

            // Display HLS values.
            scrollH.Value = (int)H;
            scrollL.Value = (int)(L * 1000);
            scrollS.Value = (int)(S * 1000);

            ShowNumericValues(R, G, B, H, L, S);
        }

        private void scrHLS_Scroll(object sender, ScrollEventArgs e)
        {
            // Convert into RGB.
            double H = scrollH.Value;
            double L = scrollL.Value / 1000.0;
            double S = scrollS.Value / 1000.0;
            int R, G, B;
            ColorManager.HlsToRgb(H, L, S, out R, out G, out B);

            // Display RGB values.
            scrollR.Value = R;
            scrollG.Value = G;
            scrollB.Value = B;

            // Save the selected color and display a sample.
            //picSample.BackColor = Color.FromArgb(R, G, B);

            ShowNumericValues(R, G, B, H, L, S);
        }

        private void onScrollScale(object sender, ScrollEventArgs e)
        {
            double scaleDouble = scrollScale.Value;
            textScale.Text = scaleDouble.ToString();

            Atributes.scaleOrthoX = (float)scaleDouble / 50;
            Atributes.scaleOrthoY = (float)scaleDouble / 50;
            Atributes.scaleOrthoZ = (float)scaleDouble / 50;

        }

        private void ShowNumericValues(int R, int G, int B, double H, double L, double S)
        {
            textR.Text = R.ToString();
            textG.Text = G.ToString();
            textB.Text = B.ToString();
           // textH.Text = H.ToString("0");
            //textL.Text = L.ToString("0.00");
            //textS.Text = S.ToString("0.00");
        }

        public System.Windows.Forms.Label getLabelQuantSpheres()
        {
            return labelQuantSpheres;
        }

        public System.Windows.Forms.Label getLabelVolumnTotal()
        {
            return labelVolumeSpheres;
        }

        public System.Windows.Forms.Label getLabelVolumMatrix()
        {
            return labelVolumeMatrix;
        }

        public System.Windows.Forms.Label getLabelTotalVolume()
        {
            return labelTotalVolume;
        }


        public double calculateTotalVolumn()
        {
            double volume = 0;

            foreach (Spheres esfera in Stereology.listEsferas)
            {
                volume = volume + ((4 / 3) * Math.PI * (Math.Pow(esfera.getRadius(), 3)));
            }
            return volume;
        }

        public double calculateTotalMatrix()
        {
            double volumnMatrix = 0;
            double volumeTotal = 1.0;
            double volumeEsferas;

            volumeEsferas = calculateTotalVolumn();

            volumnMatrix = volumeTotal - volumeEsferas;

            return volumnMatrix;
        }

        private int positionX;
        private int positionY;
        private int sizeX;
        private int sizeY;

        private System.Windows.Forms.Label labelQuantSpheres;
        private System.Windows.Forms.Label labelVolumeSpheres;
        private System.Windows.Forms.Label labelVolumeMatrix;
        private System.Windows.Forms.Label labelTotalVolume;

        private System.Windows.Forms.Panel panelOrthoView;
        public static System.Windows.Forms.Button buttonLeft;               //botão para a rotação na esquerda
        public System.Windows.Forms.Button buttonRight;                     //botão para a rotação na direita
        public static System.Windows.Forms.Button buttonPause;              //botão para pausar as rotações
        public static System.Windows.Forms.HScrollBar scrollR;              //scroll bar para a cor red
        public static System.Windows.Forms.HScrollBar scrollG;              //scroll bar para a cor green
        public static System.Windows.Forms.HScrollBar scrollB;              //scroll bar para a cor blue
        private System.Windows.Forms.HScrollBar scrollS;
        private System.Windows.Forms.HScrollBar scrollL;
        private System.Windows.Forms.HScrollBar scrollH;
        private System.Windows.Forms.HScrollBar scrollScale;                //scroll bar para a escala
        private System.Windows.Forms.PictureBox picSample;
        private System.Windows.Forms.TextBox textR;
        private System.Windows.Forms.TextBox textG;
        private System.Windows.Forms.TextBox textB;
        private System.Windows.Forms.TextBox textScale;                     //textBox com a escala da imagem
    }
}
