﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stereology.PanelStyles
{
    public class PanelCrop
    {
        public PanelCrop()
        {

        }

        public PanelCrop(int positionX, int positionY, int sizeX, int sizeY, System.Windows.Forms.Panel panelApplication)
        {
            this.positionX = positionX;
            this.positionY = positionY;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            this.panelApplication = panelApplication;

            createPanelCrop();
        }

        public void createPanelCrop()
        {
            panelCrop = new System.Windows.Forms.Panel();
            panelCrop.SetBounds(positionX, positionY, sizeX, sizeY);
            panelCrop.Visible = true;
            panelCrop.BackColor = System.Drawing.Color.FromArgb(200, 200, 200);

            createLabels();
            createTextBox();
            createScrollPositionPlane();

            panelApplication.Controls.Add(panelCrop);
        }

        public void createLabels()
        {
            System.Windows.Forms.Label labelTitle = new System.Windows.Forms.Label();
            labelTitle.Text = "Gerenciamento do plano de corte";
            labelTitle.Font = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
            labelTitle.ForeColor = System.Drawing.Color.Black;
            labelTitle.SetBounds(65, 10, 300, 20);
            panelCrop.Controls.Add(labelTitle);

            System.Windows.Forms.Label labelPositionPlane = new System.Windows.Forms.Label();
            labelPositionPlane.Text = "Posição do plano: ";
            labelPositionPlane.Font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
            labelPositionPlane.ForeColor = System.Drawing.Color.Black;
            labelPositionPlane.SetBounds(10, 50, 300, 20);
            panelCrop.Controls.Add(labelPositionPlane);
        }

        public void createScrollPositionPlane()
        {
            scrollPositionPlane = new System.Windows.Forms.HScrollBar();
            scrollPositionPlane.SetBounds(10, 75, 200, 20);
            scrollPositionPlane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            scrollPositionPlane.Maximum = 110;
            scrollPositionPlane.Minimum = -100;
            scrollPositionPlane.TabIndex = 0;
            scrollPositionPlane.Value = (int)(Atributes.positionPlane);
            scrollPositionPlane.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onScrollPositionPlane);

            panelCrop.Controls.Add(scrollPositionPlane);
        }

        public void createTextBox()
        {
            textBoxPositionPlane = new System.Windows.Forms.TextBox();
            textBoxPositionPlane.SetBounds(220, 75, 30, 20);
            textBoxPositionPlane.Enabled = false;

            panelCrop.Controls.Add(textBoxPositionPlane);
        }


        public void setVisible(bool isVisible)
        {
            this.isVisible = isVisible;
        }

        public void onScrollPositionPlane(object sender, EventArgs e)
        {
            textBoxPositionPlane.Text = scrollPositionPlane.Value.ToString();
            Atributes.positionPlane = scrollPositionPlane.Value;
        }

        private bool isVisible;
        private int positionX;
        private int positionY;
        private int sizeX;
        private int sizeY;
        private System.Windows.Forms.Panel panelCrop;
        private System.Windows.Forms.Panel panelApplication;
        private System.Windows.Forms.HScrollBar scrollPositionPlane;
        private System.Windows.Forms.TextBox textBoxPositionPlane;
    }
}
