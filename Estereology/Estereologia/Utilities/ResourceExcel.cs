﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stereology.Utilities
{
    public class ResourceExcel
    {
        public delegate void InvokeDelegate(Microsoft.Office.Interop.Excel.Worksheet worksheet);

        public ResourceExcel()
        {

        }

        public ResourceExcel(ListView listView)
        {
            this.listView = listView;
        }

        public void toExcel(object data)
        {
            Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();
            application.Visible = true;

            Microsoft.Office.Interop.Excel.Workbook workBook = application.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.Worksheets[1];

            listView.BeginInvoke(new MethodInvoker(delegate() {

                addElements(worksheet);

            }));
        }

        public void addElements(Microsoft.Office.Interop.Excel.Worksheet worksheet)
        {
            try
            {
                //Esquema para adicionar a importação das células na planilha do excel.
                for (int i = 0; i < listView.Items.Count; i++)
                {
                      ListViewItem item = listView.Items[i];

                    for (int j = 1; j < item.SubItems.Count; j++)
                    {
                        worksheet.Cells[i + 1, j] = Double.Parse(item.SubItems[j].Text);
                        worksheet.Columns[i + 1].ColumnWidth = 18;
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Erro de exportação: " + exception);
            }
        }

        private ListView listView;
    }
}
