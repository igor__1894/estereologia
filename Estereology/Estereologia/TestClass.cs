﻿using System;
using System.Collections.Generic;
using System.Text;
using Tao.OpenGl;
using ShadowEngine;

namespace Stereology
{
    class TestClass
    {
        int list;


        public void create()
        {
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

            Gl.glBegin(Gl.GL_QUADS);
            Gl.glColor3f(0.0f, 0.0f, 1.0f);
            Gl.glVertex2d(100, 150);
            Gl.glVertex2d(100, 100);
            Gl.glVertex2d(150, 100);
            Gl.glVertex2d(150, 150);
            Gl.glEnd();
            Gl.glFlush();

            /* Glu.GLUquadric quadratic = Glu.gluNewQuadric();
             Glu.gluQuadricNormals(quadratic, Glu.GLU_SMOOTH);
             Glu.gluQuadricTexture(quadratic, Gl.GL_TRUE);

             list = Gl.glGenLists(1);
             Gl.glNewList(list, Gl.GL_COMPILE);
             Gl.glPushMatrix();
             Gl.glDisable(Gl.GL_LIGHTING);

             //Glu.gluCylinder(quadratic, 0.4f, 0.2f, 0.5f, 50, 100);
             Glu.gluDisk(quadratic, 0.1f, 0.3f, 50, 10);
             Gl.glEnable(Gl.GL_LIGHTING);

             Gl.glPopMatrix();
             Gl.glEndList();*/
        }

        public void paint()
        {
            Gl.glPushMatrix();
            Gl.glColor3f(1.0f, 0.0f, 1.0f);
            Gl.glCallList(list);
            Gl.glPopMatrix();
            Gl.glDisable(Gl.GL_TEXTURE_2D);

        }
    }
}
