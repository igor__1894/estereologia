﻿using Stereology.MathAlgo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace Stereology.GraphicsPlot
{
    public class HistoSaltyArea
    {

        public HistoSaltyArea()
        {

        }

        public HistoSaltyArea(List<Interval> listIntervalSaltykov, List<Interval>listIntervalSaltykovRadius, double maxArea)
        {
            this.listIntervalSaltykov = listIntervalSaltykov;
            this.listIntervalSaltykovRadius = listIntervalSaltykovRadius;
            this.maxArea = maxArea;

        }

        public Chart createHistogramn(List<Interval> listHistAxisX,List<double> listHistAxisY, string textAxisX, string textAxisY, int positionx, int positiony, int sizex, int sizey)
        {
            double maxInterval;

            try
            {
                Chart chart = new Chart();
                //chart.SetBounds(10, 50, 400, 400);
                chart.SetBounds(positionx,positiony, sizex, sizey);

                maxInterval = listHistAxisX[listHistAxisX.Count - 1].getIntervalMax();

                List<String> listAxisX = new List<String>();
                for (int i = listHistAxisX.Count - 1; i >= 0; i--)
                {
                    double valueXMax = Math.Round(listHistAxisX[i].getIntervalMax(), 3);
                    double valueXMin = Math.Round(listHistAxisX[i].getIntervalMin(), 3);

                    listAxisX.Add(valueXMin.ToString() + " - " + valueXMax.ToString());
                }

                //Atribuição dos valores do eixo y
                List<double> listAxisY = new List<double>();
                for (int i = listHistAxisY.Count - 1; i >= 0; i--)
                {
                    listAxisY.Add((double)listHistAxisY[i]);
                }


                ChartArea chartArea = new ChartArea("chart1");
                chartArea.AxisX.Title = textAxisX;
                chartArea.AxisY.Title = textAxisY;
                chart.ChartAreas.Add(chartArea);


                Series barSeries = new Series();
                barSeries.Points.DataBindXY(listAxisX, listAxisY);
                barSeries.ChartType = SeriesChartType.RangeColumn;
                barSeries.ChartArea = "chart1";
                chart.Series.Add(barSeries);

                return chart;

            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception.ToString());
            }

            return null;
        }
        public Chart createHistogramn()
        {
            double maxInterval;

            try
            {
                Chart chart = new Chart();
                chart.SetBounds(10, 50, 400, 400);

                maxInterval = this.listIntervalSaltykov[this.listIntervalSaltykov.Count - 1].getIntervalMax();

                List<String> listAxisX = new List<String>();
                for (int i = listIntervalSaltykov.Count - 1; i >= 0; i--)
                {
                    double valueXMax = Math.Round(listIntervalSaltykov[i].getIntervalMax(), 3);
                    double valueXMin = Math.Round(listIntervalSaltykov[i].getIntervalMin(), 3);

                    listAxisX.Add(valueXMin.ToString() + " - " + valueXMax.ToString());
                }

                double sum = 0;

                //Atribuição dos valores do eixo y
                List<double> listAxisY = new List<double>();
                for (int i = listIntervalSaltykov.Count - 1; i >= 0; i--)
                {
                    listAxisY.Add((double)listIntervalSaltykov[i].getFrequence());
                }

                ChartArea chartArea = new ChartArea("chart1");
                chartArea.AxisX.Title = "Intervalo Diâmetro (D/Dmax)";
                chartArea.AxisY.Title = "NV (Número de partícular por unidade de volume)";
                chart.ChartAreas.Add(chartArea);

                Series barSeries = new Series();
                barSeries.Points.DataBindXY(listAxisX, listAxisY);
                barSeries.ChartType = SeriesChartType.RangeColumn;
                barSeries.ChartArea = "chart1";
                chart.Series.Add(barSeries);

                return chart;

            }
            catch(Exception exception)
            {

            }

            return null;
        }

        public int getTotalFrequence()
        {
            int sum = 0;

            for (int i = 0; i < listIntervalSaltykov.Count; i++)
            {

                sum = sum + listIntervalSaltykov[i].getFrequence();
            }

            return sum;
        }


        private double maxArea;
        private List<Interval> listIntervalSaltykov;
        private List<Interval> listIntervalSaltykovRadius;
    }
}
