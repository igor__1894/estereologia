﻿using Stereology.MathAlgo;
using Stereology.PanelStyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

/* Classe utilizada para a criação do histograma utlizando o método de Saltykov*/
namespace Stereology.GraphicsPlot
{
    public class HistoSaltyRadio
    {

        public HistoSaltyRadio()
        {

        }

        public HistoSaltyRadio(List<Interval> listIntervalSaltykov, double maxRadius)
        {
            this.maxRadius = maxRadius;
            this.listIntervalSaltykov = listIntervalSaltykov;
        }

        public Chart createHistogramn()
        {
            double maxInterval;

            try
            {
                Chart chart = new Chart();
                chart.SetBounds(10, 50, 400, 400);
                chart.BackColor = System.Drawing.Color.Gray;

                maxInterval = this.listIntervalSaltykov[this.listIntervalSaltykov.Count - 1].getIntervalMax();

                //Atribuição dos valores do eixo X
                List<String> listAxisX = new List<String>();
                for(int i = listIntervalSaltykov.Count - 1; i >= 0; i--)
                {
                    //listAxisX.Add((listIntervalSaltykov[i].getIntervalMax()/maxRadius).ToString());

                    double valueXMax = Math.Round(listIntervalSaltykov[i].getIntervalMax(), 3);
                    double valueXMin = Math.Round(listIntervalSaltykov[i].getIntervalMin(), 3);

                    listAxisX.Add(valueXMin.ToString() + " - " + valueXMax.ToString());
                }

                Double sum = 0;

                //Atribuição dos valores do eixo y
                List<double> listAxisY = new List<double>();
                for (int i = listIntervalSaltykov.Count - 1; i >= 0; i--)
                {
                    double relativeFrequence = (double)(listIntervalSaltykov[i].getFrequence())/(getTotalFrequence());
                    Console.WriteLine("Frequência relativa: " + relativeFrequence);
                    sum = sum + relativeFrequence;

                    listAxisY.Add(relativeFrequence);
                }

                Double result = 0;

                result = 1.6461 * ((double)(listIntervalSaltykov[listIntervalSaltykov.Count - 1].getFrequence()) / (getTotalFrequence())) -
                0.4561 * ((double)(listIntervalSaltykov[listIntervalSaltykov.Count - 2].getFrequence()) / (getTotalFrequence())) -
                0.1162 * ((double)(listIntervalSaltykov[listIntervalSaltykov.Count - 3].getFrequence()) / (getTotalFrequence())) -
                0.0415 * ((double)(listIntervalSaltykov[listIntervalSaltykov.Count - 4].getFrequence()) / (getTotalFrequence())) -
                0.0173 * ((double)(listIntervalSaltykov[listIntervalSaltykov.Count - 5].getFrequence()) / (getTotalFrequence()));

                Console.WriteLine("RESULTADO: " + result);


               //Console.WriteLine("RESULTADO: " + sum);

               ChartArea chartArea = new ChartArea("chart1");
               chartArea.AxisX.Title = "Raio relativo (R/Rmax)";
               chartArea.AxisY.Title = "Frequência relativa (F/Fmax)";
               chart.ChartAreas.Add(chartArea);

               Series barSeries = new Series();
               barSeries.Points.DataBindXY(listAxisX, listAxisY);
               barSeries.ChartType = SeriesChartType.RangeColumn;
               barSeries.ChartArea = "chart1";
               chart.Series.Add(barSeries);

               return chart;
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception.ToString());

                PanelStyles.PanelDialog panelDialog = new PanelDialog("Erro Histograma", "Infelizmente houve um problema ao criar o histograma, \r\n verifique se os planos foram criados.");
            }

            return null;
        }

        public int getTotalFrequence()
        {
            int sum = 0;

            for(int i = 0; i < listIntervalSaltykov.Count; i++)
            {

                sum = sum + listIntervalSaltykov[i].getFrequence();
            }

            return sum;
        }

        private double maxRadius;
        private double minRadius;
        private List<Interval> listIntervalSaltykov;
    }
}
